(function () {
    'use strict';

    angular
            .module('blankonApp')
            .factory('promocionService', promocionService);

    promocionService.$inject = ['$http', '$rootScope'];
    function promocionService($http, $rootScope) {
        var service = {};

        service.GetIndex = GetIndex;
        service.GetShow = GetShow;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        return service;

        function GetIndex(request) {
            return $http.get($rootScope.servidor + '/promocion', {params: request}).then(handleSuccess, handleError('Error al consultar promocion'));
        }

        function GetShow(id) {
            return $http.get($rootScope.servidor + '/promocion/' + id).then(handleSuccess, handleError('Error getting all promocion'));
        }

        function Create(OBJpromocion) {
            return $http.post($rootScope.servidor + '/promocion', OBJpromocion).then(handleSuccess, handleError('Error creating promocion'));
        }

        function Update(OBJpromocion) {
            return $http.post($rootScope.servidor + '/promocion/' + OBJpromocion.promocion.idpromocion, OBJpromocion).then(handleSuccess, handleError('Error updating promocion'));
        }

        function Delete(id) {
            return $http.post($rootScope.servidor + '/promocion/delete/' + id).then(handleSuccess, handleError('Error deleting promocion'));
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
