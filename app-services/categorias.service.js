(function () {
    'use strict';

    angular
            .module('blankonApp')
            .factory('categoriaService', categoriaService);

    categoriaService.$inject = ['$http', '$rootScope'];
    function categoriaService($http, $rootScope) {
        var service = {};

        service.GetIndex = GetIndex;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;
 
        function GetIndex(request) {
            return $http.get($rootScope.servidor + '/categoria', {params: request}).then(handleSuccess, handleError('Error al consultar entidad'));
        }
 
        function Create(categoria) {
            return $http.post($rootScope.servidor + '/categoria', categoria).then(handleSuccess, handleError('Error creating entidad'));
        }
 
        function Update(categoria) {
            return $http.post($rootScope.servidor + '/categoria/' + categoria.idarbol, categoria).then(handleSuccess, handleError('Error updating entidad'));
        }
 
        function Delete(id, request) {
            return $http.post($rootScope.servidor + '/categoria/delete/' + id, request).then(handleSuccess, handleError('Error deleting user'));
        }
 
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
