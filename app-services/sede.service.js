(function () {
    'use strict';

    angular
            .module('blankonApp')
            .factory('sedeService', sedeService);

    sedeService.$inject = ['$http', '$rootScope'];
    function sedeService($http, $rootScope) {
        var service = {};

        service.GetIndex = GetIndex;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        return service;
 
        function GetIndex() {
            return $http.get($rootScope.servidor + '/sede').then(handleSuccess, handleError('Error al consultar entidad'));
        }
 
        function Create(sede) {
            return $http.post($rootScope.servidor + '/sede', sede).then(handleSuccess, handleError('Error creating entidad'));
        }
 
        function Update(sede) {
            return $http.post($rootScope.servidor + '/sede/' + sede.idsede, sede).then(handleSuccess, handleError('Error updating entidad'));
        }
 
        function Delete(id) {
            return $http.post($rootScope.servidor + '/sede/delete/' + id).then(handleSuccess, handleError('Error deleting user'));
        }
 
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
