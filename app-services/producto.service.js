(function () {
    'use strict';

    angular
            .module('blankonApp')
            .factory('productoService', productoService);

    productoService.$inject = ['$http', '$rootScope'];
    function productoService($http, $rootScope) {
        var service = {};

        service.GetIndex = GetIndex;
        service.GetIndexStock = GetIndexStock;
        service.GetIndexStockDet = GetIndexStockDet;
        service.GetIndexKardex = GetIndexKardex;
        service.GetShow = GetShow;
        service.GetNew = GetNew;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete; 
        return service;

        function GetIndex(request) {
            return $http.get($rootScope.servidor + '/producto', {params: request}).then(handleSuccess, handleError('Error al consultar entidad'));
        }

        function GetIndexStock(request) {
            return $http.get($rootScope.servidor + '/producto/stock', {params: request}).then(handleSuccess, handleError('Error al consultar productos'));
        }

        function GetIndexStockDet(request) {
            return $http.get($rootScope.servidor + '/producto/stock/det', {params: request}).then(handleSuccess, handleError('Error al consultar productos'));
        }

        function GetIndexKardex(request) {
            return $http.get($rootScope.servidor + '/producto/kardex', {params: request}).then(handleSuccess, handleError('Error al consultar productos'));
        }

        function GetShow(id) {
            return $http.get($rootScope.servidor + '/producto/' + id).then(handleSuccess, handleError('Error getting all producto'));
        }

        function GetNew() {
            return $http.get($rootScope.servidor + '/producto/new').then(handleSuccess, handleError('Error getting all producto'));
        }

        function Create(OBJproducto) {
            return $http.post($rootScope.servidor + '/producto', OBJproducto).then(handleSuccess, handleError('Error creating entidad'));
        }

        function Update(OBJproducto) {
            return $http.post($rootScope.servidor + '/producto/' + OBJproducto.producto.idproducto, OBJproducto).then(handleSuccess, handleError('Error updating entidad'));
        }

        function Delete(id) {
            return $http.post($rootScope.servidor + '/producto/delete/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
