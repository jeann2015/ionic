(function () {
    'use strict';
    
    angular
            .module('blankonApp')
            .factory('entidadService', entidadService);

    entidadService.$inject = ['$http', '$rootScope'];     
    function entidadService($http, $rootScope) {
        var service = {}; 
        
        service.GetIndex = GetIndex;  
        service.GetCumpleanos = GetCumpleanos; 
        service.GetModulos = GetModulos; 
        service.GetSearch = GetSearch;
        service.GetShow = GetShow; 
        service.GetNew = GetNew;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;        
        service.GetNumero = GetNumero;
        service.UpdateEntidad = UpdateEntidad;
        service.UpdatePassword = UpdatePassword;
        service.GetProfile = GetProfile; 
        return service; 
        
        //Grid
        function GetIndex(request) {  
            return $http.get($rootScope.servidor + '/entidad', {params: request}).then(handleSuccess, handleError('Error al consultar entidad'));             
        }
        
        //Cumpleaños del dia
        function GetCumpleanos(request) {  
            return $http.get($rootScope.servidor + '/entidad/cumpleanos', {params: request}).then(handleSuccess, handleError('Error al consultar entidad'));             
        }
        
        //Modulos del usuario
        function GetModulos(request) {
            return $http.get($rootScope.servidor + '/entidad/modulos', {params: request}).then(handleSuccess, handleError('Error al consultar modulos de entidad'));             
        }
        
        //Autocompletable
        function GetSearch(request) { 
            return $http.get($rootScope.servidor + '/entidad', {params: request});
        }
        
        //Visualizar un registro
        function GetShow(id, request){ 
            return $http.get($rootScope.servidor + '/entidad/' + id, {params: request}).then(handleSuccess, handleError('Error getting all producto'));
        }
        
        //Visualizar profile
        function GetProfile(id){ 
            return $http.get($rootScope.servidor + '/entidad/profile/' + id).then(handleSuccess, handleError('Error getting all producto'));
        }
        
        //ConsultarSiExisteNroDocumento
        function GetNumero(request){
            return $http.get($rootScope.servidor + '/entidad/documento/nro', {params: request}).then(handleSuccess, handleError('Error getting all producto'));
        }
        
        //Actulizar Entidad Con una nueva entidad
        function UpdateEntidad(entidad){
            return $http.post($rootScope.servidor + '/entidad/subentidad/'+entidad.identidad, entidad).then(handleSuccess, handleError('Error getting all producto'));
        }
        
        //Actulizar Contrasena Entidad 
        function UpdatePassword(entidad){
            return $http.post($rootScope.servidor + '/entidad/password/'+entidad.identidad, entidad).then(handleSuccess, handleError('Error getting all producto'));
        }
        
        //Nuevo 
        function GetNew(request){
            return $http.get($rootScope.servidor + '/entidad/new', {params: request}).then(handleSuccess, handleError('Error getting all producto'));
        }
        
        //Grabar
        function Create(OBJentidad) {  
            return $http.post($rootScope.servidor + '/entidad', OBJentidad).then(handleSuccess, handleError('Error creating entidad'));
        }
        
        //Actualizar
        function Update(OBJentidad) {    
            return $http.post($rootScope.servidor + '/entidad/' + OBJentidad.entidad.identidad, OBJentidad).then(handleSuccess, handleError('Error updating entidad'));
        }
        
        //Eliminar
        function Delete(id, request) {
            return $http.post($rootScope.servidor + '/entidad/delete/' + id,  request).then(handleSuccess, handleError('Error deleting user'));
        }
        
        // private functions
        function handleSuccess(res) { 
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
