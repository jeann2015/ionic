(function () {
    'use strict';

    angular
            .module('blankonApp')
            .factory('ubigeoService', ubigeoService);

    ubigeoService.$inject = ['$http', '$rootScope'];
    function ubigeoService($http, $rootScope) {
        var service = {};

        service.GetIndex = GetIndex; 
        return service;

        function GetIndex(request) {
            return $http.get($rootScope.servidor + '/ubigeo', {params: request}).then(handleSuccess, handleError('Error al consultar entidad'));
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
