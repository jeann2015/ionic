(function () {
    'use strict';

    angular
            .module('blankonApp')
            .factory('perfilService', perfilService);

    perfilService.$inject = ['$http', '$rootScope'];
    function perfilService($http, $rootScope) {
        var service = {};

        service.GetIndex = GetIndex;
        service.GetShow = GetShow;
        service.GetNew = GetNew;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.UpdatePerfilModulo = UpdatePerfilModulo; 
        return service;

        function GetIndex(request) {
            return $http.get($rootScope.servidor + '/perfil', {params: request}).then(handleSuccess, handleError('Error al consultar entidad'));
        }

        function GetShow(id) {
            return $http.get($rootScope.servidor + '/perfil/' + id).then(handleSuccess, handleError('Error getting all producto'));
        }

        function GetNew() {
            return $http.get($rootScope.servidor + '/entidad/new').then(handleSuccess, handleError('Error getting all producto'));
        }

        function Create(perfil) {
            return $http.post($rootScope.servidor + '/perfil', perfil).then(handleSuccess, handleError('Error creating entidad'));
        }

        function Update(perfil) {
            return $http.post($rootScope.servidor + '/perfil/' + perfil.idperfil, perfil).then(handleSuccess, handleError('Error updating entidad'));
        }

        function UpdatePerfilModulo(OBJperfil) {
            return $http.post($rootScope.servidor + '/perfil/perfilmodulo/' + OBJperfil.idperfil, OBJperfil).then(handleSuccess, handleError('Error updating entidad'));
        }

        function Delete(id) {
            return $http.post($rootScope.servidor + '/perfil/delete/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
