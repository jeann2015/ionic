(function () {
    'use strict';

    angular
            .module('blankonApp')
            .factory('contactoService', contactoService);

    contactoService.$inject = ['$http', '$rootScope'];
    function contactoService($http, $rootScope) {
        var service = {};

        service.GetIndex = GetIndex;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;
 
        function GetIndex(request) {
            return $http.get($rootScope.servidor + '/contacto', {params: request}).then(handleSuccess, handleError('Error al consultar entidad'));
        }
 
        function Create(contacto) {
            return $http.post($rootScope.servidor + '/contacto', contacto).then(handleSuccess, handleError('Error creating entidad'));
        }
 
        function Update(contacto) {
            return $http.post($rootScope.servidor + '/contacto/' + contacto.idcontacto, contacto).then(handleSuccess, handleError('Error updating entidad'));
        }
 
        function Delete(id) {
            return $http.post($rootScope.servidor + '/contacto/delete/' + id).then(handleSuccess, handleError('Error deleting user'));
        }
 
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
