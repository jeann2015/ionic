(function () {
    'use strict';
    
    angular
            .module('blankonApp')
            .factory('modulosService', moduloService);

    moduloService.$inject = ['$http', '$rootScope'];     
    function moduloService($http, $rootScope) {
        var service = {}; 
        
        service.GetIndex = GetIndex;  
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete; 
        return service; 
         
        function GetIndex() {  
            return $http.get($rootScope.servidor + '/modulo').then(handleSuccess, handleError('Error al consultar entidad'));             
        }
         
        function Create(modulo) { 
            return $http.post($rootScope.servidor + '/modulo', modulo).then(handleSuccess, handleError('Error creating entidad'));
        }
         
        function Update(modulo) {  
            return $http.post($rootScope.servidor + '/modulo/' + modulo.idmodulo, modulo).then(handleSuccess, handleError('Error updating entidad'));
        }
         
        function Delete(id) {
            return $http.post($rootScope.servidor + '/modulo/delete/' + id).then(handleSuccess, handleError('Error deleting user'));
        }
         
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
