(function () {
    'use strict';
    
    angular
            .module('blankonApp')
            .factory('enterpriseService', enterpriseService);

    enterpriseService.$inject = ['$http', '$rootScope'];     
    function enterpriseService($http, $rootScope) { 
        var service = {}; 
         
        service.GetIndex = GetIndex; 
        service.GetShow = GetShow; 
        service.GetNew = GetNew;
        service.Create = Create;
        service.Update = Update;
        service.home = home;
        
        return service; 
        
        function home(){         
            return $http.get($rootScope.servidor + '/empresa/home').then(handleSuccess, handleError('Error getting all empresa'));
        }
        
        function GetIndex(request) {                         
            return $http.get($rootScope.servidor + '/empresa', {params: request}).then(handleSuccess, handleError('Error al consultar empresa'));            
        } 
        
        function GetShow(){            
            return $http.get($rootScope.servidor + '/empresa').then(handleSuccess, handleError('Error getting all empresa'));
        }
         
        function GetNew(){
            return $http.get($rootScope.servidor + '/empresa/new').then(handleSuccess, handleError('Error getting all empresa'));
        }
        
        function Create(product) {
            return $http.post($rootScope.servidor + '/empresa', product).then(handleSuccess, handleError('Error creating empresa'));
        }
 
        function Update(empresa) { 
            return $http.post($rootScope.servidor + '/empresa', empresa).then(handleSuccess, handleError('Error updating empresa'));
        }
         
        function handleSuccess(res) { 
            return res.data;
        }

        function handleError(error) { 
            return error; 
        }
    }
})();
