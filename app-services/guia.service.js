(function () {
    'use strict';

    angular
            .module('blankonApp')
            .factory('guiaService', guiaService);

    guiaService.$inject = ['$http', '$rootScope'];
    function guiaService($http, $rootScope) {
        var service = {};

        service.GetIndex = GetIndex;
        service.GetShow = GetShow;
        service.GetNew = GetNew;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        return service;

        function GetIndex(request) {
            return $http.get($rootScope.servidor + '/guia', {params: request}).then(handleSuccess, handleError('Error al consultar entidad'));
        }

        function GetShow(id) {
            return $http.get($rootScope.servidor + '/guia/' + id).then(handleSuccess, handleError('Error getting all producto'));
        }

        function GetNew(request) {
            return $http.get($rootScope.servidor + '/guia/new', {params: request}).then(handleSuccess, handleError('Error getting all producto'));
        }

        function Create(OBJguia) {
            return $http.post($rootScope.servidor + '/guia', OBJguia).then(handleSuccess, handleError('Error creating entidad'));
        }

        function Update(OBJguia) {
            return $http.post($rootScope.servidor + '/guia/' + OBJguia.guia.idguia, OBJguia).then(handleSuccess, handleError('Error updating entidad'));
        }

        function Delete(id) {
            return $http.post($rootScope.servidor + '/guia/delete/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();
