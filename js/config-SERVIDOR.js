'use strict';
angular.module('blankonConfig', ['ui-notification'])
    
    .factory('settings', ['$rootScope', function($rootScope) {
                    
        //var baseURL = 'http://cime.ionic.com', 
        var baseURL = 'http://www.lagranescuela.com', 
            settings = {
                baseURL                 : baseURL,
                pluginPath              : baseURL+'/assets/global/plugins/bower_components',
                pluginCommercialPath    : baseURL+'/assets/commercial/plugins',
                globalImagePath         : baseURL+'/img',
                adminImagePath          : baseURL+'/assets/admin/img',
                cssPath                 : baseURL+'/assets/admin/css',
                dataPath                : baseURL+'/data'
        };
        $rootScope.settings = settings;
        return settings;
    }])  
    .config(function(NotificationProvider){        
        NotificationProvider.setOptions({
            delay: 10000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'center',
            positionY: 'top'
        });
    })
    // Configuration angular loading bar
    .config(function(cfpLoadingBarProvider) {        
        cfpLoadingBarProvider.includeSpinner = true;
    })

    // Configuration event, debug and cache
    .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            events: false,
            debug: false,
            cache:false,
            cssFilesInsertBefore: 'ng_load_plugins_before',
            modules:[
                {
                    name: 'blankonApp.core.demo',
                    files: ['js/modules/core/demo.js']
                }
            ]
        });
    }])

    // Configuration ocLazyLoad with ui router
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

        $urlRouterProvider.otherwise('/about');
        
        var resolveEntidad = {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {                      
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.entidad',
                                    files: [                                        
                                        'js/modules/administracion/entidad.js',
                                        'app-services/entidad.service.js',
                                        'app-services/ubigeo.service.js',
                                        'app-services/contacto.service.js'
                                    ]
                                }
                            ]
                        );
                    }]
                };
                
        $stateProvider 
        
            .state('signin', {
                url: '/sign-in',
                templateUrl: 'views/sign/sign-in.html',
                data: {
                    pageTitle: 'SIGN IN', 
                    isPublic: true 
                },
                controller: 'SigninCtrl',
                controllerAs: 'vm',
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath, // Create variable css path
                            pluginPath = settings.pluginPath; // Create variable plugin path

                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load(
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        cssPath+'/pages/sign.css'
                                    ]
                                }, 
                                {
                                    name: 'blankonApp.account.signin',
                                    files: [
                                        'js/modules/sign/signin.js',
                                        'app-services/enterprise.service.js'
                                    ]
                                }
                            ]
                        );
                    }]
                    , 
                    simpleObj: function(){ 
                        return {value: 'simple!'};
                    },
                    enterpriseService: "enterpriseService"
                    ,
                    loadEnterprise: function(enterpriseService, $rootScope){
                        var promise;                        
                        promise = enterpriseService.home();                          
                        return promise;
                    }, 
                    // Example showing returning of custom made promise
                    greeting: function($q, $timeout){
//                        var deferred = $q.defer();
//                        $timeout(function() {
//                            deferred.resolve('Hello!');
//                        }, 500); 
//                        console.log(deferred.promise);
//                        return deferred.promise;
                    }
                }
            })  
            .state('about', {
                url: '/about',
                template: '<div>Bienvenido al sistema.</div>'               
            })
            .state('dashboard', {
                url: '/perfil/:perfilId',
                templateUrl: 'views/dashboard.html',
                data: {
                    pageTitle: 'MI PERFIL',
                    pageHeader: {
                        icon: 'fa fa-home',
                        title: 'Home',
                        subtitle: 'Mi perfil'
                    },
                    breadcrumbs: [
                        {title: 'Perfil'}, {title: 'Mi perfil'}
                    ]
                },
                controller: 'DashboardCtrl',
                controllerAs: 'vm', 
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {
                        return $ocLazyLoad.load(
                            [                                
                                {
                                    name: 'blankonApp.dashboard',
                                    files: [                                  
                                        'js/modules/dashboard.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            // =========================================================================
            // URLs IONIC
            // =========================================================================            
            .state('configuracion', {
                url: '/empresa',
                templateUrl: 'views/administracion/configuracion.html',
                data: {
                    pageTitle: 'MI EMPRESA',
                    pageHeader: {
                        icon: 'fa fa-institution',
                        title: 'Mi empresa',
                        subtitle: 'y sedes'
                    },
                    breadcrumbs: [
                        {title: 'Configuración'}, {title: 'Empresa'}
                    ]
                },
                controller: 'editEmpresaCtrl',
                controllerAs: 'vm',
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {
                        return $ocLazyLoad.load( 
                            [                                
                                {
                                    name: 'blankonApp.empresa',
                                    files: [
                                        'js/modules/administracion/empresa.js', 
                                        'app-services/sede.service.js', 
                                        'app-services/ubigeo.service.js'
                                    ]                                    
                                }
                            ]
                        );
                    }]
                }
            })
            .state('personal', {
                abstract: true,
                url: '/personal', 
                templateUrl: 'views/administracion/entidad.html',
                data: {
                    pageTitle: 'PERSONAL',
                    pageHeader: {
                        icon: 'fa fa-users',
                        title: 'Personal',
                        subtitle: 'de recursos humanos'
                    },
                    breadcrumbs: [
                        {title: 'Entidades'},{title: 'Personal'}
                    ]
                },
                controller: 'listEntidadCtrl as vm', 
                resolve: resolveEntidad
            })
            .state('personal.list', {url: '', template: ''})
            .state('personal.edit', { 
                url: '/editar/:entidadId',                               
                templateUrl: 'views/administracion/entidad-form.html',                
                controller: 'editEntidadCtrl as vm' 
            })
            .state('personal.new', { 
                url: '/nuevo',                 
                templateUrl: 'views/administracion/entidad-form.html',                
                controller: 'newEntidadCtrl as vm' 
            })            
            .state('proveedor', {
                abstract: true,
                url: '/proveedores', 
                templateUrl: 'views/administracion/entidad.html',
                data: {
                    pageTitle: 'PROVEEDORES',
                    pageHeader: {
                        icon: 'fa fa-truck',
                        title: 'Proveedores',
                        subtitle: 'de bienes y servicios'
                    },
                    breadcrumbs: [
                        {title: 'Entidades'},{title: 'Proveedores'}
                    ]
                },
                controller: 'listEntidadCtrl as vm', 
                resolve: resolveEntidad
            })            
            .state('proveedor.list', {url: ''})
            .state('proveedor.edit', { 
                url: '/editar/:entidadId',                               
                templateUrl: 'views/administracion/entidad-form.html',                
                controller: 'editEntidadCtrl as vm' 
            })
            .state('proveedor.new', { 
                url: '/nuevo',                 
                templateUrl: 'views/administracion/entidad-form.html',                
                controller: 'newEntidadCtrl as vm' 
            })
            .state('medico', { 
                abstract: true,
                url: '/medicos', 
                templateUrl: 'views/administracion/entidad.html',
                data: {
                    pageTitle: 'MEDICOS',
                    pageHeader: {
                        icon: 'fa fa-user-md',
                        title: 'Médicos',
                        subtitle: 'de recursos humanos'
                    },
                    breadcrumbs: [
                        {title: 'Entidades'},{title: 'Médicos'}
                    ]
                },
                controller: 'listEntidadCtrl as vm', 
                resolve: resolveEntidad
            })            
            .state('medico.list', {url: ''})
            .state('medico.edit', { 
                url: '/editar/:entidadId',                               
                templateUrl: 'views/administracion/entidad-form.html',                
                controller: 'editEntidadCtrl as vm' 
            })
            .state('medico.new', { 
                url: '/nuevo',                 
                templateUrl: 'views/administracion/entidad-form.html',                
                controller: 'newEntidadCtrl as vm' 
            })            
            .state('cliente', {
                abstract: true,
                url: '/clientes', 
                templateUrl: 'views/administracion/entidad.html',
                data: {
                    pageTitle: 'PACIENTES',
                    pageHeader: {
                        icon: 'fa fa-female',
                        title: 'Pacientes',
                        subtitle: ''
                    },
                    breadcrumbs: [
                        {title: 'Entidades'},{title: 'Pacientes'}
                    ]
                },
                controller: 'listEntidadCtrl as vm', 
                resolve: resolveEntidad
            })            
            .state('cliente.list', {url: ''})
            .state('cliente.edit', { 
                url: '/editar/:entidadId',                               
                templateUrl: 'views/administracion/entidad-form.html',                
                controller: 'editEntidadCtrl as vm' 
            })
            .state('cliente.new', { 
                url: '/nuevo',                 
                templateUrl: 'views/administracion/entidad-form.html',                
                controller: 'newEntidadCtrl as vm' 
            })
            .state('modulos', {
                url: '/modulos', 
                templateUrl: 'views/administracion/modulos.html',
                data: {
                    pageTitle: 'MODULOS',
                    pageHeader: {
                        icon: 'fa fa-file-text',
                        title: 'Módulos',
                        subtitle: 'menús y opciones'
                    },
                    breadcrumbs: [
                        {title: 'Seguridad'},{title: 'Módulos'}
                    ]
                },
                controller: 'formModuloCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {                      
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.modulos',
                                    files: [                                        
                                        'js/modules/administracion/modulos.js',
                                        'app-services/modulos.service.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            .state('categorias', {
                url: '/categorias', 
                templateUrl: 'views/administracion/categorias.html',
                data: {
                    pageTitle: 'CATEGORIAS',
                    pageHeader: {
                        icon: 'fa fa-sitemap',
                        title: 'Categorías',
                        subtitle: 'de productos y servicios'
                    },
                    breadcrumbs: [
                        {title: 'Almacén'},{title: 'Categorías'}
                    ]
                },
                controller: 'formCategoriaCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {                      
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.categorias',
                                    files: [                                        
                                        'js/modules/administracion/categorias.js',
                                        'app-services/categorias.service.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            .state('perfiles', { 
                abstract: true, 
                url: '/perfiles', 
                templateUrl: 'views/administracion/perfiles.html',
                data: {
                    pageTitle: 'PERFILES',
                    pageHeader: {
                        icon: 'fa fa-file-text',
                        title: 'Perfiles',
                        subtitle: 'del sistema'
                    },
                    breadcrumbs: [
                        {title: 'Seguridad'},{title: 'Perfiles'}
                    ]
                },
                controller: 'listPerfilCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {                      
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.perfil',
                                    files: [                                        
                                        'js/modules/administracion/perfil.js',
                                        'app-services/perfil.service.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            .state('perfiles.list', {url: '', template: ''})
            .state('perfiles.edit', { 
                url: '/asignar/:perfilId',                               
                templateUrl: 'views/administracion/perfiles-form.html',                
                controller: 'formPerfilCtrl as vm' 
            })
            .state('producto', {
                abstract: true,
                url: '/productos-y-servicios', 
                templateUrl: 'views/administracion/producto.html',
                data: { 
                    pageTitle: 'PRODUCTOS', 
                    pageHeader: {
                        icon: 'fa fa-cubes',
                        title: 'Productos',
                        subtitle: ''
                    },
                    breadcrumbs: [
                        {title: 'Almacén'},{title: 'Productos'}
                    ]
                },
                controller: 'listProductoCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad',  function($ocLazyLoad) {                       
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.producto',
                                    files: [                                        
                                        'js/modules/administracion/producto.js',
                                        'app-services/producto.service.js',
                                        'app-services/categorias.service.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            .state('producto.list', {url: ''})
            .state('producto.edit', { 
                url: '/editar/:productoId',                               
                templateUrl: 'views/administracion/producto-form.html',                
                controller: 'editProductoCtrl as vm' 
            })
            .state('producto.new', { 
                url: '/nuevo',                 
                templateUrl: 'views/administracion/producto-form.html',                
                controller: 'newProductoCtrl as vm' 
            })
            .state('guia', {
                abstract: true,
                url: '/guia-de-entrada', 
                templateUrl: 'views/administracion/guia.html',
                data: { 
                    pageTitle: 'GUIA ENTRADA', 
                    pageHeader: {
                        icon: 'fa fa-file-text-o',
                        title: 'Guia de entrada',
                        subtitle: 'a almacén'
                    },
                    breadcrumbs: [
                        {title: 'Almacén'},{title: 'Guía de entrada'}
                    ]
                },
                controller: 'listGuiaCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad',  function($ocLazyLoad) {                       
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.guia',
                                    files: [
                                        'js/modules/administracion/guia.js',
                                        'app-services/guia.service.js',
                                        'app-services/producto.service.js' 
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            .state('guia.list', {url: ''})
            .state('guia.edit', {
                url: '/editar/:guiaId',                               
                templateUrl: 'views/administracion/guia-form.html',                
                controller: 'editGuiaCtrl as vm' 
            })
            .state('guia.new', {
                url: '/nuevo',                 
                templateUrl: 'views/administracion/guia-form.html',                
                controller: 'newGuiaCtrl as vm' 
            })
            .state('guiasalida', {
                abstract: true,
                url: '/guia-de-salida', 
                templateUrl: 'views/administracion/guia.html',
                data: { 
                    pageTitle: 'GUIA SALIDA', 
                    pageHeader: {
                        icon: 'fa fa-file-text-o',
                        title: 'Guia de salida',
                        subtitle: 'de almacén'
                    },
                    breadcrumbs: [
                        {title: 'Almacén'},{title: 'Guía de salida'}
                    ]
                },
                controller: 'listGuiaCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad',  function($ocLazyLoad) {                       
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.guia',
                                    files: [
                                        'js/modules/administracion/guia.js',
                                        'app-services/guia.service.js',
                                        'app-services/producto.service.js' 
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            .state('guiasalida.list', {url: ''})
            .state('guiasalida.edit', {
                url: '/editar/:guiaId',                               
                templateUrl: 'views/administracion/guia-form.html',                
                controller: 'editGuiaCtrl as vm' 
            })
            .state('guiasalida.new', {
                url: '/nuevo',                 
                templateUrl: 'views/administracion/guia-form.html',                
                controller: 'newGuiaCtrl as vm'
            })
            .state('kardex', {
                url: '/kardex',
                templateUrl: 'views/administracion/kardex.html',
                data: {
                    pageTitle: 'KARDEX',
                    pageHeader: {
                        icon: 'fa fa-file-text-o',
                        title: 'Kardex',
                        subtitle: 'de producto'
                    },
                    breadcrumbs: [
                        {title: 'Almacén'}, {title: 'Kardex'}
                    ]
                },
                controller: 'listKardexCtrl',
                controllerAs: 'vm',
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {
                        return $ocLazyLoad.load( 
                            [                                
                                {
                                    name: 'blankonApp.kardex',
                                    files: [
                                        'js/modules/administracion/kardex.js',
                                        'app-services/producto.service.js' 
                                    ]                                    
                                }
                            ]
                        );
                    }]
                }
            })
            .state('tratamiento', {
                abstract: true,
                url: '/tratamientos', 
                templateUrl: 'views/administracion/tratamiento.html',
                data: { 
                    pageTitle: 'TRATAMIENTOS', 
                    pageHeader: {
                        icon: 'fa fa-cubes',
                        title: 'Tratamientos',
                        subtitle: 'y servicios'
                    },
                    breadcrumbs: [
                        {title: 'Almacén'},{title: 'Tratamientos'}
                    ]
                },
                controller: 'listTratamientoCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad',  function($ocLazyLoad) {                       
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.tratamiento',
                                    files: [                                        
                                        'js/modules/administracion/tratamiento.js',
                                        'app-services/producto.service.js',
                                        'app-services/categorias.service.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            .state('tratamiento.list', {url: ''})
            .state('tratamiento.edit', {
                url: '/editar/:productoId',                               
                templateUrl: 'views/administracion/tratamiento-form.html',                
                controller: 'editTratamientoCtrl as vm' 
            })
            .state('tratamiento.new', { 
                url: '/nuevo',                 
                templateUrl: 'views/administracion/tratamiento-form.html',                
                controller: 'newTratamientoCtrl as vm' 
            })
            .state('promocion', {
                abstract: true,
                url: '/ofertas-medicas', 
                templateUrl: 'views/administracion/promocion.html',
                data: {
                    pageTitle: 'CAMPAÑAS', 
                    pageHeader: {
                        icon: 'fa fa-file-text-o',
                        title: 'Campañas',
                        subtitle: 'y descuentos'
                    },
                    breadcrumbs: [
                        {title: 'Medicina'}, {title: 'Campañas'}
                    ]
                },
                controller: 'listPromocionCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad',  function($ocLazyLoad) {                       
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.promocion',
                                    files: [
                                        'js/modules/administracion/promocion.js',
                                        'app-services/promocion.service.js',
                                        'app-services/producto.service.js' 
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            .state('promocion.list', {url: ''})
            .state('promocion.edit', {
                url: '/editar/:promocionId/:tipo',                               
                templateUrl: 'views/administracion/promocion-form.html',                
                controller: 'editPromocionCtrl as vm' 
            })
            .state('promocion.new', {
                url: '/nuevo/:tipo',                 
                templateUrl: 'views/administracion/promocion-form.html',                
                controller: 'newPromocionCtrl as vm' 
            })
            .state('alertas', {
                url: '/alertas-tareas', 
                templateUrl: 'views/administracion/alertas.html',
                data: {
                    pageTitle: 'ALERTAS',
                    pageHeader: {
                        icon: 'fa fa-bell-o',
                        title: 'Alertas',
                        subtitle: 'y tareas'
                    },
                    breadcrumbs: [
                        {title: 'Medicina'},{title: 'Alertas'}
                    ]
                },
                controller: 'formAlertaCtrl as vm', 
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {                      
                        return $ocLazyLoad.load(
                            [
                                {
                                    name: 'blankonApp.alerta',
                                    files: [                                        
                                        'js/modules/administracion/alertas.js',                                        
                                        'app-services/entidad.service.js',
                                    ]
                                }
                            ]
                        );
                    }]
                }
            });
    })
    // Init app run
    .run(["$rootScope", "settings", "$state", "$location", "$cookies", "AuthenticationService", "$stateParams", "jwtHelper", function($rootScope, settings, $state, $location, $cookies, AuthenticationService, $stateParams, jwtHelper) {
         
        var urlpage = $location.path().split('/')[1];  
        
        $rootScope.$state = $state; // state to be accessed from view, ej. Titulo de pagina
        $rootScope.$stateParams = $stateParams; 
        $rootScope.settings = settings; // global settings to be accessed from view                
        
//        $rootScope.urlente = $location.host().split('.')[0]; //Subdominio 
//        $rootScope.servidor = 'http://lumenionic.pe/'+$rootScope.urlente; //Server Rest
//        $rootScope.api = 'http://lumenionic.pe';    
        $rootScope.urlente = 'cime'; //Subdominio 
        $rootScope.servidor = 'http://www.lagranescuela.com/apirest/public/'+$rootScope.urlente; //Server Rest
        $rootScope.api = 'http://www.lagranescuela.com/apirest/public';
          
        
        //No autenticado y urlpage es undefined proviene de index.html
        if(!AuthenticationService.authenticate() && urlpage === undefined){
            $location.path('/sign-in'); //alert('urlpage: '+urlpage); 
        } 
        
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            
            console.log('CHANGE EN stateChangeStart DE: '+fromState.name+' A: '+toState.name);
            
            var token = $cookies.get("token") || null;
            var tokexp = tokexp = (token !== null)?jwtHelper.isTokenExpired(token):true;            
            console.log('toexp: '+tokexp + ' token: '+token);
            
            // Si ya se ha autenticado ...
            var isAuthenticated = AuthenticationService.authenticate();
            
            // Cualquier acción publica es permitido
            var isPublicAction = angular.isObject(toState.data)
                               && toState.data.isPublic === true;
                       
            console.log(isPublicAction + ' | ' + isAuthenticated +' = ' + ((isAuthenticated && !isPublicAction) || (!isAuthenticated && isPublicAction)));
            if((isAuthenticated && !isPublicAction) || (!isAuthenticated && isPublicAction)){
                //Detiene ejecucion en metodo, pero continua cambio de estado.               
                return;
            }
            
            //Parar cambio de estado, pero continua ejecucion en metodo 
            event.preventDefault();
            console.log('preventDefault'); 

            // Carga               
            console.log('isAuthenticated: ' + isAuthenticated);
            if(isAuthenticated && isPublicAction){ 
                console.log('go: dashboard'); 
                $state.go("dashboard");
            }else{   
                // log on / sign in...
                console.log('go: signin');  
                $state.go("signin");              
            }            
        });   
        
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            console.log('ERROR EN stateChangeError');
        });
        
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, error) {
            console.log('SUCCESS EN stateChangeSuccess DE: '+fromState.name+' A: '+toState.name);  
            //auth.menuLeft();  
            var isPublicAction = angular.isObject(toState.data)
                               && toState.data.isPublic === true;
             
            if(isPublicAction){
                $rootScope.viewCss = { "body-content" : false, animated : false, fadeIn : false};
                $rootScope.bodyCss = { "page-session" : false, "page-sound" : true, "page-header-fixed" : false, "page-sidebar-fixed" : false};            
                $rootScope.viewLogin = false;
                $rootScope.pageContent = "";
            }else{
                $rootScope.viewCss = { "body-content" : true, animated : true, fadeIn : true};            
                $rootScope.bodyCss = { "page-session" : true, "page-sound" : true, "page-header-fixed" : true, "page-sidebar-fixed" : true};            
                $rootScope.viewLogin = true;
                $rootScope.pageContent = "page-content";//"page-content";
            }
        });
        
        $rootScope.$on('$stateNotFound', function (event, toState, toParams, fromState, fromParams, error) {
            console.log('NOTFOUND EN stateNotFound'); 
        });         
    }]);