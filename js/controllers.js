'use strict';
(function () {
    angular.module('blankonController', [])
            .controller('BlankonCtrl', BlankonCtrl)
            .controller('modalPaswordInstanceCtrl', modalPaswordInstanceCtrl);

    BlankonCtrl.$inject = ['$http', '$scope', '$uibModal', 'entidadService', 'AuthenticationService', '$location', '$log', '$state', '$rootScope', '$cookies'];
    function BlankonCtrl($http, $scope, $uibModal, entidadService, AuthenticationService, $location, $log, $state, $rootScope, $cookies) {

        $scope.logout = logout;
        $scope.loadModulosAuth = loadModulosAuth;

        $scope.searchAPI = function (inputString) {
            return entidadService.GetSearch({likeentidad: inputString});
        };

        $scope.selectedObject = function (selected) {
            if (selected) {
                $state.go("dashboard", {'perfilId': selected.originalObject.identidad});
            }
        };

        function logout() {
            AuthenticationService.ClearCredentials();
            $location.path('/sign-in');
        }

        $scope.abrirModal = function (data) {
            var modalInstance = $uibModal.open({
                templateUrl: 'myModalPasswordContent.html',
                controller: 'modalPaswordInstanceCtrl',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function () {

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


        $scope.supportIE = function () {
            // IE mode
            var isIE8 = false;
            var isIE9 = false;
            var isIE10 = false;

            // initializes main settings for IE
            isIE8 = !!navigator.userAgent.match(/MSIE 8.0/);
            isIE9 = !!navigator.userAgent.match(/MSIE 9.0/);
            isIE10 = !!navigator.userAgent.match(/MSIE 10.0/);

            if (isIE10) {
                $('html').addClass('ie10'); // detect IE10 version
            }

            if (isIE10 || isIE9 || isIE8) {
                $('html').addClass('ie'); // detect IE8, IE9, IE10 version
            }

            // Fix input placeholder issue for IE8 and IE9
            if (isIE8 || isIE9) { // ie8 & ie9
                // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
                $('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {
                    var input = $(this);

                    if (input.val() == '' && input.attr("placeholder") != '') {
                        input.addClass("placeholder").val(input.attr('placeholder'));
                    }

                    input.focus(function () {
                        if (input.val() == input.attr('placeholder')) {
                            input.val('');
                        }
                    });

                    input.blur(function () {
                        if (input.val() == '' || input.val() == input.attr('placeholder')) {
                            input.val(input.attr('placeholder'));
                        }
                    });
                });
            }
        };
        $scope.tooltip = function () {
            if ($('[data-toggle=tooltip]').length) {
                $('[data-toggle=tooltip]').tooltip({
                    animation: 'fade'
                });
            }
        };
        $scope.popover = function () {
            if ($('[data-toggle=popover]').length) {
                $('[data-toggle=popover]').popover();
            }
        };
        // Log view event module loaded
        $scope.$on('ocLazyLoad.moduleLoaded', function (e, params) {
            console.log('event module loaded', params);
        });
        // Log view event component loaded
        $scope.$on('ocLazyLoad.componentLoaded', function (e, params) {
            console.log('event component loaded', params);
        });
        // Log view event file loaded
        $scope.$on('ocLazyLoad.fileLoaded', function (e, file) {
            console.log('event file loaded', file);
        });

        $scope.supportIE(); // Call cookie sidebar minimize 
        $scope.tooltip(); // Call tooltip
        $scope.popover(); // Call popover     

        function loadModulosAuth() {
            if (AuthenticationService.authenticate()) {
                
                $http.defaults.headers.common['AuthorizationToken'] = $cookies.get('token');
                entidadService.GetModulos().then(function (response) {
                    var modulos = response.data.userModules;
                    $rootScope.empresa = response.data.empresa;
                    $rootScope.modules = (typeof modulos[$rootScope.urlente] === 'undefined') ? [] : modulos[$rootScope.urlente].modules;
                    $rootScope.entidad = response.data.userProfiles.empresas[$rootScope.urlente];
                });
            }
        }
        
        loadModulosAuth();
    }
    ;


    modalPaswordInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'entidadService', 'Notification'];
    function modalPaswordInstanceCtrl($scope, $uibModalInstance, modalParam, entidadService, Notification) {

        $scope.entidad = {};
        $scope.entidad.identidad = modalParam.data;
        $scope.entidad.contrasenanueva = '';
        $scope.entidad.contrasenaconfirm = '';
        $scope.confirm = false;

        $scope.save = function () {
            if ($scope.entidad.contrasenanueva !== $scope.entidad.contrasenaconfirm) {
                $scope.miForm.$submitted = false;
                $scope.confirm = true;
                return false;
            }

            entidadService.UpdatePassword($scope.entidad).then(function (entidad) {
                if (entidad.type === 'success') {
                    Notification.primary({message: entidad.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close();
                } else {
                    Notification.error({message: entidad.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();
