'use strict';
(function () {
    angular.module('blankonDirective', [])

            .directive('inputMask', function () {
                return {
                    restrict: 'A',
                    link: function (scope, element) {
                        element.inputmask();
                    }
                }
            })
            .directive('collapsePanel', function () {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        element.click(function () {
                            var targetCollapse = $(this).parents('.panel').find('.panel-body'),
                                    targetCollapse2 = $(this).parents('.panel').find('.panel-sub-heading'),
                                    targetCollapse3 = $(this).parents('.panel').find('.panel-footer')
                            if ((targetCollapse.is(':visible'))) {
                                $(this).find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
                                targetCollapse.slideUp();
                                targetCollapse2.slideUp();
                                targetCollapse3.slideUp();
                            } else {
                                $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
                                targetCollapse.slideDown();
                                targetCollapse2.slideDown();
                                targetCollapse3.slideDown();
                            }
                        });
                    }
                };
            })
            .directive('removePanel', function () {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        element.click(function () {
                            $(this).parents('.panel').fadeOut();
                        });
                    }
                };
            })
            .directive('expandPanel', function () {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        element.click(function () {
                            if (element.parents(".panel").hasClass('panel-fullsize'))
                            {
                                $('body').find('.panel-fullsize-backdrop').remove();
                                element.data('bs.tooltip').options.title = 'Expand';
                                element.parents(".panel").removeClass('panel-fullsize');
                            } else
                            {
                                $('body').append('<div class="panel-fullsize-backdrop"></div>');
                                element.data('bs.tooltip').options.title = 'Minimize';
                                element.parents(".panel").addClass('panel-fullsize');
                            }
                        });
                    }
                };
            })
            .directive('tooltip', function () {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        $(element).hover(function () {
                            // on mouseenter
                            $(element).tooltip('show');
                        }, function () {
                            // on mouseleave
                            $(element).tooltip('hide');
                        });
                    }
                };
            })
            .directive('backTop', function () {
                return {
                    restrict: 'A',
                    link: function (scope, element) {

                        $(window).scroll(function () {
                            if ($(this).scrollTop() > 100) {
                                element.addClass('show animated pulse');
                            } else {
                                element.removeClass('show animated pulse');
                            }
                        });
                    }
                }
            })
            .directive('a', function () {
                return {
                    restrict: 'E',
                    link: function (scope, element, attrs) {
                        if (attrs.ngClick || attrs.href === '' || attrs.href === '#' || element.data('toggle') || element.data('slide')) {
                            element.on('click', function (e) {
                                e.preventDefault(); // prevent link click for above criteria
                            });
                        }
                    }
                }
            })
            .directive('activeMenu', function ($location) {
                return {
                    link: function postLink(scope, element, attrs) {
                        scope.$on("$stateChangeSuccess", function (event, current, previous) {
                            if (attrs.href != undefined) {// this directive is called twice for some reason
                                // this var grabs the tab-level off the attribute, or defaults to 1
                                var pathLevel = attrs.activeTab || 1,
                                        // this var finds what the path is at the level specified
                                        pathToCheck = $location.path().split('/')[pathLevel],
                                        // this var finds grabs the same level of the href attribute
                                        tabLink = attrs.href.split('/')[pathLevel];
                                // now compare the two:
                                if (pathToCheck === tabLink) {

                                    if (element.closest('.submenu').length) {
                                        element.closest('.submenu').addClass('active');
                                        element.closest('.submenu').parents('.submenu').addClass('active');
                                        element.append('<span class="selected"></span>'); // add selected mark
                                    }
                                    element.parent().addClass("active"); // parent to get the <li>
                                    element.append('<span class="selected"></span>'); // add selected mark
                                } else {
                                    element.parent().removeClass("active");
                                    element.find('.selected').remove(); // remove element contain selected mark
                                }
                            }
                        });
                    }
                };
            })
            .directive('collapseMenuClick', ['settings', function (settings) {
                    return {
                        restrict: 'A',
                        link: function (scope, element, attrs) {

                            element.on("click", function () {

                                var parentElementMenu = $(this).parent('li'),
                                        parentElementSubmenu = $(this).parent('.submenu'),
                                        nextElement = $(this).nextAll(),
                                        arrowIcon = $(this).find('.arrow'),
                                        plusIcon = $(this).find('.plus');


                                parentElementMenu.siblings().removeClass('active');

                                if (parentElementSubmenu.parent('ul').find('ul:visible')) {
                                    parentElementSubmenu.parent('ul').find('ul:visible').slideUp('fast');
                                    parentElementSubmenu.parent('ul').find('.open').removeClass('open');
                                    parentElementSubmenu.siblings().children('a').find('.selected').remove();
                                    parentElementMenu.siblings().children('a').find('.selected').remove();
                                }

                                if (nextElement.is('ul:visible')) {
                                    arrowIcon.removeClass('open');
                                    plusIcon.removeClass('open');
                                    nextElement.slideUp('fast');
                                }

                                if (!nextElement.is('ul:visible')) {
                                    nextElement.slideDown('fast');
                                    parentElementMenu.children('a').append('<span class="selected"></span>'); // add selected mark
                                    parentElementSubmenu.addClass('active');
                                    parentElementSubmenu.children('a').append('<span class="selected"></span>');
                                    arrowIcon.addClass('open');
                                    plusIcon.addClass('open');
                                }
                            })
                        }
                    }
                }])
            .directive('collapseMenu', ['settings', function (settings) {
                    return {
                        restrict: 'A',
                        link: function (scope, element, attrs) {

                            element.find('a').on('click', function () {
                                //alert('Click');
                                var parentElementMenu = $(this).parent('li'),
                                        parentElementSubmenu = $(this).parent('.submenu'),
                                        nextElement = $(this).nextAll(),
                                        arrowIcon = $(this).find('.arrow'),
                                        plusIcon = $(this).find('.plus');

                                parentElementMenu.siblings().removeClass('active');

                                if (parentElementSubmenu.parent('ul').find('ul:visible')) {
                                    parentElementSubmenu.parent('ul').find('ul:visible').slideUp('fast');
                                    parentElementSubmenu.parent('ul').find('.open').removeClass('open');
                                    parentElementSubmenu.siblings().children('a').find('.selected').remove();
                                    parentElementMenu.siblings().children('a').find('.selected').remove();
                                }

                                if (nextElement.is('ul:visible')) {
                                    arrowIcon.removeClass('open');
                                    plusIcon.removeClass('open');
                                    nextElement.slideUp('fast');
                                }

                                if (!nextElement.is('ul:visible')) {
                                    nextElement.slideDown('fast');
                                    parentElementMenu.children('a').append('<span class="selected"></span>'); // add selected mark
                                    parentElementSubmenu.addClass('active');
                                    parentElementSubmenu.children('a').append('<span class="selected"></span>');
                                    arrowIcon.addClass('open');
                                    plusIcon.addClass('open');
                                }

                            });

                        }
                    }
                }])
            .directive('sidebarLeftNicescroll', function () {
                return {
                    restrict: 'A',
                    link: function () {
                        function checkHeightSidebar() {
                            if ($('.page-sidebar-fixed').length) {

                                var heightSidebarLeft = $(window).outerHeight() - $('#header').outerHeight() - $('.sidebar-footer').outerHeight() - $('.sidebar-content').outerHeight();
                                $('#sidebar-left .sidebar-menu').height(heightSidebarLeft)
                                        .niceScroll({
                                            cursorwidth: '3px',
                                            cursorborder: '0px',
                                            railalign: 'left'
                                        });
                            }
                        }
                        checkHeightSidebar();
                        $(window).resize(checkHeightSidebar);
                    }
                }
            })
            .directive('copyrightYear', function () {
                return {
                    restrict: 'A',
                    link: function (scope, element) {
                        var today = new Date();
                        $(element).text(today.getFullYear());
                    }
                };
            });

})();