'use strict'; 

angular.module('blankonApp', [
    'ui.router',
    'oc.lazyLoad',
    'angular-loading-bar',
    'ngCookies',
    'ngSanitize',
    'ngAnimate',
    'ui.grid', 'ui.grid.pagination', 
    'ui.grid.resizeColumns', 
    'ui.grid.selection', 
    'ui.grid.edit', 
    'ui.grid.autoResize',
    'ui.grid.cellNav', 
    'angucomplete-alt', 
    'blankonConfig',
    'blankonDirective',
    'blankonController',
    'angular-jwt', 'angular-storage',
    'angularFileUpload',
    'ui.bootstrap',
    'checklist-model',    
    'ngMessages',
    'treeControl'
]);