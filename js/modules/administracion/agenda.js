"use strict";
(function () {
    angular.module('blankonApp.agenda', [])
            .controller('listPlanningCtrl', listPlanningCtrl)
            .controller('newEntidadCtrl', newEntidadCtrl)
            .controller('editEntidadCtrl', editEntidadCtrl)
            .controller('modalEntidadInstanceCtrl', modalEntidadInstanceCtrl)
            .controller('modalPlanningEyeInstanceCtrl', modalPlanningEyeInstanceCtrl)
            .controller('modalContactoInstanceCtrl', modalContactoInstanceCtrl);

    listPlanningCtrl.$inject = ['$scope', '$state', 'GridExternal', '$uibModal', '$timeout', '$log'];
    function listPlanningCtrl($scope, $state,  GridExternal, $uibModal, $timeout, $log) {
        var vm = this;

        vm.filter = {};
        vm.stateparent = $state.$current.parent.self.name;
        vm.filter.tipoentidad = $state.$current.parent.self.name;
        $scope.$on('handleBroadcast', function () {
            vm.getPage();
        });

        vm.searchNombre = false;
        vm.searchTelefono = true;
        vm.searchHistoria = true;

        switch (vm.stateparent) {
            case 'planning':
            case 'cliente':
            case 'personal':
                vm.entidaddesc = 'Apellidos y nombres';
                break;
            case 'proveedor':
                vm.entidaddesc = 'Razón social';
                break;
            default:
        }

        function loadEntidades() {
            var columnsDefs = [
                {name: 'Doc', field: 'documentoabrev', width: '42'},
                {name: 'Número', field: 'numerodoc', width: '100'},
                {name: vm.entidaddesc, field: 'entidad', width: '200'},
                {name: 'Teléfono', field: 'telefono', width: '80'},
                {name: 'Correo electrónico', field: 'email', width: '180'}
            ];
            switch (vm.stateparent) {
                case 'planning':
                // case 'personal':
                //     columnsDefs.push({name: 'Cargo desempeño', field: 'nombre', width: '*', enableSorting: false});
                //     columnsDefs.push({name: 'Nacimiento', field: 'fechanacimiento', width: '90'});
                //     break;
                // case 'cliente':
                //     columnsDefs.push({name: 'Dirección', field: 'direccion', width: '*', enableSorting: false});
                //     columnsDefs.push({name: 'Nacimiento', field: 'fechanacimiento', width: '90'});
                //     break;
                // case 'proveedor':
                //     columnsDefs.push({name: 'Dirección', field: 'direccion', width: '*', enableSorting: false});
                //     columnsDefs.push({name: 'Celular', field: 'celular', width: '90'});
                //     break;
                default:
            }

            // GridExternal.getGrid(vm, columnsDefs, $scope, function () {
            //     entidadService.GetIndex(vm.filter).then(function (entidades) {

            //         vm.gridOptions.totalItems = entidades.total;
            //         vm.gridOptions.data = entidades.data;
            //         $timeout(function () {
            //             if (vm.gridApi.selection.selectRow) {
            //                 vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
            //             }
            //         });
            //         GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, entidades.data.length);
            //     });
            // });
        }

        vm.openModal = function (accion, data) {

            var templateUrl = 'myModalContent.html';
            var controller = 'modalEntidadInstanceCtrl';
            var size = 'sm';

            if (accion === 'eye') {
                controller = 'modalPlanningEyeInstanceCtrl as vm';
                templateUrl = 'myModalEyeContent.html';
                size = 'lg';
            }

            var modalInstance = $uibModal.open({
                templateUrl: templateUrl,
                controller: controller,
                windowClass: 'modal-primary',
                size: size,
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data,
                            stateparent: vm.stateparent
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload)
                    loadEntidades();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        loadEntidades();
    }

    newEntidadCtrl.$inject = ['$scope', '$state', 'entidadService', 'ubigeoService', 'Notification'];
    function newEntidadCtrl($scope, $state, entidadService, ubigeoService, Notification) {
        var vm = this;

        vm.stateparent = $state.$current.parent.self.name;
        vm.entidadespecialidad = [];
        vm.entidadsede = [];
        vm.entidad = {};
        vm.edicion = false;

        vm.descripcion = '';
        switch (vm.stateparent) {
            case 'personal':
                vm.descripcion = 'Registrarlo como personal';
                vm.entidad.iddocumento = 1;
                vm.requiredNombre = true;
                break;
            case 'cliente':
                vm.descripcion = 'Registrarlo como paciente';
                vm.entidad.iddocumento = 1;
                vm.requiredNombre = true;
                break;
            case 'medico':
                vm.descripcion = 'Registrarlo como médico';
                vm.entidad.iddocumento = 1;
                vm.requiredNombre = true;
                break;
            case 'proveedor':
                vm.descripcion = 'Registrarlo como proveedor';
                vm.entidad.iddocumento = 2;
                vm.requiredNombre = false;
                break;
            default:
        }

        vm.iddocumentoChange = function () {
            if (vm.entidad.iddocumento === 1 || vm.entidad.iddocumento === 3) {
                vm.requiredNombre = true;
                vm.entidad.razonsocial = '';
            }
            if (vm.entidad.iddocumento === 2) {
                vm.requiredNombre = false;
                vm.entidad.apellidopat = '';
                vm.entidad.apellidomat = '';
                vm.entidad.nombre = '';
            }
        };

        vm.validacionDocumento = {};
        vm.getDocumento = function () {
            if ($scope.miForm.numerodoc.$valid) {
                entidadService.GetNumero({numerodoc: vm.entidad.numerodoc, tipoentidad: vm.stateparent}).then(function (entidades) {
                    vm.validacionDocumento = entidades.data;
                });
            }
        };

        vm.getDocumentoChange = function () {
            if ($scope.miForm.numerodoc.$error) {
                vm.validacionDocumento = {};
            }
        };

        vm.registrarSubEntidad = function () {
            entidadService.UpdateEntidad({identidad: vm.validacionDocumento.idEntidad, tipoentidad: vm.stateparent}).then(function (entidades) {

                if (entidades.type === 'success') {
                    Notification.primary({message: entidades.data, title: '<i class="fa fa-check"></i>'});
                    $scope.$emit('handleBroadcast');
                    $state.go('^.list'); //estado hermano
                } else {
                    Notification.error({message: entidades.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        vm.changeUbigeo = function (ubigeo, to) {
            if (ubigeo.pais === '' || ubigeo.dpto === '' || ubigeo.prov === '') {
                vm.others[to] = [];
                return false;
            }
            if (to === 'departamentos') {
                vm.entidad.dpto = ''; //Por default '- Provincia -'
                vm.others.provincias = []; //Limpiar distritos
            }
            if (to === 'provincias') {
                vm.entidad.prov = ''; //Por default '- Provincia -'
                vm.others.distritos = []; //Limpiar distritos
            }
            if (to === 'distritos') {
                vm.entidad.dist = ''; //Por default '- Distrito -' 
            }
            ubigeoService.GetIndex(ubigeo).then(function (result) {
                vm.others[to] = result.data;
            });
        };

        entidadService.GetNew({tipoentidad: vm.stateparent}).then(function (entidades) {
            vm.others = entidades.others;
            //Inicializar con España
            vm.entidad.pais = 'ES';
            vm.changeUbigeo({pais: vm.entidad.pais || ''}, 'departamentos');
        });

        vm.save = function () {
            vm.OBJentidad = {
                entidad: vm.entidad,
                medico: vm.medico,
                entidadespecialidad: vm.entidadespecialidad,
                entidadsede: vm.entidadsede,
                tipoentidad: vm.stateparent// Tablas: cliente, proveedor, medico, personal 
            };

            entidadService.Create(vm.OBJentidad).then(function (entidades) {
                if (entidades.type === 'success') {
                    Notification.primary({message: entidades.data, title: '<i class="fa fa-check"></i>'});
                    $scope.$emit('handleBroadcast');
                    $state.go('^.list');
                } else {
                    Notification.error({message: entidades.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };
    }

    editEntidadCtrl.$inject = ['$scope', '$state', 'entidadService', 'ubigeoService', 'contactoService', '$stateParams', 'Notification', '$uibModal', '$log'];
    function editEntidadCtrl($scope, $state, entidadService, ubigeoService, contactoService, $stateParams, Notification, $uibModal, $log) {
        var vm = this;
        var before = {};
        var after = {};
        var fields = ['numerodoc', 'apellidopat', 'apellidomat', 'nombre', 'telefono', 'celular', 'email', 'idcargoorg', 'fechanacimiento', 'razonsocial', 'direccion'];

        vm.stateparent = $state.$current.parent.self.name;
        vm.entidadespecialidad = [];
        vm.entidadsede = [];
        vm.entidad = {};

        vm.edicion = true;

        vm.iddocumentoChange = function () {
            if (vm.entidad.iddocumento === 1 || vm.entidad.iddocumento === 3) {
                vm.requiredNombre = true;
            }
            if (vm.entidad.iddocumento === 2) {
                vm.requiredNombre = false;
            }
        };

        vm.changeUbigeo = function (ubigeo, to) {
            if (ubigeo.pais === '' || ubigeo.dpto === '' || ubigeo.prov === '') {
                vm.others[to] = [];
                return false;
            }
            if (to === 'departamentos') {
                vm.entidad.dpto = ''; //Por default '- Provincia -'
                vm.others.provincias = []; //Limpiar distritos
            }
            if (to === 'provincias') {
                vm.entidad.prov = ''; //Por default '- Provincia -'
                vm.others.distritos = []; //Limpiar distritos
            }
            if (to === 'distritos') {
                vm.entidad.dist = ''; //Por default '- Distrito -' 
            }
            ubigeoService.GetIndex(ubigeo).then(function (result) {
                vm.others[to] = result.data;
            });
        };

        entidadService.GetShow($stateParams.entidadId, {tipoentidad: vm.stateparent}).then(function (entidades) {

            var entidadespecialidad = [];
            angular.forEach(entidades.others.entidadespecialidad, function (value, key) {
                this.push({idespecialidad: value.idespecialidad, nombre: value.nombre});
            }, entidadespecialidad);

            var entidadsede = [];
            angular.forEach(entidades.others.entidadsede, function (value, key) {
                this.push({idsede: value.idsede, nombre: value.nombre});
            }, entidadsede);

            vm.entidad = entidades.data;
            vm.others = entidades.others;
            vm.cliente = entidades.others.clientes;
            vm.personal = entidades.others.personales;
            vm.proveedor = entidades.others.proveedores;
            vm.medico = entidades.others.medicos;
            vm.entidadespecialidad = entidadespecialidad;
            vm.entidadsede = entidadsede;
            before = angular.copy(vm.entidad);
            vm.iddocumentoChange();

            //Inicializar con España            
            if (typeof vm.entidad.pais === 'undefined') {
                vm.entidad.pais = 'ES';
                vm.changeUbigeo({pais: vm.entidad.pais || ''}, 'departamentos');
            }
        });

        vm.save = function () {
            vm.OBJentidad = {
                entidad: vm.entidad,
                cliente: vm.cliente,
                personal: vm.personal,
                proveedor: vm.proveedor,
                medico: vm.medico,
                entidadespecialidad: vm.entidadespecialidad,
                entidadsede: vm.entidadsede,
                tipoentidad: vm.stateparent// Tablas: cliente, proveedor, medico, personal 
            };

            after = angular.copy(vm.entidad);
            entidadService.Update(vm.OBJentidad).then(function (entidades) {

                if (entidades.type === 'success') {
                    Notification.primary({message: entidades.data, title: '<i class="fa fa-check"></i>'});
                    vm.brodcast();
                    $state.go('^.list');
                } else {
                    Notification.error({message: entidades.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        vm.brodcast = function () {
            for (var i = 0, len = fields.length; i < len; i++) {
                if (before[fields[i]] !== after[fields[i]]) {
                    $scope.$emit('handleBroadcast');
                    break;
                }
            }
        };

        function loadContactos() {
            contactoService.GetIndex({identidad: $stateParams.entidadId}).then(function (contactos) {
                vm.others.contactos = contactos.data;
            });
        }

        vm.openModal = function (accion, data) {

            data.identidad = $stateParams.entidadId;

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContacto.html',
                controller: 'modalContactoInstanceCtrl',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: angular.copy(data)
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload)
                    loadContactos();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }

    modalEntidadInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'entidadService', 'Notification'];
    function modalEntidadInstanceCtrl($scope, $uibModalInstance, modalParam, entidadService, Notification) {

        $scope.entidad = {};
        $scope.entidad = {
            identidad: modalParam.data.identidad,
            entidad: modalParam.data.entidad
        };

        $scope.save = function () {
            entidadService.Delete($scope.entidad.identidad, {tipoentidad: modalParam.stateparent}).then(function (entidad) {
                var reload = false;
                if (entidad.type === 'success') {
                    reload = true;
                    Notification.primary({message: entidad.data, title: '<i class="fa fa-check"></i>'});
                } else {
                    Notification.error({message: entidad.data, title: '<i class="fa fa-ban"></i>'});
                }
                $uibModalInstance.close(reload);
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalContactoInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'contactoService', 'Notification'];
    function modalContactoInstanceCtrl($scope, $uibModalInstance, modalParam, contactoService, Notification) {

        $scope.contacto = {};
        $scope.contacto.identidad = modalParam.data.identidad;

        if (modalParam.accion !== 'new') {
            $scope.contacto = modalParam.data;
        }

        $scope.accion = modalParam.accion === 'delete' ? true : false;

        $scope.save = function () {
            if (modalParam.accion === 'new') {
                contactoService.Create($scope.contacto).then(function (contactos) {
                    Notification.primary({message: contactos.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close(true);
                });
            }

            if (modalParam.accion === 'edit') {
                contactoService.Update($scope.contacto).then(function (contactos) {
                    Notification.primary({message: contactos.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close(true);
                });
            }

            if (modalParam.accion === 'delete') {
                contactoService.Delete($scope.contacto.idcontacto).then(function (contactos) {
                    var reload = false;
                    if (contactos.type === 'success') {
                        reload = true;
                        Notification.primary({message: contactos.data, title: '<i class="fa fa-check"></i>'});
                    } else {
                        Notification.error({message: contactos.data, title: '<i class="fa fa-ban"></i>'});
                    }
                    $uibModalInstance.close(reload);
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalPlanningEyeInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', '$state'];
    function modalPlanningEyeInstanceCtrl($scope, $uibModalInstance, modalParam,  $state) {

        var vm = this;
        vm.stateparent = $state.$current.parent.self.name;

        vm.titulo = '';
        switch (vm.stateparent) {
            case 'planning':
                vm.titulo = 'Planning';
                break;
            case 'cliente':
                vm.titulo = 'Paciente';
                break;
            case 'medico':
                vm.titulo = 'Médico';
                break;
            case 'proveedor':
                vm.titulo = 'Proveedor';
                break;
            default:
        }

        entidadService.GetProfile(modalParam.data).then(function (entidades) {
            vm.entidad = entidades.data;
            vm.others = entidades.others;
            vm.cliente = entidades.others.clientes;
            vm.personal = entidades.others.personales;
            //vm.proveedor = entidades.others.proveedores;
            vm.medico = entidades.others.medicos;
            vm.entidadespecialidad = entidades.others.entidadespecialidad;
            vm.entidadsede = entidades.others.entidadsede;
        });

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();