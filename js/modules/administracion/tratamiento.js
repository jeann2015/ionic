"use strict";
(function () {
    angular.module('blankonApp.tratamiento', [])
            .controller('listTratamientoCtrl', listTratamientoCtrl)
            .controller('newTratamientoCtrl', newTratamientoCtrl)
            .controller('editTratamientoCtrl', editTratamientoCtrl)
            .controller('modalSelectProductoTratamientoInstanceCtrl', modalSelectProductoTratamientoInstanceCtrl)
            .controller('modalProductoInstanceCtrl', modalProductoInstanceCtrl)
            .controller('modalCategoriaProductoInstanceCtrl', modalCategoriaProductoInstanceCtrl)
            .factory('GridInternoTratamiento', GridInternoTratamiento)
            .factory('ModalesInternoTratamiento', ModalesInternoTratamiento);

    function GridInternoTratamiento() {

        var service = {};
        service.getGrid = getGrid;
        service.setData = setData;

        function setData(vm, data) {
            vm.miBoleta.data = data;
            if (data.length <= 10) {
                vm.addRow(10 - data.length);
            }
        }

        function getGrid(vm, $scope) {
            var accion = '<div class="btn-group">' +
                    '<a href="#" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteRow(row);"><i class="fa fa-close"></i></a>' +
                    '<a href="#" class="btn btn-primary btn-xs" ng-click="grid.appScope.openModalProducto(row);"><i class="fa fa-pencil"></i></a>' +
                    '</div>';

            var addItem = '<div class="btn-group" style="padding-left: 5px; padding-top: 5px;"><a href="#" class="btn btn-primary btn-xs" ng-click="grid.appScope.addRow(1)"><i class="fa fa-plus"></i> &Iacute;tem</a></div>';

            vm.columnDefs = [
                {displayName: 'N°', field: 'sequence', width: '32', type: 'number'},
                {displayName: '', name: 'edit', width: '60', enableSorting: false, headerCellTemplate: addItem, cellClass: 'text-center', cellTemplate: accion},
                {displayName: 'Código', field: 'codigo', width: '100'},
                {displayName: 'Producto', field: 'producto', width: '*'},
                {displayName: 'Unidad', field: 'unidadabrev', width: '100'}
            ];

            vm.miBoleta = {
                enableSorting: true,
                columnDefs: vm.columnDefs,
                enableColumnMenus: false
            };

            vm.miBoleta.appScopeProvider = vm;

            vm.miBoleta.onRegisterApi = function (gridApi) {
                vm.gridApiBoleta = gridApi;
            };

            vm.deleteRow = function (row) {
                var index = vm.miBoleta.data.indexOf(row.entity);
                vm.miBoleta.data.splice(index, 1);
            };

            vm.addRow = function (items) {
                var length = vm.miBoleta.data.length;
                var sequenceact = (length > 0) ? vm.miBoleta.data[length - 1].sequence : 0;
                for (var i = 1; i <= items; i++) {
                    vm.miBoleta.data.push({sequence: sequenceact + i});
                }
            };
        }

        return service;
    }
    ;

    function ModalesInternoTratamiento() {

        var service = {};
        service.openModal = openModal;

        function openModal(vm, $uibModal, $log) {
            vm.openModalProducto = function (data) {

                var items = [];
                vm.miBoleta.data.forEach(function (row) {
                    if (typeof row.idproducto !== 'undefined') {
                        items.push(row.idproducto);
                    }
                });

                var modalInstance = $uibModal.open({
                    templateUrl: 'myModalSelectProductoContent.html',
                    controller: 'modalSelectProductoTratamientoInstanceCtrl as vm',
                    windowClass: 'modal-primary',
                    resolve: {
                        modalParam: function () {
                            return {
                                data: items
                            };
                        }
                    }
                });

                modalInstance.result.then(function (reload) {

                    var indexof = vm.miBoleta.data.indexOf(data.entity);
                    var sequence = data.entity.sequence;

                    vm.miBoleta.data.splice(indexof, 1, {
                        sequence: sequence,
                        idproducto: reload.idproducto,
                        codigo: reload.codigo,
                        producto: reload.nombre,
                        unidadabrev: reload.unidadmedidaabrev
                    });

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });

            };
        }
        ;

        return service;
    }
    ;

    listTratamientoCtrl.$inject = ['$scope', '$state', 'productoService', 'GridExternal', '$uibModal', '$timeout', '$log'];
    function listTratamientoCtrl($scope, $state, productoService, GridExternal, $uibModal, $timeout, $log) {
        var vm = this;

        vm.filter = {};
        $scope.$on('handleBroadcast', function () {
            vm.getPage();
        });

        vm.filter.idtipoproducto = 2; //Servicio 
        function loadProductos() {
            var columnsDefs = [
                {displayName: 'Categoría', field: 'categoria', width: '*'},
                {displayName: 'Código', field: 'codigo', width: '70'},
                {displayName: 'Tratamiento', field: 'nombre', width: '350'},
                {displayName: 'BASE', field: 'valorventabase', width: '70'},
                {displayName: 'IVA 21%', field: 'valorventaigv', width: '70'},
                {displayName: 'PVP', field: 'valorventa', width: '70'}
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                productoService.GetIndex(vm.filter).then(function (productos) {
                    vm.gridOptions.totalItems = productos.total;
                    vm.gridOptions.data = productos.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, productos.data.length);
                });
            });
        }

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: 'modalProductoInstanceCtrl',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload)
                    loadProductos();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        loadProductos();
    }

    newTratamientoCtrl.$inject = ['$scope', '$state', 'productoService', 'Notification', '$uibModal', '$log', 'GridInternoTratamiento', 'ModalesInternoTratamiento'];
    function newTratamientoCtrl($scope, $state, productoService, Notification, $uibModal, $log, GridInternoTratamiento, ModalesInternoTratamiento) {
        var vm = this;

        vm.producto = {};
        vm.edicion = false;
        vm.producto.idtipoproducto = 2;
        vm.producto.idmonedaventa = 1;
        vm.producto.activo = 1;

        vm.changeValorventabase = function () {
            vm.producto.valorventaigv = redondearValor(vm.producto.valorventabase * 0.21, 2);
            vm.producto.valorventa = redondearValor(vm.producto.valorventabase * 1.21, 0);
        };

        vm.changeValorventa = function () {
            vm.producto.valorventabase = redondearValor(vm.producto.valorventa / 1.21, 2);
            vm.producto.valorventaigv = redondearValor(vm.producto.valorventabase * 0.21, 2);
        };

        GridInternoTratamiento.getGrid(vm, $scope);
        GridInternoTratamiento.setData(vm, []);
        ModalesInternoTratamiento.openModal(vm, $uibModal, $log);

        productoService.GetNew().then(function (productos) {
            vm.others = productos.others;
        });

        vm.save = function () {

            var productoservicio = [];

            vm.miBoleta.data.forEach(function (row) {
                if (typeof row.idproducto !== 'undefined') {
                    productoservicio.push({idproducto: row.idproducto});
                }
            });

            vm.OBJproducto = {
                producto: vm.producto,
                productoservicio: productoservicio
            };

            productoService.Create(vm.OBJproducto).then(function (productos) {
                if (productos.type === 'success') {
                    Notification.primary({message: productos.data, title: '<i class="fa fa-check"></i>'});
                    $scope.$emit('handleBroadcast');
                    $state.go('^.list');
                } else {
                    Notification.error({message: productos.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalCategoriaContent.html',
                controller: 'modalCategoriaProductoInstanceCtrl',
                windowClass: 'modal-primary',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                vm.producto.idarbol = reload[0];
                vm.producto.categoria = reload[1];
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }

    editTratamientoCtrl.$inject = ['$scope', '$state', 'productoService', '$stateParams', 'Notification', '$uibModal', '$log', 'GridInternoTratamiento', 'ModalesInternoTratamiento'];
    function editTratamientoCtrl($scope, $state, productoService, $stateParams, Notification, $uibModal, $log, GridInternoTratamiento, ModalesInternoTratamiento) {
        var vm = this;
        var before = {};
        var after = {};
        var fields = ['categoria', 'nombre', 'codigo', 'idunidadmedidda', 'idmonedacompra', 'valorcompra', 'idmonedaventa', 'valorventa'];

        vm.producto = {};
        vm.edicion = true;

        vm.changeValorventabase = function () {
            vm.producto.valorventaigv = redondearValor(vm.producto.valorventabase * 0.21, 2);
            vm.producto.valorventa = redondearValor(vm.producto.valorventabase * 1.21, 0);
        };

        vm.changeValorventa = function () {
            vm.producto.valorventabase = redondearValor(vm.producto.valorventa / 1.21, 2);
            vm.producto.valorventaigv = redondearValor(vm.producto.valorventabase * 0.21, 2);
        };

        GridInternoTratamiento.getGrid(vm, $scope);
        ModalesInternoTratamiento.openModal(vm, $uibModal, $log);

        productoService.GetShow($stateParams.productoId).then(function (productos) {
            vm.producto = productos.data;
            vm.others = productos.others;

            productos.others.productoservicios.forEach(function (row, index) {
                row.sequence = index + 1;
            });

            before = angular.copy(vm.producto);
            GridInternoTratamiento.setData(vm, productos.others.productoservicios);
        });

        vm.save = function () {

            var productoservicio = [];

            vm.miBoleta.data.forEach(function (row) {
                if (typeof row.idproducto !== 'undefined') {
                    productoservicio.push({idproducto: row.idproducto});
                }
            });

            vm.OBJproducto = {
                producto: vm.producto,
                productoservicio: productoservicio
            };

            after = angular.copy(vm.producto);
            productoService.Update(vm.OBJproducto).then(function (productos) {
                if (productos.type === 'success') {
                    Notification.primary({message: productos.data, title: '<i class="fa fa-check"></i>'});
                    vm.brodcast();
                    $state.go('^.list'); //estado hermano
                } else {
                    Notification.error({message: productos.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        vm.brodcast = function () {
            for (var i = 0, len = fields.length; i < len; i++) {
                if (before[fields[i]] !== after[fields[i]]) {
                    $scope.$emit('handleBroadcast');
                    break;
                }
            }
        };

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalCategoriaContent.html',
                controller: 'modalCategoriaProductoInstanceCtrl',
                windowClass: 'modal-primary',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                vm.producto.idarbol = reload[0];
                vm.producto.categoria = reload[1];
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }

    modalProductoInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'productoService', 'Notification'];
    function modalProductoInstanceCtrl($scope, $uibModalInstance, modalParam, productoService, Notification) {

        $scope.producto = {};
        $scope.producto = {
            idproducto: modalParam.data.idproducto,
            nombre: modalParam.data.nombre
        };

        $scope.save = function () {
            productoService.Delete($scope.producto.idproducto).then(function (productos) {
                var reload = false;
                if (productos.type === 'success') {
                    reload = true;
                    Notification.primary({message: productos.data, title: '<i class="fa fa-check"></i>'});
                } else {
                    Notification.error({message: productos.data, title: '<i class="fa fa-ban"></i>'});
                }
                $uibModalInstance.close(reload);
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalCategoriaProductoInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'categoriaService'];
    function modalCategoriaProductoInstanceCtrl($scope, $uibModalInstance, modalParam, categoriaService) {

        $scope.treeOptions = {
            nodeChildren: "children",
            dirSelectable: false,
            allowDeselect: true
        };

        $scope.titulo = (modalParam.data.idtipoproducto === 1) ? 'Productos' : 'Servicios'
        $scope.idseleccionado = modalParam.data.idarbol;
        $scope.seleccionado = modalParam.data.categoria;

        $scope.nodeSelect = function (node, selected) {
            $scope.idseleccionado = selected ? node.idarbol : '';
            $scope.seleccionado = selected ? node.nombre : '';
            console.log(selected);
        };

        categoriaService.GetIndex({idcategoria: modalParam.data.idtipoproducto}).then(function (categorias) {
            $scope.categorias = categorias.data;
        });

        $scope.save = function () {
            var reload = [$scope.idseleccionado, $scope.seleccionado];
            $uibModalInstance.close(reload);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalSelectProductoTratamientoInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'productoService', 'GridExternal', '$timeout'];
    function modalSelectProductoTratamientoInstanceCtrl($scope, $uibModalInstance, modalParam, productoService, GridExternal, $timeout) {

        var vm = this;
        vm.filter = {};

        var funcAlert = function (grid, row) {
            return (row.entity.stockalert) ? 'alertStock' : '';
        };

        function loadProductos() {
            var columnsDefs = [
                {displayName: 'Producto', field: 'nombre', width: '*'},
                {displayName: 'Unidad', field: 'unidadmedidaabrev', width: '80'},
                {displayName: 'Tipo stock', field: 'tipostock', width: '80'},
                {displayName: 'Stock', field: 'stock', width: '50', enableSorting: false, cellClass: funcAlert},
                {displayName: '', field: 'editor', width: '100', enableSorting: false, cellClass: 'text-center',
                    cellTemplate: '<a href="#" class="btn btn-primary btn-xs" ng-if="grid.appScope.filtroRow(row.entity);" ng-click="grid.appScope.seleccionar(row.entity);">Seleccionar</a>'
                }
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                productoService.GetIndexStock(vm.filter).then(function (productos) {
                    vm.gridOptions.totalItems = productos.total;
                    vm.gridOptions.data = productos.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, productos.data.length);
                });
            });
        }

        loadProductos();

        vm.seleccionar = function (row) {
            $uibModalInstance.close(row);
        };

        vm.filtroRow = function (row) {
            return modalParam.data.indexOf(row.idproducto) === -1 ? true : false;
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function redondearValor(valor, precision) {
        if (precision > 0) {
            var rounded = Math.round(parseFloat((valor * Math.pow(10, precision)).toFixed(precision))) / Math.pow(10, precision);
            return rounded.toFixed(precision);
        } else {
            return Math.round(valor);
        }
    }

})();