"use strict";
(function () {
    angular.module('blankonApp.alerta', ['ui.bootstrap'])
            .controller('formAlertaCtrl', formAlertaCtrl);

    formAlertaCtrl.$inject = ['entidadService', '$rootScope', '$uibModal', '$log'];
    function formAlertaCtrl(entidadService) {
        var vm = this;

        vm.cumpleanos = [];
        function loadCumpleanos() {
            entidadService.GetCumpleanos({}).then(function (cumpleanos) { 
                vm.cumpleanos = cumpleanos.data;
            });
        }
        loadCumpleanos();
    }
})();