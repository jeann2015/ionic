"use strict";
(function () {
    angular.module("blankonApp.empresa", [])
            .controller('editEmpresaCtrl', editEmpresaCtrl)
            .controller('modalEmpresaInstanceCtrl', modalEmpresaInstanceCtrl);

    editEmpresaCtrl.$inject = ['$scope', '$uibModal', 'enterpriseService', 'ubigeoService', 'sedeService', '$log', 'Notification'];
    function editEmpresaCtrl($scope, $uibModal, enterpriseService, ubigeoService, sedeService, $log, Notification) {

        var vm = this;

        enterpriseService.GetShow().then(function (empresas) {
            vm.empresa = empresas.data;
            vm.others = empresas.others;
        });

        function loadSedes() {
            sedeService.GetIndex().then(function (sedes) {
                vm.others.sedes = sedes.data;
            });
        }

        vm.changeUbigeo = function (ubigeo, to) {
            if (ubigeo.pais === '' || ubigeo.dpto === '' || ubigeo.prov === '') {
                vm.others[to] = [];
                return false;
            }
            if (to === 'departamentos') {
                vm.empresa.dpto = ''; //Por default '- Provincia -'
                vm.others.provincias = []; //Limpiar distritos
            }
            if (to === 'provincias') {
                vm.empresa.prov = ''; //Por default '- Provincia -'
                vm.others.distritos = []; //Limpiar distritos
            }
            if (to === 'distritos') {
                vm.empresa.dist = ''; //Por default '- Distrito -' 
            }
            ubigeoService.GetIndex(ubigeo).then(function (result) {
                vm.others[to] = result.data;
            });
        };

        vm.save = function () {
            enterpriseService.Update(vm.empresa).then(function (empresas) {
                if (empresas.type === 'success') {
                    Notification.primary({message: empresas.data, title: '<i class="fa fa-check"></i>'});
                } else {
                    Notification.error({message: empresas.data, title: '<i class="fa fa-ban"></i>'});
                }
                $scope.miForm.$submitted = false;
            });
        };

        vm.openModal = function (accion, data) {
            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: 'modalEmpresaInstanceCtrl',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data,
                            personal: vm.others.personal
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload)
                    loadSedes();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }

    modalEmpresaInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'sedeService', 'Notification'];
    function modalEmpresaInstanceCtrl($scope, $uibModalInstance, modalParam, sedeService, Notification) {

        $scope.sede = {};
        $scope.sede.principal = '0';
        $scope.esprincipal = '0';

        if (modalParam.accion !== 'new') {
            $scope.sede = {
                idsede: modalParam.data.idsede,
                nombre: modalParam.data.nombre,
                direccion: modalParam.data.direccion,
                telefono: modalParam.data.telefono,
                celular: modalParam.data.celular,
                identidad: modalParam.data.identidad,
                principal: modalParam.data.principal
            };
            $scope.esprincipal = modalParam.data.principal;
        }
        $scope.others = {};
        $scope.others.personal = modalParam.personal;

        $scope.accion = modalParam.accion === 'delete' ? true : false;

        $scope.save = function () {
            if (modalParam.accion === 'new') {
                sedeService.Create($scope.sede).then(function (sedes) {
                    Notification.primary({message: sedes.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close(true);
                });
            }

            if (modalParam.accion === 'edit') {
                sedeService.Update($scope.sede).then(function (sedes) {
                    Notification.primary({message: sedes.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close(true);
                });
            }

            if (modalParam.accion === 'delete') {
                sedeService.Delete($scope.sede.idsede).then(function (sedes) {
                    var reload = false;
                    if (sedes.type === 'success') {
                        reload = true;
                        Notification.primary({message: sedes.data, title: '<i class="fa fa-check"></i>'});
                    } else {
                        Notification.error({message: sedes.data, title: '<i class="fa fa-ban"></i>'});
                    }
                    $uibModalInstance.close(reload);
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();