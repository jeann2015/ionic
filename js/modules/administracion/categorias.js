"use strict";
(function () {
    angular.module('blankonApp.categoria', ['ui.bootstrap'])
            .controller('formCategoriaCtrl', formCategoriaCtrl)
            .controller('modalCategoriaInstanceCtrl', modalCategoriaInstanceCtrl);

    formCategoriaCtrl.$inject = ['categoriaService', '$uibModal', '$log'];
    function formCategoriaCtrl(categoriaService,  $uibModal, $log) {
        var vm = this;

        vm.treeOptions = {
            nodeChildren: "children",
            dirSelectable: true
        };

        vm.showSelected = function (sel) {
           
        };

        function loadProductos() {
            categoriaService.GetIndex({idcategoria: '1'}).then(function (categorias) {
                vm.productos = categorias.data;
            });
        }

        function loadServicios() {
            categoriaService.GetIndex({idcategoria: '2'}).then(function (categorias) {
                vm.servicios = categorias.data;
            });
        }

        vm.openModal = function (tipo, accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: 'modalCategoriaInstanceCtrl',
                windowClass: 'modal-primary',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data,
                            tipo: tipo
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {

                if (reload[0]) {
                    if (reload[1] === '1') {
                        loadProductos();
                    }

                    if (reload[1] === '2') {
                        loadServicios();
                    }
                }
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        loadProductos();
        loadServicios();
    }

    modalCategoriaInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'categoriaService', 'Notification'];
    function modalCategoriaInstanceCtrl($scope, $uibModalInstance, modalParam, categoriaService, Notification) {

        $scope.categoria = {};
        $scope.categoria.idcategoria = modalParam.tipo;

        if (modalParam.tipo === '1') {
            $scope.titulo = 'Categoría producto';
        }

        if (modalParam.tipo === '2') {
            $scope.titulo = 'Categoría servicio';
        }

        if (modalParam.accion === 'new' && typeof (modalParam.data) !== 'undefined') {
            $scope.categoria.parent = modalParam.data.idarbol;
        }

        if (modalParam.accion !== 'new') {
            $scope.categoria = {
                idarbol: modalParam.data.idarbol,
                parent: modalParam.data.parent,
                nombre: modalParam.data.nombre
            };
        }

        $scope.accion = modalParam.accion === 'delete' ? true : false;
        $scope.save = function () {
            if (modalParam.accion === 'new') {
                categoriaService.Create($scope.categoria).then(function (categorias) {
                    Notification.primary({message: categorias.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close([true, modalParam.tipo]);
                });
            }

            if (modalParam.accion === 'edit') {
                categoriaService.Update($scope.categoria).then(function (categorias) {
                    Notification.primary({message: categorias.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close([true, modalParam.tipo]);
                });
            }

            if (modalParam.accion === 'delete') {
                categoriaService.Delete($scope.categoria.idarbol, {idcategoria: modalParam.tipo}).then(function (categorias) {
                    var reload = false;
                    if (categorias.type === 'success') {
                        reload = true;
                        Notification.primary({message: categorias.data, title: '<i class="fa fa-check"></i>'});
                    } else {
                        Notification.error({message: categorias.data, title: '<i class="fa fa-ban"></i>'});
                    }
                    $uibModalInstance.close([reload, modalParam.tipo]);
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();