"use strict";
(function () {
    angular.module('blankonApp.promocion', [])
            .controller('listPromocionCtrl', listPromocionCtrl)
            .controller('newPromocionCtrl', newPromocionCtrl)
            .controller('editPromocionCtrl', editPromocionCtrl)
            .controller('modalPromocionInstanceCtrl', modalPromocionInstanceCtrl)
            .controller('modalSelectProductoPromocionInstanceCtrl', modalSelectProductoPromocionInstanceCtrl)
            .factory('GridInternoPromocion', GridInternoPromocion)
            .factory('ModalesInternoPromocion', ModalesInternoPromocion);

    function GridInternoPromocion() {

        var service = {};
        service.getGrid = getGrid;
        service.setData = setData;

        function setData(vm, data) {
            vm.miBoleta.data = data;
            if (data.length <= 10) {
                vm.addRow(10 - data.length);
            }
        }

        function getGrid(vm, $scope) {

            var accion = '<div class="btn-group">' +
                    '<a href="#" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteRow(row);"><i class="fa fa-close"></i></a>' +
                    '<a href="#" class="btn btn-primary btn-xs" ng-click="grid.appScope.openModal(\'1\',row);"><i class="fa fa-pencil"></i></a>' +
                    '</div>';
            var addItem = '<div class="btn-group" style="padding-left: 5px; padding-top: 5px;"><a href="#" class="btn btn-primary btn-xs" ng-click="grid.appScope.addRow(1)"><i class="fa fa-plus"></i> &Iacute;TEM</a></div>';


            vm.columnDefs = [
                {displayName: 'N°', field: 'sequence', width: '32', type: 'number', enableCellEdit: false},
                {displayName: '', name: 'edit', width: '62', enableCellEdit: false, enableSorting: false, headerCellTemplate: addItem, cellClass: 'text-center', cellTemplate: accion},
                {displayName: 'Tratamiento', field: 'nombre', width: '*', enableCellEdit: false},
                {displayName: 'PVP', field: 'pvp', width: '100', enableCellEdit: false},
                {displayName: 'Dscto. (%)(€)', field: 'valordscto', width: '100', cellEditableCondition: function ($scope) {
                        return ($scope.row.entity.idproducto !== null && $scope.row.entity.pvp !== null) ? true : false;
                    }},
                {displayName: 'PVP - Dscto.', field: 'pvpdscto', width: '100', enableCellEdit: false}
            ];

            vm.miBoleta = {
                enableSorting: true,
                columnDefs: vm.columnDefs,
                enableCellEditOnFocus: true,
                enableColumnMenus: false
            };

            vm.miBoleta.appScopeProvider = vm;

            vm.miBoleta.onRegisterApi = function (gridApi) {
                vm.gridApiBoleta = gridApi;
                vm.gridApiBoleta.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    rowEntity[colDef.field] = (parseFloat(newValue) > 0) ? parseFloat(newValue) : null;

                    rowEntity['pvpdscto'] = rowEntity[colDef.field];

                    if (vm.promocion.tipodscto === '1') { //Porcentaje
                        rowEntity['pvpdscto'] = rowEntity[colDef.field] === null ? null : redondearValor(rowEntity['pvp'] - (rowEntity['pvp'] * rowEntity[colDef.field] / 100), 2);
                    } else {
                        rowEntity['pvpdscto'] = rowEntity[colDef.field] === null ? null : redondearValor(rowEntity['pvp'] - rowEntity[colDef.field], 2);
                    }

                });
            };

            vm.deleteRow = function (row) {
                var index = vm.miBoleta.data.indexOf(row.entity);
                vm.miBoleta.data.splice(index, 1);
            };

            vm.addRow = function (items) {
                var length = vm.miBoleta.data.length;
                var sequenceact = (length > 0) ? vm.miBoleta.data[length - 1].sequence : 0;
                for (var i = 1; i <= items; i++) {
                    vm.miBoleta.data.push({
                        sequence: sequenceact + i,
                        idproducto: null,
                        pvp: null,
                        valordscto: null,
                        pvpdscto: null
                    });
                }
            };
        }
        return service;
    }
    ;

    function ModalesInternoPromocion() {

        var service = {};
        service.openModal = openModal;

        function openModal(vm, $uibModal, $log) {
            vm.openModal = function (tipo, data) {

                if (tipo === '1') { // 1: paquete 
                    var items = [];
                    vm.miBoleta.data.forEach(function (row) {
                        if (row.idproducto !== null) {
                            items.push(row.idproducto);
                        }
                    });
                }

                if (tipo === '2') { // 2: Tratamiento
                    var items = [];
                    if (typeof data.idproducto !== 'undefined') {
                        items[0] = data.idproducto;
                    }
                }

                var modalInstance = $uibModal.open({
                    templateUrl: 'myModalSelectProductoContent.html',
                    controller: 'modalSelectProductoPromocionInstanceCtrl as vm',
                    windowClass: 'modal-primary',
                    //size: 'lg',
                    resolve: {
                        modalParam: function () {
                            return {
                                data: items
                            };
                        }
                    }
                });

                modalInstance.result.then(function (reload) {

                    if (tipo === '1') { // 1: paquete 
                        var indexof = vm.miBoleta.data.indexOf(data.entity);
                        var sequence = data.entity.sequence;
                        console.log(reload);
                        vm.miBoleta.data.splice(indexof, 1, {
                            sequence: sequence,
                            idproducto: reload.idproducto,
                            nombre: reload.nombre,
                            pvp: reload.valorventa,
                            valordscto: null,
                            pvpdscto: null
                        });
                    }

                    if (tipo === '2') { // 2: Tratamiento
                        vm.producto = reload;
                        vm.promocion.valordscto = null;
                        vm.promocion.pvpdscto = null;
                    }

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });

            };
        }
        ;

        return service;
    }
    ;

    listPromocionCtrl.$inject = ['$scope', '$state', 'promocionService', 'GridExternal', '$uibModal', '$timeout', '$log'];
    function listPromocionCtrl($scope, $state, promocionService, GridExternal, $uibModal, $timeout, $log) {

        var vm = this;
        vm.stateparent = $state.$current.parent.self.name;

        vm.filter = {};

        $scope.$on('handleBroadcast', function () {
            vm.getPage();
        });

        function loadPromociones() {

            var columnsDefs = [
                {displayName: 'Tipo', field: 'promocion', width: '80'},
                {displayName: 'Campaña', field: 'nombre', width: '*'},
                {displayName: 'Desde', field: 'desde', width: '80'},
                {displayName: 'Hasta', field: 'hasta', width: '80'},
                {displayName: 'Tipo dscto.', field: 'tipodscto', width: '88'}, //monetario porcentual
                {displayName: 'PVP', field: 'pvp', width: '80'},
                {displayName: 'Dscto.', field: 'valordscto', width: '80'},
                {displayName: 'PVP - Dscto.', field: 'pvpdscto', width: '100'}
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                promocionService.GetIndex(vm.filter).then(function (guias) {
                    guias.data.forEach(function (row) {
                        var tipodscto = row.tipodscto;
                        row.tipodscto = (tipodscto === '1') ? 'Porcentual' : 'Monetario';
                        if (row.valordscto !== null)
                            row.valordscto = row.valordscto + (tipodscto === '1' ? ' %' : ' €');
                    });
                    vm.gridOptions.totalItems = guias.total;
                    vm.gridOptions.data = guias.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, guias.data.length);
                });
            });
        }

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: 'modalPromocionInstanceCtrl',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload)
                    loadPromociones();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        loadPromociones();
    }

    newPromocionCtrl.$inject = ['$scope', '$state', '$stateParams', 'promocionService', 'Notification', '$uibModal', '$log', 'GridInternoPromocion', 'ModalesInternoPromocion'];
    function newPromocionCtrl($scope, $state, $stateParams, promocionService, Notification, $uibModal, $log, GridInternoPromocion, ModalesInternoPromocion) {

        var vm = this;
        var before = {};
        var after = {};
        var fields = ['nombre', 'desde', 'hasta', 'tipodscto', 'pvp', 'valordscto', 'pvpdscto'];

        vm.stateparent = $state.$current.parent.self.name;

        vm.producto = {};
        vm.promocion = {};
        vm.promocion.idtipopromocion = parseInt($stateParams.tipo, 10); //1:Paquete 2://Tratamiento 
        vm.promocion.tipodscto = '1';

        vm.edicion = false;

        if (vm.promocion.idtipopromocion === 1) { // 1:Paquete 2: Tratamiento
            GridInternoPromocion.getGrid(vm, $scope);
            GridInternoPromocion.setData(vm, []);
        }

        vm.tipodescuento = function () {
            if (vm.promocion.idtipopromocion === 1) { // 1:Paquete 2: Tratamiento
                vm.miBoleta.data.forEach(function (row) {
                    row.valordscto = null;
                    row.pvpdscto = null;
                });
            } else {
                vm.promocion.valordscto = null;
                vm.promocion.pvpdscto = null;
            }
        };

        vm.changeValordscto = function () {
            if (typeof vm.producto.valorventa !== 'undefined') {
                if (vm.promocion.tipodscto === '1') { //1: Porcentaje 2: Monetario
                    vm.promocion.pvpdscto = vm.promocion.valordscto === '' ? null : redondearValor(vm.producto.valorventa - (vm.producto.valorventa * vm.promocion.valordscto / 100), 2);
                } else {
                    vm.promocion.pvpdscto = vm.promocion.valordscto === '' ? null : redondearValor(vm.producto.valorventa - vm.promocion.valordscto, 2);
                }
            }
        };

        ModalesInternoPromocion.openModal(vm, $uibModal, $log);

        vm.save = function () {
            var promociondet = [];
            var datoobligatorio = [];
            if (vm.promocion.idtipopromocion === 1) { //1:Paquete 2://Tratamiento 
                vm.miBoleta.data.forEach(function (row) {
                    if (row.idproducto !== null && row.pvp !== null) {
                        if (row.valordscto !== null) {
                            promociondet.push({idproducto: row.idproducto, pvp: row.pvp, valordscto: row.valordscto, pvpdscto: row.pvpdscto});
                        } else {
                            datoobligatorio.push({producto: row.nombre});
                        }
                    }
                });

                if (datoobligatorio.length > 0) {
                    datoobligatorio.forEach(function (row) {
                        Notification.warning({message: 'Ingresar descuento para "' + row.producto + '"', title: '<i class="fa fa-ban"></i>'});
                    });
                    $scope.miForm.$submitted = false;
                    return false;
                }
            } else {
                vm.promocion.nombre = vm.producto.nombre;
                vm.promocion.idproducto = vm.producto.idproducto;
                vm.promocion.pvp = vm.producto.valorventa;
            }

            vm.OBJpromocion = {
                promocion: vm.promocion,
                promociondet: promociondet
            };

            after = angular.copy(vm.promocion);
            promocionService.Create(vm.OBJpromocion).then(function (promociones) {
                if (promociones.type === 'success') {
                    Notification.primary({message: promociones.data, title: '<i class="fa fa-check"></i>'});
                    vm.brodcast();
                    $state.go('^.list');
                } else {
                    Notification.error({message: promociones.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        vm.brodcast = function () {
            for (var i = 0, len = fields.length; i < len; i++) {
                if (before[fields[i]] !== after[fields[i]]) {
                    $scope.$emit('handleBroadcast');
                    break;
                }
            }
        };
    }

    editPromocionCtrl.$inject = ['$scope', '$state', 'promocionService', '$stateParams', 'Notification', '$uibModal', '$log', 'GridInternoPromocion', 'ModalesInternoPromocion'];
    function editPromocionCtrl($scope, $state, promocionService, $stateParams, Notification, $uibModal, $log, GridInternoPromocion, ModalesInternoPromocion) {

        var vm = this;
        var before = {};
        var after = {};
        var fields = ['nombre', 'desde', 'hasta', 'tipodscto', 'pvp', 'valordscto', 'pvpdscto'];

        vm.stateparent = $state.$current.parent.self.name;

        vm.producto = {};
        vm.promocion = {};
        vm.promocion.idtipopromocion = parseInt($stateParams.tipo, 10); //1:Paquete 2://Tratamiento  
        vm.edicion = true;


        if (vm.promocion.idtipopromocion === 1) { // 1:Paquete 2: Tratamiento
            GridInternoPromocion.getGrid(vm, $scope);
        }

        vm.tipodescuento = function () {
            if (vm.promocion.idtipopromocion === 1) { // 1:Paquete 2: Tratamiento 
                vm.miBoleta.data.forEach(function (row) {
                    row.valordscto = null;
                    row.pvpdscto = null;
                });
            } else {
                vm.promocion.valordscto = null;
                vm.promocion.pvpdscto = null;
            }
        };

        vm.changeValordscto = function () {
            if (typeof vm.producto.valorventa !== 'undefined') {
                if (vm.promocion.tipodscto === '1') { //1: Porcentaje 2: Monetario
                    vm.promocion.pvpdscto = vm.promocion.valordscto === '' ? null : redondearValor(vm.producto.valorventa - (vm.producto.valorventa * vm.promocion.valordscto / 100), 2);
                } else {
                    vm.promocion.pvpdscto = vm.promocion.valordscto === '' ? null : redondearValor(vm.producto.valorventa - vm.promocion.valordscto, 2);
                }
            }
        };

        promocionService.GetShow($stateParams.promocionId).then(function (promociones) {
            vm.promocion = promociones.data;
            vm.others = promociones.others;
            before = angular.copy(vm.promocion);

            if (vm.promocion.idtipopromocion === 1) { //1:Paquete 2://Tratamiento                 
                promociones.others.promociondet.forEach(function (row, index) {
                    row.sequence = index + 1;
                    row.pvp = parseFloat(row.pvp),
                            row.valordscto = parseFloat(row.valordscto),
                            row.pvpdscto = parseFloat(row.pvpdscto)
                });
                GridInternoPromocion.setData(vm, promociones.others.promociondet);
            } else {
                vm.producto.codigo = promociones.others.producto.codigo;
                vm.producto.nombre = vm.promocion.nombre;
                vm.producto.idproducto = vm.promocion.idproducto;
                vm.producto.valorventa = vm.promocion.pvp;
            }
        });

        vm.save = function () {
            var promociondet = [];
            var datoobligatorio = [];
            if (vm.promocion.idtipopromocion === 1) { //1:Paquete 2://Tratamiento 
                vm.miBoleta.data.forEach(function (row) {
                    if (row.idproducto !== null && row.pvp !== null) {
                        if (row.valordscto !== null) {
                            promociondet.push({idproducto: row.idproducto, pvp: row.pvp, valordscto: row.valordscto, pvpdscto: row.pvpdscto});
                        } else {
                            datoobligatorio.push({producto: row.nombre});
                        }
                    }
                });

                if (datoobligatorio.length > 0) {
                    datoobligatorio.forEach(function (row) {
                        Notification.warning({message: 'Ingresar descuento para "' + row.producto + '"', title: '<i class="fa fa-ban"></i>'});
                    });
                    $scope.miForm.$submitted = false;
                    return false;
                }
            } else {
                vm.promocion.nombre = vm.producto.nombre;
                vm.promocion.idproducto = vm.producto.idproducto;
                vm.promocion.pvp = vm.producto.valorventa;
            }

            vm.OBJpromocion = {
                promocion: vm.promocion,
                promociondet: promociondet
            };

            after = angular.copy(vm.promocion);
            promocionService.Update(vm.OBJpromocion).then(function (promociones) {
                if (promociones.type === 'success') {
                    Notification.primary({message: promociones.data, title: '<i class="fa fa-check"></i>'});
                    vm.brodcast();
                    $state.go('^.list'); //estado hermano
                } else {
                    Notification.error({message: promociones.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        ModalesInternoPromocion.openModal(vm, $uibModal, $log);

        vm.brodcast = function () {
            for (var i = 0, len = fields.length; i < len; i++) {
                if (before[fields[i]] !== after[fields[i]]) {
                    $scope.$emit('handleBroadcast');
                    break;
                }
            }
        };
    }

    modalPromocionInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'promocionService', 'Notification'];
    function modalPromocionInstanceCtrl($scope, $uibModalInstance, modalParam, promocionService, Notification) {

        $scope.nombre = modalParam.data.nombre;

        $scope.save = function () {
            promocionService.Delete(modalParam.data.idpromocion).then(function (promociones) {

                var reload = false;
                if (promociones.type === 'success') {
                    reload = true;
                    Notification.primary({message: promociones.data, title: '<i class="fa fa-check"></i>'});
                } else {
                    Notification.error({message: promociones.data, title: '<i class="fa fa-ban"></i>'});
                }
                $uibModalInstance.close(reload);
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalSelectProductoPromocionInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'productoService', 'GridExternal', '$timeout'];
    function modalSelectProductoPromocionInstanceCtrl($scope, $uibModalInstance, modalParam, productoService, GridExternal, $timeout) {

        var vm = this;
        vm.filter = {};
        vm.filter.idtipoproducto = 2; //Servicio 

        function loadProductos() {
            var columnsDefs = [
                {displayName: 'Código', field: 'codigo', width: '100'},
                {displayName: 'Tratamiento', field: 'nombre', width: '*'},
                {displayName: 'PVP', field: 'valorventa', width: '100'},
                {displayName: '', field: 'editor', width: '100', enableSorting: false, cellClass: 'text-center',
                    cellTemplate: '<a href="#" class="btn btn-primary btn-xs" ng-if="grid.appScope.filtroRow(row.entity);" ng-click="grid.appScope.seleccionar(row.entity);">Seleccionar</a>'
                }
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                productoService.GetIndex(vm.filter).then(function (productos) {
                    vm.gridOptions.totalItems = productos.total;
                    vm.gridOptions.data = productos.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, productos.data.length);
                });
            });
        }

        loadProductos();

        vm.seleccionar = function (row) {
            $uibModalInstance.close(row);
        };

        vm.filtroRow = function (row) {
            return (modalParam.data.indexOf(row.idproducto) === -1 && row.valorventa !== null) ? true : false;
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function redondearValor(valor, precision) {
        if (precision > 0) {
            var rounded = Math.round(parseFloat((valor * Math.pow(10, precision)).toFixed(precision))) / Math.pow(10, precision);
            return rounded.toFixed(precision);
        } else {
            return Math.round(valor);
        }
    }
})();