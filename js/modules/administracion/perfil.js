"use strict";
(function () {
    angular.module('blankonApp.perfil', ['ui.bootstrap'])
            .controller('listPerfilCtrl', listPerfilCtrl)
            .controller('formPerfilCtrl', formPerfilCtrl)
            .controller('modalPerfilInstanceCtrl', modalPerfilInstanceCtrl);

    listPerfilCtrl.$inject = ['$scope', 'perfilService', 'GridExternal', '$uibModal', '$timeout', '$log'];
    function listPerfilCtrl($scope, perfilService, GridExternal, $uibModal, $timeout, $log) {
        var vm = this;
        vm.filter = {};
        vm.gridOptions = {};

        function loadPerfiles() {

            var columnsDefs = [
                {name: 'Nombre', field: 'nombre', width: '200'},
                {name: 'Descripción', field: 'descripcion', width: '*'},
                {name: 'acción', width: '100', enableSorting: false, cellClass: 'text-center', cellTemplate:
                            '<a href="#/perfiles/asignar/{{row.entity.idperfil}}" class="btn btn-success btn-xs" tooltip-placement="top" uib-tooltip="Asignados"><i class="fa fa-eye"></i> m&oacute;dulos</a>'}
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                perfilService.GetIndex(vm.filter).then(function (entidades) {

                    vm.gridOptions.totalItems = entidades.total;
                    vm.gridOptions.data = entidades.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, entidades.data.length);
                });
            });
        }

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: 'modalPerfilInstanceCtrl',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload)
                    loadPerfiles();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        loadPerfiles();
    }

    modalPerfilInstanceCtrl.$inject = ['$rootScope', '$scope', '$uibModalInstance', 'modalParam', 'perfilService', 'Notification'];
    function modalPerfilInstanceCtrl($rootScope, $scope, $uibModalInstance, modalParam, perfilService, Notification) {

        $scope.perfil = {};

        if (modalParam.accion !== 'new') {
            $scope.perfil = {
                idperfil: modalParam.data.idperfil,
                nombre: modalParam.data.nombre,
                descripcion: modalParam.data.descripcion
            };
        }

        $scope.accion = modalParam.accion === 'delete' ? true : false;

        $scope.save = function () {
            if (modalParam.accion === 'new') {
                perfilService.Create($scope.perfil).then(function (perfiles) {
                    Notification.primary({message: perfiles.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close(true);
                });
            }

            if (modalParam.accion === 'edit') {
                perfilService.Update($scope.perfil).then(function (perfiles) {
                    Notification.primary({message: perfiles.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close(true);
                });
            }

            if (modalParam.accion === 'delete') {
                perfilService.Delete($scope.perfil.idperfil).then(function (perfiles) {
                    var reload = false;
                    if (perfiles.type === 'success') {
                        reload = true;
                        Notification.primary({message: perfiles.data, title: '<i class="fa fa-check"></i>'});
                    } else {
                        Notification.error({message: perfiles.data, title: '<i class="fa fa-ban"></i>'});
                    }
                    $uibModalInstance.close(reload);
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    formPerfilCtrl.$inject = ['$scope', '$location', 'perfilService', '$stateParams', 'Notification', '$rootScope'];
    function formPerfilCtrl($scope, $location, perfilService, $stateParams, Notification, $rootScope) {
        var vm = this;
        vm.modulosEmpresa = {};
        function loadOpciones() {
            perfilService.GetShow($stateParams.perfilId).then(function (perfiles) {
                vm.perfil = perfiles.data;
                vm.modulosEmpresa = perfiles.others.modulos[$rootScope.urlente].modules;
                vm.perfilmodulo = perfiles.others.perfilmodulo;
                $scope.miForm.$submitted = false;
            });
        }

        vm.save = function () {
            vm.OBJperfil = {
                idperfil: $stateParams.perfilId,
                perfilmodulo: vm.perfilmodulo
            };

            perfilService.UpdatePerfilModulo(vm.OBJperfil).then(function (perfil) {
                Notification.primary({message: perfil.data, title: perfil.title || 'Ok'});
                loadOpciones();
            });
        };

        vm.marcarTodos = function (objeto, opt) {
            marcarTodos(objeto, opt, vm);
        };

        loadOpciones();
    }

    function marcarTodos(objeto, opt, vm) {

        var index = indexOfObjetos(vm.perfilmodulo, objeto.id, 'idmodulo');
        if (index === -1) {
            vm.perfilmodulo.push({idmodulo: objeto.id, nombre: objeto.name});
        } else {
            vm.perfilmodulo.splice(index, 1);
        }

        if (opt === 'menus') {
            angular.forEach(objeto.menus, function (menu, key) {
                var i = indexOfObjetos(vm.perfilmodulo, menu.id, 'idmodulo');

                if (i !== -1)
                    vm.perfilmodulo.splice(i, 1);
                if (index === -1)
                    vm.perfilmodulo.push({idmodulo: menu.id, nombre: menu.name});

                angular.forEach(menu.options, function (opcion, key) {
                    var i = indexOfObjetos(vm.perfilmodulo, opcion.id, 'idmodulo');

                    if (i !== -1)
                        vm.perfilmodulo.splice(i, 1);
                    if (index === -1)
                        vm.perfilmodulo.push({idmodulo: opcion.id, nombre: opcion.name});
                });
            });
        }

        if (opt === 'opciones') {
            angular.forEach(objeto.options, function (opcion, key) {
                var i = indexOfObjetos(vm.perfilmodulo, opcion.id, 'idmodulo');
              
                if (i !== -1)
                    vm.perfilmodulo.splice(i, 1);
 
                if (index === -1)
                    vm.perfilmodulo.push({idmodulo: opcion.id, nombre: opcion.name});
            });
        }
    }

    function indexOfObjetos(myArray, searchTerm, property) {
        for (var i = 0, len = myArray.length; i < len; i++) {
            if (myArray[i][property] === searchTerm)
                return i;
        }
        return -1;
    }

})();