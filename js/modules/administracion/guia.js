"use strict";
(function () {
    angular.module('blankonApp.guia', [])
            .controller('listGuiaCtrl', listGuiaCtrl)
            .controller('newGuiaCtrl', newGuiaCtrl)
            .controller('editGuiaCtrl', editGuiaCtrl)
            .controller('modalGuiaInstanceCtrl', modalGuiaInstanceCtrl)
            .controller('modalSelectProductoGuiaInstanceCtrl', modalSelectProductoGuiaInstanceCtrl)
            .controller('modalGuiaSubitemsInstanceCtrl', modalGuiaSubitemsInstanceCtrl)
            .controller('modalGuiaSubitemsStockInstanceCtrl', modalGuiaSubitemsStockInstanceCtrl)
            .factory('GridInterno', GridInterno)
            .factory('ModalesInterno', ModalesInterno);

    function GridInterno() {

        var service = {};
        service.getGrid = getGrid;
        service.setData = setData;

        function setData(vm, data) {
            vm.miBoleta.data = data;
            if (data.length <= 10) {
                vm.addRow(10 - data.length);
            }
        }

        function getGrid(vm, $scope) {

            var accion = '<div class="btn-group">' +
                    '<a href="#" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteRow(row);"><i class="fa fa-close"></i></a>' +
                    '<a href="#" class="btn btn-primary btn-xs" ng-click="grid.appScope.openModal(row);"><i class="fa fa-pencil"></i></a>' +
                    '<a href="#" class="btn btn-teal btn-xs" ng-click="grid.appScope.openModalSubitems(row);" ng-if="grid.appScope.filtroRow(row.entity);"><i class="fa fa-book"></i></a>' +
                    '</div>';

            var addItem = '<div class="btn-group" style="padding-left: 5px; padding-top: 5px;"><a href="#" class="btn btn-xs" ng-click="grid.appScope.addRow(1)"><i class="fa fa-plus"></i> &Iacute;TEM</a></div>';

            vm.columnDefs = [
                {displayName: 'N°', field: 'sequence', width: '32', type: 'number', enableCellEdit: false},
                {displayName: '', name: 'edit', width: '80', enableCellEdit: false, enableSorting: false, headerCellTemplate: addItem, cellClass: 'text-center', cellTemplate: accion},
                {displayName: 'Código', field: 'codigo', width: '100', enableCellEdit: false},
                {displayName: 'Producto', field: 'producto', width: '400', enableCellEdit: false},
                {displayName: 'Unidad', field: 'unidadabrev', width: '100', enableCellEdit: false},
                {displayName: 'Cantidad', field: 'cantidad', width: '*', type: 'number', cellEditableCondition: function ($scope) {
                        return ($scope.row.entity.idtipostock === 1) ? true : false;
                    }}
            ];

            vm.miBoleta = {
                enableSorting: true,
                columnDefs: vm.columnDefs,
                enableCellEditOnFocus: true,
                enableColumnMenus: false
            };

            vm.miBoleta.appScopeProvider = vm;

            vm.miBoleta.onRegisterApi = function (gridApi) {
                vm.gridApiBoleta = gridApi;
                vm.gridApiBoleta.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    rowEntity[colDef.field] = (parseInt(newValue, 10) > 0) ? parseInt(newValue, 10) : null;
                });
            };

            vm.filtroRow = function (row) {
                var e = (row.idtipostock === 2) ? true : false;
                return e;
            };

            vm.deleteRow = function (row) {
                var index = vm.miBoleta.data.indexOf(row.entity);
                vm.miBoleta.data.splice(index, 1);
            };

            vm.addRow = function (items) {
                var length = vm.miBoleta.data.length;
                var sequenceact = (length > 0) ? vm.miBoleta.data[length - 1].sequence : 0;
                for (var i = 1; i <= items; i++) {
                    vm.miBoleta.data.push({sequence: sequenceact + i});
                }
            };
        }

        return service;
    }
    ;

    function ModalesInterno() {

        var service = {};
        service.openModal = openModal;
        service.openModalSubitems = openModalSubitems;

        function openModal(vm, $uibModal, $log) {
            vm.openModal = function (data) {

                var items = [];
                vm.miBoleta.data.forEach(function (row) {
                    if (typeof row.idproducto !== 'undefined') {
                        items.push(row.idproducto);
                    }
                });

                var modalInstance = $uibModal.open({
                    templateUrl: 'myModalSelectProductoContent.html',
                    controller: 'modalSelectProductoGuiaInstanceCtrl as vm',
                    windowClass: 'modal-primary',
                    //size: 'lg',
                    resolve: {
                        modalParam: function () {
                            return {
                                data: items
                            };
                        }
                    }
                });

                modalInstance.result.then(function (reload) {

                    var indexof = vm.miBoleta.data.indexOf(data.entity);
                    var sequence = data.entity.sequence;

                    vm.miBoleta.data.splice(indexof, 1, {
                        sequence: sequence,
                        idproducto: reload.idproducto,
                        codigo: reload.codigo,
                        producto: reload.nombre,
                        unidadabrev: reload.unidadmedidaabrev,
                        idtipostock: reload.idtipostock,
                        cantidad: null,
                        subitems: []
                    });

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });

            };
        }
        ;

        function openModalSubitems(vm, $uibModal, $log, stateparent) {
            vm.openModalSubitems = function (data) {
                
                var controller = 'modalGuiaSubitemsInstanceCtrl as vm';
                if (stateparent === 'guiasalida') {
                    controller = 'modalGuiaSubitemsStockInstanceCtrl as vm';
                }

                var modalInstance = $uibModal.open({
                    templateUrl: 'myModalGuiaSubitemsContent.html',
                    controller: controller,
                    windowClass: 'modal-teal',
                    size: 'sm',
                    resolve: {
                        modalParam: function () {
                            return {
                                data: data.entity
                            };
                        }
                    }
                });

                modalInstance.result.then(function () {}, function () {
                    var indexof = vm.miBoleta.data.indexOf(data.entity);
                    var cantidad = 0;

                    vm.miBoleta.data[indexof].subitems.forEach(function (row) {
                        var cant = parseInt(row['cantidad'], 10);
                        if (cant > 0) {
                            cantidad = cantidad + cant;
                        }
                    });

                    vm.miBoleta.data[indexof].cantidad = (cantidad === 0) ? null : cantidad;

                    $log.info('Modal dismissed at: ' + new Date());
                });
            };
        }
        ;
        return service;
    }
    ;

    listGuiaCtrl.$inject = ['$scope', '$state', 'guiaService', 'GridExternal', '$uibModal', '$timeout', '$log'];
    function listGuiaCtrl($scope, $state, guiaService, GridExternal, $uibModal, $timeout, $log) {

        var vm = this;
        vm.stateparent = $state.$current.parent.self.name;

        vm.filter = {};
        vm.filter.movimiento = (vm.stateparent === 'guia') ? 'I' : 'S';

        $scope.$on('handleBroadcast', function () {
            vm.getPage();
        });

        var funcAnulado = function (grid, row, col, rowRenderIndex, colRenderIndex) {
            if (row.entity.idestadodocumento === 3) {
                return 'morado';
            } else {
                return '';
            }
        };
        vm.searchNGuia = true;

        function loadGuias() {
            var columnsDefs = [
                {displayName: 'N° guía', field: 'serienumero', width: '70', enableSorting: false},
                {displayName: 'Fecha guía', field: 'fechadocumento', width: '90', enableSorting: false},
                {displayName: 'Estado guía', field: 'estadodocumento', width: '95', cellClass: funcAnulado, enableSorting: false},
                {displayName: 'Código', field: 'codigo', width: '100', enableSorting: false},
                {displayName: 'Producto', field: 'producto', width: '*', enableSorting: false},
                {displayName: 'Unidad', field: 'unidadabrev', width: '100', enableSorting: false},
                {displayName: 'Cantidad', field: 'cantidad', width: '100', enableSorting: false}
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                guiaService.GetIndex(vm.filter).then(function (guias) {
                    vm.gridOptions.totalItems = guias.total;
                    vm.gridOptions.data = guias.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, guias.data.length);
                });
            });
        }

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: 'modalGuiaInstanceCtrl',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data,
                            stateparent: vm.stateparent
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload)
                    loadGuias();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        loadGuias();
    }

    newGuiaCtrl.$inject = ['$scope', '$state', 'guiaService', 'Notification', '$uibModal', '$log', 'GridInterno', 'ModalesInterno'];
    function newGuiaCtrl($scope, $state, guiaService, Notification, $uibModal, $log, GridInterno, ModalesInterno) {

        var vm = this;
        var before = {};
        var after = {};
        var fields = ['fechadocumento', 'identidaddocumento', 'idestadodocumento'];

        vm.stateparent = $state.$current.parent.self.name;

        vm.guia = {};
        vm.guia.idestadodocumento = 2; //1:En espera 2:Atendido 3:Anulado
        vm.guia.iddocumentofiscal = (vm.stateparent === 'guia') ? 1 : 2;
        vm.guia.movimiento = (vm.stateparent === 'guia') ? 'I' : 'S';

        vm.edicion = false;

        GridInterno.getGrid(vm, $scope);
        GridInterno.setData(vm, []);

        guiaService.GetNew({movimiento: vm.guia.movimiento}).then(function (guias) {
            vm.others = guias.others;
            vm.guia.serienumero = guias.others.serienumero;
            vm.guia.fechadocumento = guias.others.fechaactual;

            guias.others.estadodocumentos.forEach(function (row, index) {
                if (row.idestadodocumento === 3 || row.idestadodocumento === 1) {
                    guias.others.estadodocumentos.splice(index, 1);
                }
            });

        });

        ModalesInterno.openModal(vm, $uibModal, $log);
        ModalesInterno.openModalSubitems(vm, $uibModal, $log, vm.stateparent);

        vm.save = function () {
            vm.guiadet = [];
            var datoobligatorio = [];
            vm.miBoleta.data.forEach(function (row) {
                if (row.idproducto !== '' && typeof row.subitems !== 'undefined') {
                    if (row.cantidad > 0) {
                        var subitems = [];
                        row.subitems.forEach(function (row) {
                            if (parseInt(row.cantidad, 10) > 0) {
                                subitems.push({cantidad: row.cantidad, vencimiento: row.vencimiento});
                            }
                        });
                        vm.guiadet.push({idproducto: row.idproducto, cantidad: row.cantidad, subitems: subitems});
                    } else {
                        datoobligatorio.push({producto: row.producto});
                    }
                }
            });

            if (datoobligatorio.length > 0) {
                datoobligatorio.forEach(function (row) {
                    Notification.warning({message: 'Ingresar cantidad para "' + row.producto + '"', title: '<i class="fa fa-ban"></i>'});
                });
                $scope.miForm.$submitted = false;
                return false;
            }

            if(vm.guiadet.length === 0){
                Notification.warning({message: 'Ingresar al menos un producto', title: '<i class="fa fa-ban"></i>'});
                $scope.miForm.$submitted = false;
                return false;
            }
            
            vm.OBJguia = {
                guia: vm.guia,
                guiadet: vm.guiadet
            };

            after = angular.copy(vm.guia);
            guiaService.Create(vm.OBJguia).then(function (guias) {
                if (guias.type === 'success') {
                    Notification.primary({message: guias.data, title: '<i class="fa fa-check"></i>'});
                    vm.brodcast();
                    $state.go('^.list'); //estado hermano
                } else {
                    Notification.error({message: guias.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        vm.brodcast = function () {
            for (var i = 0, len = fields.length; i < len; i++) {
                if (before[fields[i]] !== after[fields[i]]) {
                    $scope.$emit('handleBroadcast');
                    break;
                }
            }
        };
    }

    editGuiaCtrl.$inject = ['$scope', '$state', 'guiaService', '$stateParams', 'Notification', '$uibModal', '$log', 'GridInterno', 'ModalesInterno'];
    function editGuiaCtrl($scope, $state, guiaService, $stateParams, Notification, $uibModal, $log, GridInterno, ModalesInterno) {

        var vm = this;
        var before = {};
        var after = {};

        var fields = ['fechadocumento', 'identidaddocumento', 'idestadodocumento'];

        vm.stateparent = $state.$current.parent.self.name;
        vm.guia = {};
        vm.guia.iddocumentofiscal = (vm.stateparent === 'guia') ? 1 : 2;
        vm.guia.movimiento = (vm.stateparent === 'guia') ? 'I' : 'S';

        vm.edicion = true;

        GridInterno.getGrid(vm, $scope);

        guiaService.GetShow($stateParams.guiaId).then(function (guias) {
            vm.guia = guias.data;
            vm.others = guias.others;
            before = angular.copy(vm.guia);

            guias.others.guiadet.forEach(function (row, index) {
                row.sequence = index + 1;
                row.cantidad = parseInt(row.cantidad, 10);
                row.subitems.forEach(function (row2) {
                    row2.cantidad = parseInt(row2.cantidad, 10);
                    row2.ven = row2.vencimiento;
                    console.log('Ingreso: ' + row2.vencimiento);
                    if (row2.vencimiento !== null) {
                        console.log('Ingreso');
                        var ano = row2.vencimiento.slice(0, 4);
                        var mes = parseInt(row2.vencimiento.slice(5, 7), 10) - 1;
                        var dia = row2.vencimiento.slice(8, 10);
                        row2.vencimiento = new Date(ano, mes, dia);
                    }
                });
            });

            guias.others.estadodocumentos.forEach(function (row, index) {
                if (row.idestadodocumento === 1) { //1: En espera
                    guias.others.estadodocumentos.splice(index, 1);
                }
            });

            GridInterno.setData(vm, guias.others.guiadet);

        });

        vm.save = function () {
            vm.guiadet = [];
            var datoobligatorio = [];
            vm.miBoleta.data.forEach(function (row) {
                if (row.idproducto !== '' && typeof row.subitems !== 'undefined') {
                    if (row.cantidad > 0) {
                        var subitems = [];
                        row.subitems.forEach(function (row) {
                            if (parseInt(row.cantidad, 10) > 0) {
                                subitems.push({cantidad: row.cantidad, vencimiento: row.vencimiento});
                            }
                        });
                        vm.guiadet.push({idproducto: row.idproducto, cantidad: row.cantidad, subitems: subitems});
                    } else {
                        datoobligatorio.push({producto: row.producto});
                    }
                }
            });

            if (datoobligatorio.length > 0) {
                datoobligatorio.forEach(function (row) {
                    Notification.warning({message: 'Ingresar cantidad para "' + row.producto + '"', title: '<i class="fa fa-ban"></i>'});
                });
                $scope.miForm.$submitted = false;
                return false;
            }
            
            if(vm.guiadet.length === 0){
                Notification.warning({message: 'Ingresar al menos un producto', title: '<i class="fa fa-ban"></i>'});
                $scope.miForm.$submitted = false;
                return false;
            }
            
            vm.OBJguia = {
                guia: vm.guia,
                guiadet: vm.guiadet
            };

            after = angular.copy(vm.guia);
            guiaService.Update(vm.OBJguia).then(function (guias) {
                if (guias.type === 'success') {
                    Notification.primary({message: guias.data, title: '<i class="fa fa-check"></i>'});
                    $scope.$emit('handleBroadcast');
                    $state.go('^.list'); //estado hermano
                } else {
                    Notification.error({message: guias.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        ModalesInterno.openModal(vm, $uibModal, $log);
        ModalesInterno.openModalSubitems(vm, $uibModal, $log, vm.stateparent);

        vm.brodcast = function () {
            for (var i = 0, len = fields.length; i < len; i++) {
                if (before[fields[i]] !== after[fields[i]]) {
                    $scope.$emit('handleBroadcast');
                    break;
                }
            }
        };
    }

    modalGuiaInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'guiaService', 'Notification'];
    function modalGuiaInstanceCtrl($scope, $uibModalInstance, modalParam, guiaService, Notification) {
        $scope.serienumero = modalParam.data.serienumero;
        $scope.tipodeguia = modalParam.stateparent === 'guia' ? 'entrada' : 'salida';

        $scope.save = function () {
            guiaService.Delete(modalParam.data.idguia).then(function (guias) {
                var reload = false;
                if (guias.type === 'success') {
                    reload = true;
                    Notification.primary({message: guias.data, title: '<i class="fa fa-check"></i>'});
                } else {
                    Notification.error({message: guias.data, title: '<i class="fa fa-ban"></i>'});
                }
                $uibModalInstance.close(reload);
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalSelectProductoGuiaInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'productoService', 'GridExternal', '$timeout'];
    function modalSelectProductoGuiaInstanceCtrl($scope, $uibModalInstance, modalParam, productoService, GridExternal, $timeout) {

        var vm = this;
        vm.filter = {};

        var funcAlert = function (grid, row) {
            return (row.entity.stockalert) ? 'alertStock' : '';
        };

        function loadProductos() {
            var columnsDefs = [
                {displayName: 'Producto', field: 'nombre', width: '*'},
                {displayName: 'Unidad', field: 'unidadmedidaabrev', width: '80'},
                {displayName: 'Tipo stock', field: 'tipostock', width: '90'},
                {displayName: 'Stock', field: 'stock', width: '60', enableSorting: false, cellClass: funcAlert},
                {displayName: '', field: 'editor', width: '100', enableSorting: false, cellClass: 'text-center',
                    cellTemplate: '<a href="#" class="btn btn-primary btn-xs" ng-if="grid.appScope.filtroRow(row.entity);" ng-click="grid.appScope.seleccionar(row.entity);">Seleccionar</a>'
                }
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                productoService.GetIndexStock(vm.filter).then(function (productos) {
                    vm.gridOptions.totalItems = productos.total;
                    vm.gridOptions.data = productos.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, productos.data.length);
                });
            });
        }

        loadProductos();

        vm.seleccionar = function (row) {
            $uibModalInstance.close(row);
        };

        vm.filtroRow = function (row) {
            return modalParam.data.indexOf(row.idproducto) === -1 ? true : false;
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalGuiaSubitemsInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam'];
    function modalGuiaSubitemsInstanceCtrl($scope, $uibModalInstance, modalParam) {

        var vm = this;

        vm.fechavence = true;
        vm.deleteRow = function (row) {
            var index = vm.gridOptionsSubitems.data.indexOf(row.entity);
            vm.gridOptionsSubitems.data.splice(index, 1);
        };

        vm.addFila = function (items) {
            for (var i = 1; i <= items; i++) {
                vm.gridOptionsSubitems.data.push({vencimiento: '', cantidad: null});
            }
        };

        vm.gridOptionsSubitems = {
            enableColumnMenus: false,
            enableCellEditOnFocus: true
        };
        vm.gridOptionsSubitems.appScopeProvider = vm;
        vm.producto = modalParam.data.producto;
        vm.unidadmedida = modalParam.data.unidadabrev;

        var columnsDefs = [
            {displayName: '', name: 'remove', width: '30', enableSorting: false, cellClass: 'text-center', enableCellEdit: false,
                headerCellTemplate: '<div class="pl-10 mt-5"><a href="#" ng-click="grid.appScope.addFila(1)"><i class="fa fa-plus"></i></a></div>',
                cellTemplate: '<a href="#" ng-click="grid.appScope.deleteRow(row);"><i class="fa fa-close"></i></a>'
            },
            {displayName: 'Fecha vence', field: 'vencimiento', width: '130', enableSorting: false, type: 'date', cellFilter: 'date:"yyyy-MM-dd"'},
            {displayName: 'Cantidad', field: 'cantidad', width: '*', enableSorting: false, type: 'number'}
        ];

        vm.gridOptionsSubitems.columnDefs = columnsDefs;
        vm.gridOptionsSubitems.onRegisterApi = function (gridApi) {
            vm.gridApiBoleta = gridApi;
            vm.gridApiBoleta.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                if (colDef.field === 'cantidad') {
                    rowEntity[colDef.field] = (parseInt(newValue, 10) > 0) ? parseInt(newValue, 10) : null;
                }
            });
        };

        vm.gridOptionsSubitems.data = modalParam.data.subitems;

        if (modalParam.data.subitems.length <= 7) {
            vm.addFila(7 - modalParam.data.subitems.length);
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalGuiaSubitemsStockInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'productoService'];
    function modalGuiaSubitemsStockInstanceCtrl($scope, $uibModalInstance, modalParam, productoService) {

        var vm = this;

        vm.fechavence = false;
        vm.gridOptionsSubitems = {
            enableColumnMenus: false,
            enableCellEditOnFocus: true
        };

        vm.gridOptionsSubitems.appScopeProvider = vm;
        vm.producto = modalParam.data.producto;
        vm.unidadmedida = modalParam.data.unidadabrev;

        var columnsDefs = [
            {displayName: 'Fecha vence', field: 'vencimiento', width: '100', enableSorting: false, enableCellEdit: false, type: 'date', cellFilter: 'date:"yyyy-MM-dd"'},
            {displayName: 'Stock', field: 'stock', width: '70', enableSorting: false, enableCellEdit: false},
            {displayName: 'Cantidad', field: 'cantidad', width: '*', enableSorting: false, type: 'number'}
        ];

        vm.gridOptionsSubitems.columnDefs = columnsDefs;
        vm.gridOptionsSubitems.onRegisterApi = function (gridApi) {
            vm.gridApiBoleta = gridApi;
            vm.gridApiBoleta.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                if (colDef.field === 'cantidad') {
                    rowEntity[colDef.field] = (parseInt(newValue, 10) > 0) ? parseInt(newValue, 10) : null;
                }
            });
        };

        productoService.GetIndexStockDet({idproducto: modalParam.data.idproducto}).then(function (stocks) {

            var stocks = stocks.data;
            stocks.forEach(function (row) {
                if (row.vencimiento === '') {
                    row.vencimiento = null;
                } else {
                    var ano = row.vencimiento.slice(0, 4);
                    var mes = parseInt(row.vencimiento.slice(5, 7), 10) - 1;
                    var dia = row.vencimiento.slice(8, 10);
                    row.vencimiento = new Date(ano, mes, dia);
                }
            });

            if (modalParam.data.subitems.length === 0) {
                modalParam.data.subitems = stocks;
                vm.gridOptionsSubitems.data = modalParam.data.subitems;
            }

            if (modalParam.data.subitems.length > 0) {
                vm.gridOptionsSubitems.data = modalParam.data.subitems;

                vm.gridOptionsSubitems.data.forEach(function (row1) {
                    var noExiste = true;

                    stocks.forEach(function (row2, index) {
                        if (String(row1.vencimiento) === String(row2.vencimiento)) {
                            noExiste = false;
                            row1.stock = row2.stock;
                        }
                    });

                    if (noExiste) {
                        row1.vencimiento = null;
                        row1.stock = 0;
                    }
                });

                stocks.forEach(function (row1) {
                    var nuevo = true;
                    vm.gridOptionsSubitems.data.forEach(function (row2) {
                        console.log(row1.vencimiento + ' === ' + row2.vencimiento);
                        if (String(row1.vencimiento) === String(row2.vencimiento)) {
                            nuevo = false;
                        }
                    });
                    if (nuevo) {
                        vm.gridOptionsSubitems.data.push(row1);
                    }
                });
            }
        });

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();