"use strict";
(function () {
    angular.module('blankonApp.producto', [])
            .controller('listProductoCtrl', listProductoCtrl)
            .controller('newProductoCtrl', newProductoCtrl)
            .controller('editProductoCtrl', editProductoCtrl)
            .controller('modalProductoInstanceCtrl', modalProductoInstanceCtrl)
            .controller('modalCategoriaProductoInstanceCtrl', modalCategoriaProductoInstanceCtrl);

    listProductoCtrl.$inject = ['$scope', 'productoService', 'GridExternal', '$uibModal', '$timeout', '$log'];
    function listProductoCtrl($scope, productoService, GridExternal, $uibModal, $timeout, $log) {
        var vm = this;

        vm.filter = {};
        $scope.$on('handleBroadcast', function () {
            vm.getPage();
        });

        vm.filter.idtipoproducto = 1; //Servicio 

        function loadProductos() {
            var columnsDefs = [
                {displayName: 'Código', field: 'codigo', width: '70'},
                {displayName: 'Producto', field: 'nombre', width: '*'},
                {displayName: 'Medida', field: 'unidadmedidaabrev', width: '70'},
                {displayName: 'Stock Mín.', field: 'stockmin', width: '85'},
                {displayName: 'BASE', field: 'valorventabase', width: '70'},
                {displayName: 'IVA 21%', field: 'valorventaigv', width: '70'},
                {displayName: 'PVP', field: 'valorventa', width: '70'}
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                productoService.GetIndex(vm.filter).then(function (productos) {
                    vm.gridOptions.totalItems = productos.total;
                    vm.gridOptions.data = productos.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, productos.data.length);
                });
            });
        }

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: 'modalProductoInstanceCtrl',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload)
                    loadProductos();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        loadProductos();
    }

    newProductoCtrl.$inject = ['$scope', '$state', 'productoService', 'Notification', '$uibModal', '$log'];
    function newProductoCtrl($scope, $state, productoService, Notification, $uibModal, $log) {
        var vm = this;

        vm.producto = {};
        vm.edicion = false;
        vm.producto.idtipoproducto = 1;
        vm.producto.idmonedaventa = 1;
        vm.producto.activo = 1;
        vm.producto.interno = '0';

        vm.changeValorventabase = function () {
            vm.producto.valorventaigv = redondearValor(vm.producto.valorventabase * 0.21, 2);
            vm.producto.valorventa = redondearValor(vm.producto.valorventabase * 1.21, 0);
        };

        vm.changeValorventa = function () {
            vm.producto.valorventabase = redondearValor(vm.producto.valorventa / 1.21, 2);
            vm.producto.valorventaigv = redondearValor(vm.producto.valorventabase * 0.21, 2);
        };

        productoService.GetNew().then(function (productos) {
            vm.others = productos.others;
        });

        vm.save = function () {
            vm.OBJproducto = {
                producto: vm.producto
            };

            productoService.Create(vm.OBJproducto).then(function (productos) {
                if (productos.type === 'success') {
                    Notification.primary({message: productos.data, title: '<i class="fa fa-check"></i>'});
                    $scope.$emit('handleBroadcast');
                    $state.go('^.list');
                } else {
                    Notification.error({message: productos.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalCategoriaContent.html',
                controller: 'modalCategoriaProductoInstanceCtrl',
                windowClass: 'modal-primary',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                vm.producto.idarbol = reload[0];
                vm.producto.categoria = reload[1];
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }

    editProductoCtrl.$inject = ['$scope', '$state', 'productoService', '$stateParams', 'Notification', '$uibModal', '$log'];
    function editProductoCtrl($scope, $state, productoService, $stateParams, Notification, $uibModal, $log) {
        var vm = this;
        var before = {};
        var after = {};
        var fields = ['codigo', 'nombre', 'idunidadmedidda', 'stockmin', 'valorventabase', 'valorventa'];

        vm.producto = {};
        vm.edicion = true;

        vm.changeValorventabase = function () {
            vm.producto.valorventaigv = redondearValor(vm.producto.valorventabase * 0.21, 2);
            vm.producto.valorventa = redondearValor(vm.producto.valorventabase * 1.21, 0);
        };

        vm.changeValorventa = function () {
            vm.producto.valorventabase = redondearValor(vm.producto.valorventa / 1.21, 2);
            vm.producto.valorventaigv = redondearValor(vm.producto.valorventabase * 0.21, 2);
        };

        productoService.GetShow($stateParams.productoId).then(function (productos) {
            vm.producto = productos.data;
            vm.others = productos.others;

            before = angular.copy(vm.producto);
        });

        vm.save = function () {
            vm.OBJproducto = {
                producto: vm.producto
            };

            after = angular.copy(vm.producto);
            productoService.Update(vm.OBJproducto).then(function (productos) {
                if (productos.type === 'success') {
                    Notification.primary({message: productos.data, title: '<i class="fa fa-check"></i>'});
                    vm.brodcast();
                    $state.go('^.list');
                } else {
                    Notification.error({message: productos.data, title: '<i class="fa fa-ban"></i>'});
                    $scope.miForm.$submitted = false;
                }
            });
        };

        vm.brodcast = function () {
            for (var i = 0, len = fields.length; i < len; i++) {
                if (before[fields[i]] !== after[fields[i]]) {
                    $scope.$emit('handleBroadcast');
                    break;
                }
            }
        };

        vm.openModal = function (accion, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalCategoriaContent.html',
                controller: 'modalCategoriaProductoInstanceCtrl',
                windowClass: 'modal-primary',
                resolve: {
                    modalParam: function () {
                        return {
                            accion: accion,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                vm.producto.idarbol = reload[0];
                vm.producto.categoria = reload[1];
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }

    modalProductoInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'productoService', 'Notification'];
    function modalProductoInstanceCtrl($scope, $uibModalInstance, modalParam, productoService, Notification) {

        $scope.producto = {};
        $scope.producto = {
            idproducto: modalParam.data.idproducto,
            nombre: modalParam.data.nombre
        };

        $scope.save = function () {
            productoService.Delete($scope.producto.idproducto).then(function (productos) {
                var reload = false;
                if (productos.type === 'success') {
                    reload = true;
                    Notification.primary({message: productos.data, title: '<i class="fa fa-check"></i>'});
                } else {
                    Notification.error({message: productos.data, title: '<i class="fa fa-ban"></i>'});
                }
                $uibModalInstance.close(reload);
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    modalCategoriaProductoInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'categoriaService'];
    function modalCategoriaProductoInstanceCtrl($scope, $uibModalInstance, modalParam, categoriaService) {

        $scope.treeOptions = {
            nodeChildren: "children",
            dirSelectable: false,
            allowDeselect: true
        };

        $scope.titulo = (modalParam.data.idtipoproducto === 1) ? 'Productos' : 'Servicios'
        $scope.idseleccionado = modalParam.data.idarbol;
        $scope.seleccionado = modalParam.data.categoria;

        $scope.nodeSelect = function (node, selected) {
            $scope.idseleccionado = selected ? node.idarbol : '';
            $scope.seleccionado = selected ? node.nombre : '';
        };

        categoriaService.GetIndex({idcategoria: modalParam.data.idtipoproducto}).then(function (categorias) {
            $scope.categorias = categorias.data;
        });

        $scope.save = function () {
            var reload = [$scope.idseleccionado, $scope.seleccionado];
            $uibModalInstance.close(reload);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function redondearValor(valor, precision) {
        if (precision > 0) {
            var rounded = Math.round(parseFloat((valor * Math.pow(10, precision)).toFixed(precision))) / Math.pow(10, precision);
            return rounded.toFixed(precision);
        } else {
            return Math.round(valor);
        }
    }

})();