"use strict";
(function () {
    angular.module("blankonApp.kardex", [])
            .controller('listKardexCtrl', listKardexCtrl)
            .controller('modalSelectProductoKardexInstanceCtrl', modalSelectProductoKardexInstanceCtrl);

    listKardexCtrl.$inject = ['$uibModal', 'productoService', '$log'];
    function listKardexCtrl($uibModal, productoService, $log) {

        var vm = this;
        vm.kardex = [];
        vm.producto = {};
        vm.openModal = function () {
            var items = [vm.producto.idproducto];
            var modalInstance = $uibModal.open({
                templateUrl: 'myModalSelectProductoContent.html',
                controller: 'modalSelectProductoKardexInstanceCtrl as vm',
                windowClass: 'modal-primary',
                resolve: {
                    modalParam: function () {
                        return {
                            data: items
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                productoService.GetIndexKardex({idproducto: reload.idproducto}).then(function (kardex) {
                    vm.producto = kardex.data.producto;
                    vm.kardex = kardex.data.kardex;
                    vm.totales = kardex.data.totales;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });


        };
    }

    modalSelectProductoKardexInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modalParam', 'productoService', 'GridExternal', '$timeout'];
    function modalSelectProductoKardexInstanceCtrl($scope, $uibModalInstance, modalParam, productoService, GridExternal, $timeout) {

        var vm = this;
        vm.filter = {};

        var funcAlert = function (grid, row) {
            return (row.entity.stockalert) ? 'alertStock' : '';
        };

        function loadProductos() {
            var columnsDefs = [
                {displayName: 'Producto', field: 'nombre', width: '*'},
                {displayName: 'Unidad', field: 'unidadmedidaabrev', width: '70'},
                {displayName: 'Tipo stock', field: 'tipostock', width: '85'},
                {displayName: 'Stock', field: 'stock', width: '55', enableSorting: false, cellClass: funcAlert},
                {displayName: '', field: 'editor', width: '100', enableSorting: false, cellClass: 'text-center',
                    cellTemplate: '<a href="#" class="btn btn-primary btn-xs" ng-if="grid.appScope.filtroRow(row.entity);" ng-click="grid.appScope.seleccionar(row.entity);">Seleccionar</a>'
                }
            ];

            GridExternal.getGrid(vm, columnsDefs, $scope, function () {
                productoService.GetIndexStock(vm.filter).then(function (productos) {
                    vm.gridOptions.totalItems = productos.total;
                    vm.gridOptions.data = productos.data;
                    $timeout(function () {
                        if (vm.gridApi.selection.selectRow) {
                            vm.gridApi.selection.selectRow(vm.gridOptions.data[0]);
                        }
                    });
                    GridExternal.setInfoPagina(vm, vm.gridApi.pagination, vm.gridOptions, productos.data.length);
                });
            });
        }

        loadProductos();

        vm.seleccionar = function (row) {
            $uibModalInstance.close(row);
        };

        vm.filtroRow = function (row) {
            return modalParam.data.indexOf(row.idproducto) === -1 ? true : false;
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();