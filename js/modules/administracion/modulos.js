"use strict";
(function () {
    angular.module('blankonApp.modulos', ['ui.bootstrap'])
            .controller('formModuloCtrl', formModuloCtrl)
            .controller('modalInstanceCtrl', modalInstanceCtrl);

    formModuloCtrl.$inject = ['modulosService', '$rootScope', '$uibModal', '$log', '$timeout'];
    function formModuloCtrl(modulosService, $rootScope, $uibModal, $log, $timeout) {
        var vm = this;
        vm.modulos = [];
        vm.modulosCopy = [];

        function loadModulos() {
            vm.modulos = [];
            vm.modulosCopy = [];
            modulosService.GetIndex().then(function (entidades) {
                $timeout(function () {
                    vm.modulos = entidades.data[$rootScope.urlente].modules;
                    vm.modulosCopy = entidades.data[$rootScope.urlente].modules;
                });

            });
        }

        vm.openModal = function (modal, accion, data) {

            var templateUrl = '';
            if (modal === 'modulo') {
                templateUrl = 'myModalContent.html';
            }
            if (modal === 'menu') {
                templateUrl = 'myModalContentMenu.html';
            }
            if (modal === 'opcion') {
                templateUrl = 'myModalContentOpcion.html';
            }

            var modalInstance = $uibModal.open({
                templateUrl: templateUrl,
                controller: 'modalInstanceCtrl as vm',
                windowClass: 'modal-primary',
                size: 'sm',
                resolve: {
                    modulos: function () {
                        return {
                            modal: modal,
                            accion: accion,
                            modulos: vm.modulos,
                            data: data
                        };
                    }
                }
            });

            modalInstance.result.then(function (reload) {
                if (reload) {
                    loadModulos();
                }
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        loadModulos();
    }

    modalInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'modulos', 'modulosService', 'Notification'];
    function modalInstanceCtrl($scope, $uibModalInstance, modulos, modulosService, Notification) {

        var vm = this;
        vm.modulo = {};
        vm.others = {};
        vm.others.modulos = modulos.modulos;
        vm.indexModuloSelected = 0;

        vm.soloNumero = function () {
            if (vm.modulo.orden < 1) {
                vm.modulo.orden = 1;
            }
        };

        vm.mimenuChange = function () {
            vm.others.modulos.forEach(function (row, index) {
                if (row.id === vm.Tmpmodulo) {
                    vm.indexModuloSelected = index;
                }
            });
        };

        if (modulos.accion !== 'new') {
            vm.modulo = {
                parent: modulos.data.parent,
                idmodulo: modulos.data.id,
                nombre: modulos.data.name,
                icono: modulos.data.icon,
                orden: modulos.data.ordenT
            };

            if (modulos.modal === 'opcion') {
                vm.Tmpmodulo = modulos.data.modulopadre;
                vm.modulo.url = modulos.data.uri;
                vm.mimenuChange();
            }
        }

        vm.accion = modulos.accion === 'delete' ? true : false;

        vm.save = function () {

            if (modulos.accion === 'new') {
                if (modulos.modal === 'modulo') {
                    vm.modulo.url = '#';
                    vm.modulo.nivel = 1;
                }
                if (modulos.modal === 'menu') {
                    vm.modulo.url = '#';
                    vm.modulo.nivel = 2;
                }
                if (modulos.modal === 'opcion') {
                    vm.modulo.nivel = 3;
                }

                modulosService.Create(vm.modulo).then(function (modulos) {
                    Notification.primary({message: modulos.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close(true);
                });
            }

            if (modulos.accion === 'edit') {

                modulosService.Update(vm.modulo).then(function (modulos) {
                    Notification.primary({message: modulos.data, title: '<i class="fa fa-check"></i>'});
                    $uibModalInstance.close(true);
                });
            }

            if (modulos.accion === 'delete') {
                modulosService.Delete(vm.modulo.idmodulo).then(function (modulos) {
                    var reload = false;
                    if (modulos.type === 'success') {
                        reload = true;
                        Notification.primary({message: modulos.data, title: '<i class="fa fa-check"></i>'});
                    } else {
                        Notification.error({message: modulos.data, title: '<i class="fa fa-ban"></i>'});
                    }
                    $uibModalInstance.close(reload);
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();