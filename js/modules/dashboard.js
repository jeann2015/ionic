'use strict';
(function () {
    angular.module("blankonApp.dashboard", [])

            .controller('DashboardCtrl', function ($scope, $rootScope, entidadService, $stateParams, jwtHelper, $cookies) {
                var vm = this;
                var id = $stateParams.perfilId;

                vm.tokenPayload = jwtHelper.decodeToken($cookies.get("token"));

                entidadService.GetProfile(id).then(function (entidades) {
                    vm.entidad = entidades.data;
                    vm.others = entidades.others;
                    vm.cliente = entidades.others.clientes;
                    vm.personal = entidades.others.personales;
                    //vm.proveedor = entidades.others.proveedores;
                    vm.medico = entidades.others.medicos;
                    vm.entidadespecialidad = entidades.others.entidadespecialidad;
                    vm.entidadsede = entidades.others.entidadsede;
                });

            });

})();