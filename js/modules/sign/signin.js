"use strict";
(function () {
    angular.module('blankonApp.account.signin', [])
            .controller('SigninCtrl', SigninCtrl);

    SigninCtrl.$inject = ['loadEnterprise', '$rootScope', 'AuthenticationService', '$location'];
    function SigninCtrl(loadEnterprise, $rootScope, AuthenticationService, $location) {

        var vm = this;
        vm.login = login;
        vm.submitlogin = 'Entrar';

        $rootScope.empresa = loadEnterprise.data;

        function login() {
            vm.dataLoading = false;
            vm.submitlogin = 'Autenticando...';

            AuthenticationService.Login(vm.username, vm.password, $rootScope.urlente, function (response) {
                if (response.success) {
                    var len = response.userEnterprises.length;
                    if (len === 0) {
                        vm.warning = 'No tiene empresas asociadas a su cuenta. Comunique con la empresa.';
                    } else {
                        AuthenticationService.SetCredentials(response);
                        $location.path('/perfil/' + $rootScope.entidad.identidad);
                    }
                } else { 
                    vm.error = response.message;
                    vm.dataLoading = false;
                    vm.submitlogin = 'Entrar';
                }
            });
        }
        
        $('#sign-wrapper').css('min-height', $(window).outerHeight());
    }
})();