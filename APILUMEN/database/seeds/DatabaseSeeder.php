<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call('temaTableSeeder');
        $this->call('ubigeoTableSeeder');
        $this->call('empresaTableSeeder');
        $this->call('almacenTableSeeder');
        $this->call('entidadTableSeeder');
        $this->call('superperfilTableSeeder');
        $this->call('moduloTableSeeder');
        $this->call('monedaTableSeeder');        
        $this->call('unidadmedidaTableSeeder');
        Model::reguard();
    }
}
