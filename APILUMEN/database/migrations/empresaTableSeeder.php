<?php
use Illuminate\Database\Seeder;
//use Faker\Factory as Faker;
//use Illuminate\Database\ConnectionInterface;
 
class empresaTableSeeder extends Seeder{
    
    public function run(){                        
        
        $matriz[] = array(
            'razonsocial'=>'Clínica Internacional de Medicina Estética',
            'ruc' =>'A01001529', 
            'url'=>'cime', 
            'direccion'=>' Avenida Martí Pujol 230 -246 08911 Badalona, Barcelona, España.',
            'idtema'=>1,
            'imglogologin'=>'logo.png',
            'imglogosistema'=>'milogo1.png',
            'telefono'=>'+34 937 063 850',
            'email'=>'info@clinica-cime.com',
            'facebook'=>'https://www.facebook.com/ClinicaCime',
            'twitter'=>'https://twitter.com/Clinica_CIME',
            'paginaweb'=>'http://www.clinica-cime.com'
            );
        
        $matriz[] = array(
            'razonsocial'=>'Colegio Alas Peruano Argentino',
            'ruc' =>'104412002', 
            'url'=>'calaspa', 
            'direccion'=>'Av. 28 de Julio n° 104 - Lima',
            'idtema'=>1,
            'imglogologin'=>'logo.png',
            'imglogosistema'=>'milogo1.png',
            'telefono'=>'',
            'email'=>'',
            'facebook'=>'',
            'twitter'=>'',
            'paginaweb'=>'');
        
        foreach ($matriz as $array) {            
                \DB::table('empresa')->insert(array( 
                    'razonsocial' => $array['razonsocial'],
                    'ruc' => $array['ruc'],
                    'url' => $array['url'],
                    'direccion' => $array['direccion'],
                    'idtema' => $array['idtema'],
                    'imglogologin' => $array['imglogologin'],
                    'imglogosistema' => $array['imglogosistema'],
                    'telefono' => $array['telefono'],
                    'email' => $array['email'],
                    'facebook' => $array['facebook'],
                    'twitter' => $array['twitter'],
                    'paginaweb' => $array['paginaweb']
                ));            
        }
        
        $matriz = []; 
        $matriz[] = array('nombre'=>'Médico');
        $matriz[] = array('nombre'=>'Otros');
        foreach ($matriz as $array) {
            \DB::table('tipoespecialidad')->insert(array(  
                'idempresa' => 1,
                'nombre' => $array['nombre'] 
            ));            
        }
        
        $matriz = []; 
        $matriz[] = array('nombre'=>'Medico estético ', 'idtipoespecialidad'=>1);
        $matriz[] = array('nombre'=>'Cirujano Plástico', 'idtipoespecialidad'=>1);
        $matriz[] = array('nombre'=>'Cirujano vascular', 'idtipoespecialidad'=>1);
        $matriz[] = array('nombre'=>'Ginecólogo', 'idtipoespecialidad'=>1);
        $matriz[] = array('nombre'=>'Fisioterapeuta', 'idtipoespecialidad'=>1);
        $matriz[] = array('nombre'=>'Médico del deporte', 'idtipoespecialidad'=>1);
        $matriz[] = array('nombre'=>'Médico General', 'idtipoespecialidad'=>1);
        $matriz[] = array('nombre'=>'Nutricionista', 'idtipoespecialidad'=>1);
        
        
        foreach ($matriz as $array) {
            \DB::table('especialidad')->insert(array(  
                'idempresa' => 1,
                'nombre' => $array['nombre'],
                'idtipoespecialidad' => $array['idtipoespecialidad']
            ));            
        }
        
        $matriz = []; 
        $matriz[] = array('nombre'=>'Recepción');
        $matriz[] = array('nombre'=>'Técnico');
        $matriz[] = array('nombre'=>'Médico');
        $matriz[] = array('nombre'=>'Dirección');
        $matriz[] = array('nombre'=>'Administración');
        $matriz[] = array('nombre'=>'Marketing');
        
        foreach ($matriz as $array) {
            \DB::table('cargoorg')->insert(array(  
                'idempresa' => 1,
                'nombre' => $array['nombre'] 
            ));            
        }
        
        $matriz = []; 
        $matriz[] = array('nombre'=>'Sede principal', 'direccion'=>'Av.Canda 2035', 'principal'=>'1');
        $matriz[] = array('nombre'=>'Sede Puerto Maldonado', 'direccion'=>'Av.Arequipa 105', 'principal'=>'0');        
        foreach ($matriz as $array) {
            \DB::table('sede')->insert(array(  
                'idempresa' => 1,
                'identidad' => 1,
                'nombre' => $array['nombre'], 
                'direccion' => $array['direccion'], 
                'principal' => $array['principal'], 
            ));            
        }
        
        $matriz = []; 
        $matriz[] = array('nombre'=>'Paquete');
        $matriz[] = array('nombre'=>'Tratamiento');        
        $matriz[] = array('nombre'=>'Producto');        
        $matriz[] = array('nombre'=>'Servico');        
        
        foreach ($matriz as $array) {
            \DB::table('tipopromocion')->insert(array(  
                'nombre' => $array['nombre'] 
            ));            
        }
        
    }
    
}
