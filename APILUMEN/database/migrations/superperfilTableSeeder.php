<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Database\ConnectionInterface;
 
class superperfilTableSeeder extends Seeder{
    
    public function run(){        
        
        //$matriz[] = array('nombre'=>'Super Administrador SI', 'descripcion'=>'Super perfil solo para RedYachay'); //RedYachay
        $matriz[] = array('nombre'=>'Administrador S.I.', 'descripcion'=>'Para el encargado del area de sistema en el cliente'); //empresa
        $matriz[] = array('nombre'=>'Público', 'descripcion'=>'Perfil por defecto para los perfiles que creen los clientes'); //empresa
        
        foreach ($matriz as $array) {            
            \DB::table('superperfil')->insert(array( 
                'nombre' => $array['nombre'],
                'descripcion' => $array['descripcion'] 
            ));            
        }
                
        \DB::table('perfil')->insert(array( 
            'idsuperperfil' => 1, // Administrador S.I.
            'idempresa' => 1,
            'nombre' => 'Administrador S.I.',
            'descripcion' => 'Encargado del sistema de informacion',
            'activo' => 1
            
        )); 
        
        \DB::table('entidadperfil')->insert(array( 
            'identidad' => 1,  
            'idperfil' => 1
        )); 
    }
    
}
