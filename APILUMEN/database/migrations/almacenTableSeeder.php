<?php
use Illuminate\Database\Seeder;
//use Faker\Factory as Faker;
//use Illuminate\Database\ConnectionInterface;
 
class almacenTableSeeder extends Seeder{
    
    public function run(){                        
         
        $matriz = []; 
        $matriz[] = array('nombre'=>'Guia de entrada almacén');
        $matriz[] = array('nombre'=>'Guia de salida almacén');
        foreach ($matriz as $array) {
            \DB::table('documentofiscal')->insert(array(   
                'nombre' => $array['nombre'] 
            ));            
        }
        
        $matriz = []; 
        $matriz[] = array('idempresa'=>1, 'iddocumentofiscal'=>1, 'serie'=> '001', 'serienumero' => 3);
        $matriz[] = array('idempresa'=>1, 'iddocumentofiscal'=>2, 'serie'=> '001', 'serienumero' => 1);
        
        foreach ($matriz as $array) {
            \DB::table('serie')->insert(array(
                'idempresa' => 1,
                'iddocumentofiscal' => $array['iddocumentofiscal'],
                'serie' => $array['serie'],
                'serienumero' => $array['serienumero']
            ));
        }
                
        $matriz = []; 
        $matriz[] = array('nombre'=>'En espera');
        $matriz[] = array('nombre'=>'Atendido');
        $matriz[] = array('nombre'=>'Anulado');
        
        foreach ($matriz as $array) {
            \DB::table('estadodocumento')->insert(array(   
                'nombre' => $array['nombre'] 
            ));            
        } 
         
        $matriz = [];
        $matriz[] = array('nombre'=>'Genérico');
        $matriz[] = array('nombre'=>'Detallado'); 
        
        foreach ($matriz as $array) {
            \DB::table('tipostock')->insert($array);            
        }
        
        $matriz = [];
        $matriz[] = array('idempresa'=>1, 'idtipoproducto'=>1, 'idunidadmedida'=>1, 'nombre' =>'producto A', 'activo' => '1', 'idtipostock' => 1);
        $matriz[] = array('idempresa'=>1, 'idtipoproducto'=>1, 'idunidadmedida'=>1, 'nombre' =>'producto B', 'activo' => '1', 'idtipostock' => 1);
        $matriz[] = array('idempresa'=>1, 'idtipoproducto'=>1, 'idunidadmedida'=>1, 'nombre' =>'producto C', 'activo' => '1', 'idtipostock' => 1);
        $matriz[] = array('idempresa'=>1, 'idtipoproducto'=>1, 'idunidadmedida'=>1, 'nombre' =>'producto D', 'activo' => '1', 'idtipostock' => 2);
        $matriz[] = array('idempresa'=>1, 'idtipoproducto'=>1, 'idunidadmedida'=>1, 'nombre' =>'producto E', 'activo' => '1', 'idtipostock' => 2);
        
        foreach ($matriz as $array) {
            \DB::table('producto')->insert($array);            
        } 
    } 
}