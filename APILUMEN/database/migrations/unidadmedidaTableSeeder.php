<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Database\ConnectionInterface;
 
class unidadmedidaTableSeeder extends Seeder{
    
    public function run(){        
        // 1: UN Unidad 
        // 2: AU Servicio
        // 3: M Metro
        // 4: K  Kit
        // 5: Kx3  Kit por 3
        
        $matriz[] = array('idempresa' => 1, 'nombre'=>'Unidad','abreviatura'=>'Unid.');        
        $matriz[] = array('idempresa' => 1, 'nombre'=>'Kilogramo','abreviatura'=>'Kg.');        
        $matriz[] = array('idempresa' => 1, 'nombre'=>'Metro','abreviatura'=>'M.');     
        $matriz[] = array('idempresa' => 1, 'nombre'=>'Litro','abreviatura'=>'L.');     
        $matriz[] = array('idempresa' => 1, 'nombre'=>'Mililitro','abreviatura'=>'Ml.');     
        //$matriz[] = array('idempresa' => 1, 'nombre'=>'Mililitro','abreviatura'=>'Ml.');     
        
        foreach ($matriz as $array) {            
                \DB::table('unidadmedida')->insert(array(
                    'idempresa' => $array['idempresa'],
                    'nombre' => $array['nombre'],
                    'abreviatura' => $array['abreviatura'] 
                ));
        }
    }
    
}
