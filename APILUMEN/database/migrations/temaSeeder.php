<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Database\ConnectionInterface;
 
class temaTableSeeder extends Seeder{
    
    public function run(){        
        
        $matriz = []; 
        $matriz[] = array('nombre'=>'ajax', 'archivo'=>'ajax.theme.css', 'imgvista' => '1.jpg');
        $matriz[] = array('nombre'=>'alizarin-crimson', 'archivo'=>'alizarin-crimson.theme.css', 'imgvista' => '2.jpg');
        $matriz[] = array('nombre'=>'amazon', 'archivo'=>'amazon.theme.css', 'imgvista' => '3.jpg');
        $matriz[] = array('nombre'=>'amber', 'archivo'=>'amber.theme.css', 'imgvista' => '4.jpg');
        $matriz[] = array('nombre'=>'android-green', 'archivo'=>'android-green.theme.css', 'imgvista' => '5.jpg');
        $matriz[] = array('nombre'=>'angularjs-theme', 'archivo'=>'angularjs-theme.css', 'imgvista' => '6.jpg');
        $matriz[] = array('nombre'=>'antique-brass', 'archivo'=>'antique-brass.theme.css', 'imgvista' => '1.jpg');
        $matriz[] = array('nombre'=>'antique-bronze', 'archivo'=>'antique-bronze.theme', 'imgvista' => '2.jpg');           
        
        foreach ($matriz as $array) {
            \DB::table('tema')->insert(array( 
                'nombre' => $array['nombre'],
                'archivo' => $array['archivo'],
                'imgvista' => $array['imgvista'] 
            ));            
        }
        
        $matriz = []; 
        $matriz[] = array('nombre'=>'Documento Nacional de identidad', 'abreviatura'=>'DNI');
        $matriz[] = array('nombre'=>'Código de Indentificación Fiscal', 'abreviatura'=>'CIF');
        $matriz[] = array('nombre'=>'Número de Identidad de Extranjero', 'abreviatura'=>'NIE');
        foreach ($matriz as $array) {
            \DB::table('documento')->insert(array(  
                'nombre' => $array['nombre'],
                'abreviatura' => $array['abreviatura'] 
            ));            
        }
        
        $matriz = []; 
        $matriz[] = array('nombre'=>'Productos');
        $matriz[] = array('nombre'=>'Servicios');
        $matriz[] = array('nombre'=>'Localización');
        $matriz[] = array('nombre'=>'Grupo');
        $matriz[] = array('nombre'=>'Familia');        
        
        foreach ($matriz as $array) {
            \DB::table('categoria')->insert(array(  
                'nombre' => $array['nombre'] 
            ));
        }
        
        $matriz = []; 
        $matriz[] = array('idcategoria'=>1, 'idempresa'=>1, 'parent'=>5 ,'nombre'=>'B1.1');        
        $matriz[] = array('idcategoria'=>1, 'idempresa'=>1, 'parent'=>6 ,'nombre'=>'A1');
        $matriz[] = array('idcategoria'=>1, 'idempresa'=>1, 'parent'=>NULL ,'nombre'=>'B');//3        
        $matriz[] = array('idcategoria'=>1, 'idempresa'=>1, 'parent'=>6 ,'nombre'=>'A2');
        $matriz[] = array('idcategoria'=>1, 'idempresa'=>1, 'parent'=>3 ,'nombre'=>'B1');
        $matriz[] = array('idcategoria'=>1, 'idempresa'=>1, 'parent'=>NULL ,'nombre'=>'A');//6
        //7
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>NULL,'nombre'=>'Tratamientos'); 
        //8
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>7,'nombre'=>'MEDICINA ESTÉTICA'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>7,'nombre'=>'MEDICAL SPA'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>7,'nombre'=>'TRATAMIENTOS CAPILARES'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>7,'nombre'=>'NUTRICIÓN'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>7,'nombre'=>'CIRUGÍA ESTÉTICA'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>7,'nombre'=>'CIRUGÍA VASCULAR'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>7,'nombre'=>'FISIOTERAPIA'); 
        //15
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>8,'nombre'=>'TRATAMIENTOS FACIALES'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Ácido Hialurónico'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Botox'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Hilos tensores PDO'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Hilos tensores Happy Lift'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Plasma Rico en Plaquetas PRP'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Peeling Quimico'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Tratamiento despigmentante'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Láser IPL '); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Vitaminas faciales'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>15,'nombre'=>'Profhilo'); 
        //26
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>8,'nombre'=>'TRATAMIENTOS CORPORALES'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>26,'nombre'=>'Aqualyx'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>26,'nombre'=>'Esclerosis de varices'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>26,'nombre'=>'Vasculight'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>26,'nombre'=>'Mesoterapia corporal con Alidya'); 
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>26,'nombre'=>'Depilación láser médica'); 
        //32
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Radiofrecuencia facial');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Crioterapia');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Cavitación');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Termosudación');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Presoterapia');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Masajes');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Radiofrecuencia');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Limpieza facial');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>9,'nombre'=>'Tratamientos Alqvimia');
        //41
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>10,'nombre'=>'Infiltraciones capilares');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>10,'nombre'=>'Microinjertos capilares');
        //43
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Estudio Antropométrico A');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Estudio Antropométrico B');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Estudio Antropométrico C');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Pack OB A');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Pack OB B');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Pack OB C');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Dieta de aporte proteico');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Procedimiento POSE');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Balón Intragástrico');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>11,'nombre'=>'Test de intolerancia alimentaria');
        //53
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>12,'nombre'=>'CIRUGÍA FACIAL');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>53,'nombre'=>'Blefaroplastia');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>53,'nombre'=>'Otoplastia');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>53,'nombre'=>'Lipofiling');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>53,'nombre'=>'Resección bolas bichart');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>53,'nombre'=>'Dermaroller');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>53,'nombre'=>'Lifting');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>53,'nombre'=>'Rinoplastia');
        //61
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>12,'nombre'=>'CIRUGÍA MAMARIA');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>61,'nombre'=>'Aumento de mama');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>61,'nombre'=>'Reducción de mama');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>61,'nombre'=>'Mastopexia');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>61,'nombre'=>'Aumento con lipofilling');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>61,'nombre'=>'Aumento + pexia');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>61,'nombre'=>'Aumento + pexia + lipofilling');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>61,'nombre'=>'Ginecomastia');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>61,'nombre'=>'Pezones invertidos');
        //70
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>12,'nombre'=>'CIRUGÍA CORPORAL');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>70,'nombre'=>'Liposucción');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>70,'nombre'=>'Dermolipectomía');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>70,'nombre'=>'Cruroplastia, lifting muslos');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>70,'nombre'=>'Braquiplastia, lifting brazo');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>70,'nombre'=>'Genital');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>70,'nombre'=>'Cicatrices');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>70,'nombre'=>'Queloides');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>70,'nombre'=>'Exéresis lesión cutánea');
        //79
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>13,'nombre'=>'Esclerosis con espuma');
        $matriz[] = array('idcategoria'=>2, 'idempresa'=>1, 'parent'=>13,'nombre'=>'Ecodoppler');
        
        foreach ($matriz as $array) {
            \DB::table('arbol')->insert(array(                  
                'idcategoria' => $array['idcategoria'],
                'idempresa' => $array['idempresa'],
                'parent' => $array['parent'],
                'nombre' => $array['nombre'] 
            ));            
        }
        
    }
    
}
