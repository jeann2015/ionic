<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Database\ConnectionInterface;
 
class ubigeoTableSeeder extends Seeder{
    
    public function run(){        
                
        
        
        
        $matriz[] = array('idubigeo'=>'AR0000000', 'pais'=>'AR', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Argentina', 'nacionalidad' => 'Argentino');
        $matriz[] = array('idubigeo'=>'BO0000000', 'pais'=>'BO', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Bolivia', 'nacionalidad' => 'Boliviano');
        $matriz[] = array('idubigeo'=>'BR0000000', 'pais'=>'BR', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Brasil', 'nacionalidad' => 'Brasilero');
        $matriz[] = array('idubigeo'=>'CH0000000', 'pais'=>'CH', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Chile', 'nacionalidad' => 'Chileno');
        $matriz[] = array('idubigeo'=>'CO0000000', 'pais'=>'CO', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Colombia', 'nacionalidad' => 'Colombiano');
        $matriz[] = array('idubigeo'=>'ES0000000', 'pais'=>'ES', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'España', 'nacionalidad' => 'Español');
        $matriz[] = array('idubigeo'=>'EC0000000', 'pais'=>'EC', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Ecuador', 'nacionalidad' => 'Ecuatoriano');
        $matriz[] = array('idubigeo'=>'PY0000000', 'pais'=>'PY', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Paraguay', 'nacionalidad' => 'Paraguayo');
        $matriz[] = array('idubigeo'=>'PE0000000', 'pais'=>'PE', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Perú', 'nacionalidad' => 'Peruano');
        $matriz[] = array('idubigeo'=>'UY0000000', 'pais'=>'UY', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Uruguay', 'nacionalidad' => 'Uruguayo');
        $matriz[] = array('idubigeo'=>'VE0000000', 'pais'=>'VE', 'dpto'=>'000', 'prov' => '00', 'dist' => '00', 'nombre' => 'Venezuela', 'nacionalidad' => 'Venezolano');
        
        
        $matriz[] = array('idubigeo'=>'PE0010000', 'pais'=>'PE', 'dpto'=>'001', 'prov' => '00', 'dist' => '00', 'nombre' => 'Amazonas', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0010100', 'pais'=>'PE', 'dpto'=>'001', 'prov' => '01', 'dist' => '00', 'nombre' => 'Chachapoyas', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0010101', 'pais'=>'PE', 'dpto'=>'001', 'prov' => '01', 'dist' => '01', 'nombre' => 'Chachapoyas', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0010102', 'pais'=>'PE', 'dpto'=>'001', 'prov' => '01', 'dist' => '02', 'nombre' => 'Asunción', 'nacionalidad' => '');
        
        $matriz[] = array('idubigeo'=>'PE0020000', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '00', 'dist' => '00', 'nombre' => 'Madre de Dios', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020100', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '01', 'dist' => '00', 'nombre' => 'Manu', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020200', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '02', 'dist' => '00', 'nombre' => 'Tahuamanu', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020300', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '03', 'dist' => '00', 'nombre' => 'Tambopata', 'nacionalidad' => '');
        
        $matriz[] = array('idubigeo'=>'PE0020101', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '01', 'dist' => '01', 'nombre' => 'Fitzcarrald', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020102', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '01', 'dist' => '02', 'nombre' => 'Manu', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020103', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '01', 'dist' => '03', 'nombre' => 'Madre de Dios', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020104', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '01', 'dist' => '04', 'nombre' => 'Heupetuhe', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020201', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '02', 'dist' => '01', 'nombre' => 'Iñapari', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020202', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '02', 'dist' => '02', 'nombre' => 'Iberia', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020203', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '02', 'dist' => '03', 'nombre' => 'Tahuamanu', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020301', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '03', 'dist' => '01', 'nombre' => 'Tambopata', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020302', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '03', 'dist' => '02', 'nombre' => 'Inambari', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020303', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '03', 'dist' => '03', 'nombre' => 'Las piedras', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0020304', 'pais'=>'PE', 'dpto'=>'002', 'prov' => '03', 'dist' => '04', 'nombre' => 'Laberinto', 'nacionalidad' => '');
        
        $matriz[] = array('idubigeo'=>'PE0030000', 'pais'=>'PE', 'dpto'=>'003', 'prov' => '00', 'dist' => '00', 'nombre' => 'Cajamarca', 'nacionalidad' => '');
        
        $matriz[] = array('idubigeo'=>'PE0040000', 'pais'=>'PE', 'dpto'=>'004', 'prov' => '00', 'dist' => '00', 'nombre' => 'Lima', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'PE0040100', 'pais'=>'PE', 'dpto'=>'004', 'prov' => '01', 'dist' => '00', 'nombre' => 'Cañete', 'nacionalidad' => '');
        
        $matriz[] = array('idubigeo'=>'ES0010000', 'pais'=>'ES', 'dpto'=>'001', 'prov' => '00', 'dist' => '00', 'nombre' => 'Salamanca', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'ES0010100', 'pais'=>'ES', 'dpto'=>'001', 'prov' => '01', 'dist' => '00', 'nombre' => 'Salamanca', 'nacionalidad' => ''); 
        $matriz[] = array('idubigeo'=>'ES0010200', 'pais'=>'ES', 'dpto'=>'001', 'prov' => '02', 'dist' => '00', 'nombre' => 'Béjar', 'nacionalidad' => ''); 
        
        $matriz[] = array('idubigeo'=>'ES0020000', 'pais'=>'ES', 'dpto'=>'002', 'prov' => '00', 'dist' => '00', 'nombre' => 'Barcelona', 'nacionalidad' => '');
        $matriz[] = array('idubigeo'=>'ES0020100', 'pais'=>'ES', 'dpto'=>'002', 'prov' => '01', 'dist' => '00', 'nombre' => 'Tarrasa', 'nacionalidad' => ''); 
        $matriz[] = array('idubigeo'=>'ES0020200', 'pais'=>'ES', 'dpto'=>'002', 'prov' => '02', 'dist' => '00', 'nombre' => 'Badalona', 'nacionalidad' => ''); 
        
        foreach ($matriz as $array) {
                \DB::table('ubigeo')->insert(array( 
                    'idubigeo' => $array['idubigeo'],
                    'pais' => $array['pais'],
                    'dpto' => $array['dpto'],
                    'prov' => $array['prov'], 
                    'dist' => $array['dist'], 
                    'nombre' => $array['nombre'], 
                    'nacionalidad' => $array['nacionalidad'] 
                ));            
        }
    }
    
}
