<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Database\ConnectionInterface;
 
class entidadTableSeeder extends Seeder{
    
    public function run(){        
        
        $matriz[] = array( 
            'idempresa'=>1,
            'iddocumento' =>1,              
            'numerodoc'=>'44120026A', 
            'apellidopat'=>'Chauca',
            'apellidomat'=>'Chavez',
            'nombre'=>'Julio César',
            'entidad'=>'Chauca Chavez, Julio César',
            'password'=>'44120026A',
            'imgperfil'=>'',
            'email'=>'chaucachavez@gmail.com',
            'tipopersonal'=>'1',
            'idcargoorg'=>1,
            'fechanacimiento'=>'1988-07-01',
            'sexo'=>'M',
            'telefono'=>'01 653321',
            'celular'=>'997592538',
            'idubigeo'=>'PE0020301',
            'direccion'=>'Av.Canadá 1585',
            'facebook'=>'f/bejuco.pe',
            'twitter'=>'#chaucachavez',
            'paginaweb'=>'www.lagranescuela.com',
            'whatsapp'=>'997 592 538',
            'acceso'=>1
            );
        
        $matriz[] = array(
            'idempresa'=>1,
            'iddocumento' =>1,              
            'numerodoc'=>'12345678B', 
            'apellidopat'=>'Pomari',
            'apellidomat'=>'Lopez',
            'nombre'=>'José Salomón',
            'entidad'=>'Pomari Lopez, José Salomón',
            'password'=>'12345678B',
            'imgperfil'=>'',
            'email'=>'maritza@gmail.com',
            'tipopersonal'=>'1',
            'idcargoorg'=>2,
            'fechanacimiento'=>NULL,
            'sexo'=>'M',            
            'telefono'=>'',
            'celular'=>'',
            'idubigeo'=>'',
            'direccion'=>'',
            'facebook'=>'',
            'twitter'=>'',
            'paginaweb'=>'',
            'whatsapp'=>'',
            'acceso'=>1
            ); 
        
        $matriz[] = array( 
            'idempresa'=>1,
            'iddocumento' =>1,              
            'numerodoc'=>'87654321C', 
            'apellidopat'=>'Chavez',
            'apellidomat'=>'Jara',
            'nombre'=>'Yolanda',
            'entidad'=>'Chavez Jara, Yolanda',
            'password'=>'87654321C',
            'imgperfil'=>'',
            'email'=>'yolanda@hotmail.com',
            'tipopersonal'=>'1',
            'idcargoorg'=>3,
            'fechanacimiento'=>NULL,
            'sexo'=>'F',            
            'telefono'=>'',
            'celular'=>'',
            'idubigeo'=>'',
            'direccion'=>'',
            'facebook'=>'',
            'twitter'=>'',
            'paginaweb'=>'',
            'whatsapp'=>'',
            'acceso'=>1
            );  
        
        foreach ($matriz as $array) {            
                \DB::table('entidad')->insert(array( 
                    'idempresa' => $array['idempresa'],
                    'iddocumento' => $array['iddocumento'],
                    'numerodoc' => $array['numerodoc'],
                    'apellidopat' => $array['apellidopat'],
                    'apellidomat' => $array['apellidomat'],
                    'nombre' => $array['nombre'],
                    'entidad' => $array['entidad'],
                    'password' => $array['password'],
                    'imgperfil' => $array['imgperfil'],
                    'email' => $array['email'],
                    'tipopersonal' => $array['tipopersonal'],
                    'idcargoorg' => $array['idcargoorg'],
                    'fechanacimiento'=>$array['fechanacimiento'],
                    'sexo'=>$array['sexo'],         
                    'telefono'=>$array['telefono'],
                    'celular'=>$array['celular'],
                    'idubigeo'=>$array['idubigeo'],
                    'direccion'=>$array['direccion'],
                    'facebook'=>$array['facebook'],
                    'twitter'=>$array['twitter'],
                    'paginaweb'=>$array['paginaweb'],
                    'whatsapp'=>$array['whatsapp'],
                    'acceso'=>$array['acceso']
                ));
        }
    }
    
}
