<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Database\ConnectionInterface;
 
class monedaTableSeeder extends Seeder{
    
    public function run(){        
        
        //$matriz[] = array('idempresa'=>1, 'nombre'=>'Nuevo Sol','simbolo'=>'S/.');
        //$matriz[] = array('idempresa'=>1, 'nombre'=>'Dolar Americano','simbolo'=>'$');        
        //$matriz[] = array('idempresa'=>1, 'nombre'=>'Euro','simbolo'=>'E'); 
        $matriz[] = array('idempresa'=>1, 'nombre'=>'Euro','simbolo'=>'€'); 
        
        foreach ($matriz as $array) {            
            \DB::table('moneda')->insert(array( 
                'idempresa' => $array['idempresa'],
                'nombre' => $array['nombre'],
                'simbolo' => $array['simbolo'] 
            ));            
        }
        
        $matriz = [];
        $matriz[] = array('nombre'=>'Producto');
        $matriz[] = array('nombre'=>'Servicio');         
        
        foreach ($matriz as $array) {
            \DB::table('tipoproducto')->insert(array(  
                'nombre' => $array['nombre'] 
            ));
        }
    }
    
}
