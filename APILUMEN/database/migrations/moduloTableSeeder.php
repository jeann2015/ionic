<?php

use Illuminate\Database\Seeder; 

class moduloTableSeeder extends Seeder {

    public function run() {
        //1
        $matriz[] = array('parent'=>null,'nombre'=>'Mantenimiento','icono'=>'fa fa-globe', 'url' => '#', 'orden' => 1, 'nivel' => 1);        
        $matriz[] = array('parent'=>null,'nombre'=>'Áreas','icono'=>'fa fa-globe', 'url' => '#', 'orden' => 2, 'nivel' => 1);
                
        //3 NIVEL-2
        $matriz[] = array('parent'=>1,'nombre'=>'Seguridad','icono'=>"fa fa-lock", 'url' => "#", 'orden' => 1, 'nivel' => 2);
        $matriz[] = array('parent'=>1,'nombre'=>'Entidades','icono'=>"fa fa-group", 'url' => "#", 'orden' => 2, 'nivel' => 2);
        $matriz[] = array('parent'=>1,'nombre'=>'Configuración','icono'=>"fa fa-cog", 'url' => "#", 'orden' => 3, 'nivel' => 2);
        $matriz[] = array('parent'=>1,'nombre'=>'Almacén','icono'=>"fa fa-cubes", 'url' => "#", 'orden' => 4, 'nivel' => 2);
        $matriz[] = array('parent'=>2,'nombre'=>'Medicina','icono'=>"fa fa-hospital-o", 'url' => "#", 'orden' => 1, 'nivel' => 2);        
        
        //8 NIVEL-3        
        $matriz[] = array('parent'=>3,'nombre'=>'Módulos','icono'=>'', 'url' => 'modulos', 'orden' => 1, 'nivel' => 3);
        $matriz[] = array('parent'=>3,'nombre'=>'Perfiles','icono'=>'', 'url' => 'perfiles', 'orden' => 2, 'nivel' => 3);
                
        $matriz[] = array('parent'=>4,'nombre'=>'Personal','icono'=>'', 'url' => 'personal', 'orden' => 1, 'nivel' => 3);
        $matriz[] = array('parent'=>4,'nombre'=>'Médicos','icono'=>'', 'url' => 'medicos', 'orden' => 2, 'nivel' => 3);
        $matriz[] = array('parent'=>4,'nombre'=>'Pacientes','icono'=>'', 'url' => 'clientes', 'orden' => 3, 'nivel' => 3);
        $matriz[] = array('parent'=>4,'nombre'=>'Proveedores','icono'=>'', 'url' => 'proveedores', 'orden' => 4, 'nivel' => 3);
        
        $matriz[] = array('parent'=>5,'nombre'=>'Empresa','icono'=>'', 'url' => 'empresa', 'orden' => 1, 'nivel' => 3);        
        
        $matriz[] = array('parent'=>6,'nombre'=>'Categorias','icono'=>'', 'url' => 'categorias', 'orden' => 1, 'nivel' => 3);
        $matriz[] = array('parent'=>6,'nombre'=>'productos','icono'=>'', 'url' => 'productos-y-servicios', 'orden' => 2, 'nivel' => 3);        
        $matriz[] = array('parent'=>6,'nombre'=>'Guía de entrada','icono'=>'', 'url' => 'guia-de-entrada', 'orden' => 3, 'nivel' => 3);        
        $matriz[] = array('parent'=>6,'nombre'=>'Guía de salida','icono'=>'', 'url' => 'guia-de-salida', 'orden' => 4, 'nivel' => 3);        
        $matriz[] = array('parent'=>6,'nombre'=>'Kardex','icono'=>'', 'url' => 'kardex', 'orden' => 5, 'nivel' => 3);        
                 
        $matriz[] = array('parent'=>7,'nombre'=>'Tratamientos','icono'=>'', 'url' => 'tratamientos', 'orden' => 1, 'nivel' => 3);
        $matriz[] = array('parent'=>7,'nombre'=>'Campañas','icono'=>'', 'url' => 'ofertas-medicas', 'orden' => 2, 'nivel' => 3);
        $matriz[] = array('parent'=>7,'nombre'=>'Alertas y tareas','icono'=>'', 'url' => 'alertas-tareas', 'orden' => 3, 'nivel' => 3);
 
        foreach ($matriz as $array) {
                $idmodulo = \DB::table('modulo')->insertGetId(array(  
                    'parent' => $array['parent'],
                    'nombre' => $array['nombre'],
                    'icono' => $array['icono'],
                    'url' => $array['url'],
                    'orden' => $array['orden'],
                    'nivel' => $array['nivel'] 
                ), 'idmodulo');                       
                
                //tabla: moduloempresa
                $idmoduloempresa = \DB::table('moduloempresa')->insertGetId(array( 
                    'idmodulo' => $idmodulo, 
                    'idempresa' => 1 
                ), 'idmoduloempresa');
                
                //tabla: entidadmodulo
//                \DB::table('entidadmodulo')->insert(array(  
//                    'identidad' => 1,
//                    'idmodulo' => $idmodulo 
//                ));
                
                //tabla: perfilmodulo
                \DB::table('perfilmodulo')->insert(array(  
                    'idperfil' => 1,
                    'idmodulo' => $idmodulo 
                ));
        }
    }

}
