<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/ 

use App\Models\entidad;

$app->get('/sql', function () use ($app) {      
    $data = entidad::get(); 
    dd($data);  
});

$app->get('/', function ()  use ($app) {
    return $app->welcome();
});

$app->get('/phpinfo', function () {
    return phpinfo();
});
 
//Empresa
//$app->post('/{enterprise}/uploads', ['middleware' => 'cors', 'uses' => 'empresaController@uploads']);    
$app->get('/{enterprise}/empresa/home', ['middleware' => 'cors', 'uses' => 'empresaController@home']);
$app->group(['middleware' => ['cors' ]], function ($app)
{   
    $app->get('/{enterprise}/empresa', 'App\Http\Controllers\empresaController@show');
    $app->post('/{enterprise}/empresa', 'App\Http\Controllers\empresaController@update');
});
//End Empresa 

//Entidad 
$app->post('/authenticate', ['middleware' => 'cors', 'uses' => 'entidadController@authenticate']);
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{    
    $app->get('/{enterprise}/entidad/modulos', 'App\Http\Controllers\entidadController@modulos');
    $app->get('/{enterprise}/entidad', 'App\Http\Controllers\entidadController@index');
    $app->get('/{enterprise}/entidad/cumpleanos', 'App\Http\Controllers\entidadController@cumpleanos');
    $app->get('/{enterprise}/entidad/new', 'App\Http\Controllers\entidadController@newentidad');    
    $app->get('/{enterprise}/entidad/{id}', 'App\Http\Controllers\entidadController@show');
    $app->get('/{enterprise}/entidad/profile/{id}', 'App\Http\Controllers\entidadController@showprofile');
    $app->get('/{enterprise}/entidad/documento/nro', 'App\Http\Controllers\entidadController@nrodocumento');
    $app->post('/{enterprise}/entidad', 'App\Http\Controllers\entidadController@store');    
    $app->post('/{enterprise}/entidad/{id}', 'App\Http\Controllers\entidadController@update');
    $app->post('/{enterprise}/entidad/delete/{id}', 'App\Http\Controllers\entidadController@destroy');        
    $app->post('/{enterprise}/entidad/subentidad/{id}', 'App\Http\Controllers\entidadController@updatesubentidad');
    $app->post('/{enterprise}/entidad/password/{id}', 'App\Http\Controllers\entidadController@updatepassword');
});
//End Entidad

//Contacto
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{ 
    $app->get('/{enterprise}/contacto', 'App\Http\Controllers\contactoController@index');  
    $app->post('/{enterprise}/contacto', 'App\Http\Controllers\contactoController@store');
    $app->post('/{enterprise}/contacto/{id}', 'App\Http\Controllers\contactoController@update');
    $app->post('/{enterprise}/contacto/delete/{id}', 'App\Http\Controllers\contactoController@destroy');
});
//End Contacto

//Tree
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{
    $app->get('/{enterprise}/categoria', 'App\Http\Controllers\arbolController@index'); 
    $app->post('/{enterprise}/categoria', 'App\Http\Controllers\arbolController@store');
    $app->post('/{enterprise}/categoria/{id}', 'App\Http\Controllers\arbolController@update');    
    $app->post('/{enterprise}/categoria/delete/{id}', 'App\Http\Controllers\arbolController@destroy');
});
//End Tree 

//Producto
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{
    $app->get('/{enterprise}/producto', 'App\Http\Controllers\productoController@index');
    $app->get('/{enterprise}/producto/stock', 'App\Http\Controllers\productoController@indexstock'); 
    $app->get('/{enterprise}/producto/stock/det', 'App\Http\Controllers\productoController@indexstockdet'); 
    $app->get('/{enterprise}/producto/kardex', 'App\Http\Controllers\productoController@indexkardex'); 
    $app->get('/{enterprise}/producto/new', 'App\Http\Controllers\productoController@newproducto');
    $app->get('/{enterprise}/producto/{id}', 'App\Http\Controllers\productoController@show');
    $app->post('/{enterprise}/producto', 'App\Http\Controllers\productoController@store');
    $app->post('/{enterprise}/producto/{id}', 'App\Http\Controllers\productoController@update');
    $app->post('/{enterprise}/producto/delete/{id}', 'App\Http\Controllers\productoController@destroy');
});
//End Producto 

//Guia
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{
    $app->get('/{enterprise}/guia', 'App\Http\Controllers\guiaController@index');
    $app->get('/{enterprise}/guia/new', 'App\Http\Controllers\guiaController@newguia');
    $app->get('/{enterprise}/guia/{id}', 'App\Http\Controllers\guiaController@show');
    $app->post('/{enterprise}/guia', 'App\Http\Controllers\guiaController@store');
    $app->post('/{enterprise}/guia/{id}', 'App\Http\Controllers\guiaController@update');
    $app->post('/{enterprise}/guia/delete/{id}', 'App\Http\Controllers\guiaController@destroy');
});
//End Guia 

//Modulos
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{   
    $app->get('/{enterprise}/modulo', 'App\Http\Controllers\moduloController@index'); 
    $app->post('/{enterprise}/modulo', 'App\Http\Controllers\moduloController@store');
    $app->post('/{enterprise}/modulo/{id}', 'App\Http\Controllers\moduloController@update');    
    $app->post('/{enterprise}/modulo/delete/{id}', 'App\Http\Controllers\moduloController@destroy');
});
//End Modulos 

//Perfiles
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{
    $app->get('/{enterprise}/perfil', 'App\Http\Controllers\perfilController@index');  
    $app->post('/{enterprise}/perfil', 'App\Http\Controllers\perfilController@store');
    $app->get('/{enterprise}/perfil/{id}', 'App\Http\Controllers\perfilController@show');
    $app->post('/{enterprise}/perfil/{id}', 'App\Http\Controllers\perfilController@update');    
    $app->post('/{enterprise}/perfil/perfilmodulo/{id}', 'App\Http\Controllers\perfilController@update_perfilmodulo');    
    $app->post('/{enterprise}/perfil/delete/{id}', 'App\Http\Controllers\perfilController@destroy');
});
//End Perfiles 

//Sede
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{ 
    $app->get('/{enterprise}/sede', 'App\Http\Controllers\sedeController@index');  
    $app->post('/{enterprise}/sede', 'App\Http\Controllers\sedeController@store');
    $app->post('/{enterprise}/sede/{id}', 'App\Http\Controllers\sedeController@update');
    $app->post('/{enterprise}/sede/delete/{id}', 'App\Http\Controllers\sedeController@destroy');
});
//End Sede

//Ubigeo
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{
    $app->get('/{enterprise}/ubigeo', 'App\Http\Controllers\ubigeoController@index'); 
});
//End Ubigeo   

//Promocion
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{
    $app->get('/{enterprise}/promocion', 'App\Http\Controllers\promocionController@index');  
    $app->post('/{enterprise}/promocion', 'App\Http\Controllers\promocionController@store');
    $app->get('/{enterprise}/promocion/{id}', 'App\Http\Controllers\promocionController@show');
    $app->post('/{enterprise}/promocion/{id}', 'App\Http\Controllers\promocionController@update');        
    $app->post('/{enterprise}/promocion/delete/{id}', 'App\Http\Controllers\promocionController@destroy');
});
//End Promocion 


//Citas
$app->group(['middleware' => ['cors', 'ch.token']], function ($app)
{
    $app->get('/{enterprise}/citas', 'App\Http\Controllers\citasController@index');  
    // $app->post('/{enterprise}/promocion', 'App\Http\Controllers\promocionController@store');
    // $app->get('/{enterprise}/promocion/{id}', 'App\Http\Controllers\promocionController@show');
    // $app->post('/{enterprise}/promocion/{id}', 'App\Http\Controllers\promocionController@update');        
    // $app->post('/{enterprise}/promocion/delete/{id}', 'App\Http\Controllers\promocionController@destroy');
});
//End Citas