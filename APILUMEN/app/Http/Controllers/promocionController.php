<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\empresa;
use App\Models\promocion;
use App\Models\producto;
use \Firebase\JWT\JWT;

class promocionController extends Controller {

    private function formatFecha($fecha, $format = 'dd/mm/yyyy') {

        $newFecha = NULL;
        if (!empty($fecha) && strlen($fecha) == 10) {
            if ($format === 'dd/mm/yyyy') {
                //de: yyyy-mm-dd a: dd/mm/yyyy 
                $fecha = explode('-', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
            if ($format === 'yyyy-mm-dd') {
                //de: dd/mm/yyyy a: yyyy-mm-dd 
                $fecha = explode('/', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
        }
        return $newFecha;
    }

    public function index(Request $request, $enterprise) {

        $paramsTMP = $request->all();

        $empresa = new empresa();
        $promocion = new promocion();

        $param = array();
        $param['promocion.idempresa'] = $empresa->idempresa($enterprise);

        $orderName = !empty($paramsTMP['orderName']) ? $paramsTMP['orderName'] : 'promocion.nombre';
        $orderSort = !empty($paramsTMP['orderSort']) ? $paramsTMP['orderSort'] : 'ASC';
        $pageSize = !empty($paramsTMP['pageSize']) ? $paramsTMP['pageSize'] : 25;

        $like = !empty($paramsTMP['likenombre']) ? trim($paramsTMP['likenombre']) : '';
        $data = $promocion->grid($param, $like, $pageSize, $orderName, $orderSort);

        if ($data) {
            return $this->crearRespuesta($data->items(), 200, $data->total(), $data->count() . '|' . $orderName . '|' . $orderSort, $paramsTMP);
        }

        return $this->crearRespuestaError('Promoción no encontrado', 404);
    }

    public function show($enterprise, $id) {

        $promocion = promocion::find($id);

        if ($promocion) {
            $promocion->desde = $this->formatFecha($promocion->desde);
            $promocion->hasta = $this->formatFecha($promocion->hasta);

            $listcombox = [];
            if ($promocion->idtipopromocion === 1) { //1: Paquete 2: Tratamiento
                $listcombox['promociondet'] = $promocion->promociondet(['idpromocion' => $id]);
            } else {
                $listcombox['producto'] = producto::find($promocion->idproducto);
            }

            return $this->crearRespuesta($promocion, 200, '', '', $listcombox);
        }

        return $this->crearRespuestaError('Promoción no encotrado', 404);
    }

    public function store(Request $request, $enterprise) {

        $empresa = new empresa();

        $idempresa = $empresa->idempresa($enterprise);
        $request = $request->all();


        $request['promocion']['idempresa'] = $idempresa;
        $request['promocion']['desde'] = $this->formatFecha($request['promocion']['desde'], 'yyyy-mm-dd');
        $request['promocion']['hasta'] = $this->formatFecha($request['promocion']['hasta'], 'yyyy-mm-dd');
        $request['promocion']['activo'] = '1';

        \DB::beginTransaction();
        try {
            //Graba en 2 tablas(promocion, promociondet)            
            $promocion = promocion::create($request['promocion']);
            $id = $promocion->idpromocion;

            if (isset($request['promociondet'])) {
                $dataPromocionDet = [];
                foreach ($request['promociondet'] as $row) {
                    $row['idpromocion'] = $id;
                    $dataPromocionDet[] = $row;
                }

                $promocion->GrabarPromociondet($dataPromocionDet, $id);
            }
        } catch (QueryException $e) {
            \DB::rollback();
        }
        \DB::commit();

        return $this->crearRespuesta('"' . $promocion->nombre . '" ha sido creado.', 201);
    }

    public function update(Request $request, $enterprise, $id) {

        $promocion = promocion::find($id);

        if ($promocion) {
            $request = $request->all();
            $request['promocion']['desde'] = $this->formatFecha($request['promocion']['desde'], 'yyyy-mm-dd');
            $request['promocion']['hasta'] = $this->formatFecha($request['promocion']['hasta'], 'yyyy-mm-dd');
            //VALIDACIONES 
            $promocion->fill($request['promocion']);

            \DB::beginTransaction();
            try {
                //Graba en 2 tablas(promocion, promociondet)
                $promocion->save();

                if (isset($request['promociondet'])) {
                    $dataPromocionDet = [];
                    foreach ($request['promociondet'] as $row) {
                        $row['idpromocion'] = $id;
                        $dataPromocionDet[] = $row;
                    }
                    $promocion->GrabarPromociondet($dataPromocionDet, $id);
                }
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta('"' . $promocion->nombre . '" ha sido editado. ', 200);
        }

        return $this->crearRespuestaError('El id especificado no corresponde a una Promoción', 404);
    }

    public function destroy($enterprise, $id) {

        $promocion = promocion::find($id);

        if ($promocion) {
            \DB::beginTransaction();
            try {
                //Elimina en 1 tablas(producto, productoservicio)        
                $promocion->delete();
                $promocion->GrabarPromociondet([], $id);
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta('"' . $promocion->nombre . '" a sido eliminado.', 200);
        }
        return $this->crearRespuestaError('Promoción no encotrado', 404);
    }

}
