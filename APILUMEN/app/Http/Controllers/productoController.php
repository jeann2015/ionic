<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\empresa;
use App\Models\producto;
use \Firebase\JWT\JWT;

class productoController extends Controller {

    public function index(Request $request, $enterprise) {

        $paramsTMP = $request->all();

        $empresa = new empresa();
        $producto = new producto();

        $param = array();
        $param['producto.idempresa'] = $empresa->idempresa($enterprise);
        if (isset($paramsTMP['idtipoproducto'])) {
            $param['producto.idtipoproducto'] = $paramsTMP['idtipoproducto'];
        }

        $orderName = !empty($paramsTMP['orderName']) ? $paramsTMP['orderName'] : 'producto.nombre';
        $orderSort = !empty($paramsTMP['orderSort']) ? $paramsTMP['orderSort'] : 'ASC';
        $pageSize = !empty($paramsTMP['pageSize']) ? $paramsTMP['pageSize'] : 25;

        $like = !empty($paramsTMP['likeproducto']) ? trim($paramsTMP['likeproducto']) : '';
        $data = $producto->grid($param, $like, $pageSize, $orderName, $orderSort);

        if ($data) {
            return $this->crearRespuesta($data->items(), 200, $data->total());
        }

        return $this->crearRespuestaError('Producto no encontrado', 404);
    }

    public function indexstock(Request $request, $enterprise) {

        $paramsTMP = $request->all();

        $empresa = new empresa();
        $producto = new producto();

        $param = array();

        $idempresa = $empresa->idempresa($enterprise);

        $param['producto.idempresa'] = $idempresa;
        if (isset($paramsTMP['idproducto'])) {
            $param['producto.idproducto'] = $paramsTMP['idproducto'];
        }

        $orderName = !empty($paramsTMP['orderName']) ? $paramsTMP['orderName'] : 'producto.nombre';
        $orderSort = !empty($paramsTMP['orderSort']) ? $paramsTMP['orderSort'] : 'ASC';
        $pageSize = !empty($paramsTMP['pageSize']) ? $paramsTMP['pageSize'] : 25;

        $like = !empty($paramsTMP['likeproducto']) ? trim($paramsTMP['likeproducto']) : '';
        $data = $producto->gridstock($param, $like, $pageSize, $orderName, $orderSort);

        $data = $this->setStock($data, $idempresa);

        if ($data) {
            return $this->crearRespuesta($data->items(), 200, $data->total());
        }

        return $this->crearRespuestaError('Producto no encontrado', 404);
    }

    public function indexkardex(Request $request, $enterprise) {

        $empresa = new empresa();
        $producto = new producto();
        $param = array();

        $paramsTMP = $request->all();

        $param['producto.idempresa'] = $empresa->idempresa($enterprise);
        $param['producto.idproducto'] = $paramsTMP['idproducto'];
        $data = $producto->productoKardex($param);

        if ($data) {

            $kardex = [];
            $totales = ['entradas' => 0, 'salidas' => 0, 'existencias' => 0];

            $desprod = array(
                'idproducto' => $data[0]->idproducto,
                'codigo' => $data[0]->codigo,
                'categoria' => $data[0]->categoria,
                'nombre' => $data[0]->nombre,
                'unidadmedidaabrev' => $data[0]->unidadmedidaabrev,
                'stockmin' => $data[0]->stockmin
            );

            $existencia = 0;
            foreach ($data as $fila) {
                if ($fila->movimiento === 'I' || $fila->movimiento === 'S') {
                    if ($fila->movimiento === 'I') {
                        $existencia = $existencia + $fila->cantidad;
                        $totales['entradas'] = $totales['entradas'] + $fila->cantidad;
                    }
                    if ($fila->movimiento === 'S') {
                        $existencia = $existencia - $fila->cantidad;
                        $totales['salidas'] = $totales['salidas'] + $fila->cantidad;
                    }
                    $row = array(
                        'fechadocumento' => $fila->fechadocumento,
                        'movimiento' => $fila->movimiento,
                        'documento' => $fila->documento,
                        'numero' => $fila->serienumero,
                        'entradas' => ($fila->movimiento === 'I') ? $fila->cantidad : '',
                        'salidas' => ($fila->movimiento === 'S') ? $fila->cantidad : '',
                        'existencias' => $existencia
                    );

                    $kardex[] = $row;
                }
            }

            $totales['existencias'] = $totales['entradas'] - $totales['salidas'];

            $data = array(
                'producto' => $desprod,
                'kardex' => $kardex,
                'totales' => $totales
            );
            return $this->crearRespuesta($data, 200);
        }

        return $this->crearRespuestaError('Producto no encontrado', 404);
    }

    public function indexstockdet(Request $request, $enterprise) {

        $empresa = new empresa();
        $producto = new producto();

        $paramsTMP = $request->all();
        $param = array();

        $param['producto.idempresa'] = $empresa->idempresa($enterprise);
        $param['producto.idproducto'] = $paramsTMP['idproducto'];

        $stock = $producto->productostockdet($param);

        return $this->crearRespuesta($stock, 200);
    }

    private function setStock($data, $idempresa) {

        $producto = new producto();

        $whereIns = [];
        foreach ($data->items() as $row) {
            $whereIns[] = $row->idproducto;
            $row->stock = 0;
        }

        $stock = [];
        // Obtiene Ingresos y Salidas de productos de Almacendet, agrupado por(idproducto, movimiento) 
        if (!empty($whereIns)) {
            $stock = $producto->productostock($whereIns, $idempresa);
        }

        //Actualiza grid paginacion con Stock
        if (!empty($stock)) {
            foreach ($data->items() as $row) {
                if (isset($stock[$row->idproducto])) {
                    $row->stock = $stock[$row->idproducto]['stock'];
                    $row->stockalert = ($stock[$row->idproducto]['stock'] < $row->stockmin) ? true : false;
                }
            }
        }

        return $data;
    }

    public function show($enterprise, $id) {

        $empresa = new empresa();

        $producto = producto::find($id);

        if ($producto) {
            $idempresa = $empresa->idempresa($enterprise);

            $listcombox = array(
                'monedas' => $empresa->monedas($idempresa),
                'unidadmedidas' => $empresa->unidadmedidas($idempresa),
                'tipoproductos' => $empresa->tipoproductos(),
                'tipostock' => $empresa->tipostocks()
            );
            if ($producto->idtipoproducto === 2) {
                $param = array(
                    'producto.idproducto' => $id,
                    'producto.idempresa' => $producto->idempresa,
                );
                $listcombox['productoservicios'] = $producto->productoServicios($param);
            }

            return $this->crearRespuesta($producto, 200, '', '', $listcombox);
        }

        return $this->crearRespuestaError('Producto no encotrado', 404);
    }

    public function newproducto($enterprise) {

        $empresa = new empresa();

        $idempresa = $empresa->idempresa($enterprise);

        $listcombox = array(
            'monedas' => $empresa->monedas($idempresa),
            'unidadmedidas' => $empresa->unidadmedidas($idempresa),
            'tipoproductos' => $empresa->tipoproductos(),
            'tipostock' => $empresa->tipostocks()
        );

        return $this->crearRespuesta([], 200, '', '', $listcombox);
    }

    public function store(Request $request, $enterprise) {

        $empresa = new empresa();
        $idempresa = $empresa->idempresa($enterprise);
        $request = $request->all();

        $request['producto']['idempresa'] = $idempresa;

        //VALIDACIONES 

        if (!empty($request['producto']['codigo'])) {
            $codigoproducto = $request['producto']['codigo'];
            $producto = producto::where('codigo', '=', $codigoproducto)->first();
            if ($producto) {
                return $this->crearRespuesta('No puede registrarse, el c&oacute;digo de producto "' . $codigoproducto . '" ya existe. Pertenece a ' . $producto->nombre, [200, 'info']);
            }
        }

        $dataProdServ = [];

        \DB::beginTransaction();
        try {
            //Graba en 1 tablaa(producto)            
            $producto = producto::create($request['producto']);
            $id = $producto->idproducto;

            if (isset($request['productoservicio'])) {
                foreach ($request['productoservicio'] as $row) {
                    $dataProdServ[] = ['idproducto' => $id, 'idproductoitem' => $row['idproducto']];
                }
                $producto->GrabarProductoServicio($dataProdServ, $id);
            }
        } catch (QueryException $e) {
            \DB::rollback();
        }
        \DB::commit();

        return $this->crearRespuesta('"' . $producto->nombre . '" ha sido creado.', 201);
    }

    public function update(Request $request, $enterprise, $id) {

        $producto = producto::find($id);

        if ($producto) {
            $request = $request->all();

            //VALIDACIONES 
            $codigoproducto = $request['producto']['codigo'];
            $consultado = false;
            if ($codigoproducto !== $producto->codigo) {
                $consultado = true;
                $row = producto::where('codigo', '=', $codigoproducto)->first();
                if ($row) {
                    return $this->crearRespuesta('No puede registrarse, el c&oacute;digo de producto "' . $codigoproducto . '" ya existe. Pertenece a ' . $row->nombre, [200, 'info']);
                }
            }

            $producto->fill($request['producto']);

            \DB::beginTransaction();
            try {
                //Graba en 2 tablaa(producto, productoservicio)                                   
                $producto->save();

                if (isset($request['productoservicio'])) {
                    $dataProdServ = [];
                    foreach ($request['productoservicio'] as $row) {
                        $dataProdServ[] = ['idproducto' => $id, 'idproductoitem' => $row['idproducto']];
                    }
                    $producto->GrabarProductoServicio($dataProdServ, $id);
                }
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta('Producto "' . $producto->nombre . '" ha sido editado. ', 200);
        }

        return $this->crearRespuestaError('El id especificado no corresponde a un producto', 404);
    }

    public function destroy($enterprise, $id) {

        $producto = producto::find($id);

        if ($producto) {

            $return = $producto->validadorDataRelacionada($id);
            if ($return['validator']) {
                return $this->crearRespuesta($return['message'], [200, 'info']);
            }


            \DB::beginTransaction();
            try {
                //Elimina en 1 tablas(producto, productoservicio)        
                $producto->delete();
                $producto->GrabarProductoServicio([], $id);
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta('Producto "' . $producto->nombre . '" a sido eliminado.', 200);
        }
        return $this->crearRespuestaError('producto no encotrado', 404);
    }

}
