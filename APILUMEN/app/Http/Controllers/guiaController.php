<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\empresa;
use App\Models\producto;
use App\Models\entidad;
use App\Models\guia;
use App\Models\almacen;
use \Firebase\JWT\JWT;

class guiaController extends Controller {

    private function formatFecha($fecha, $format = 'dd/mm/yyyy') {
        $newFecha = NULL;
        if (!empty($fecha) && strlen($fecha) == 10) {
            if ($format === 'dd/mm/yyyy') {
                //de: yyyy-mm-dd a: dd/mm/yyyy 
                $fecha = explode('-', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
            if ($format === 'yyyy-mm-dd') {
                //de: dd/mm/yyyy a: yyyy-mm-dd 
                $fecha = explode('/', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
        }
        return $newFecha;
    }

    public function index(Request $request, $enterprise) {

        $paramsTMP = $request->all();

        $empresa = new empresa();
        $guia = new guia();

        $param = array();
        $param['guia.idempresa'] = $empresa->idempresa($enterprise);
        $param['guia.movimiento'] = $paramsTMP['movimiento'];

        $orderName = !empty($paramsTMP['orderName']) ? $paramsTMP['orderName'] : 'guia.serienumero';
        $orderSort = !empty($paramsTMP['orderSort']) ? $paramsTMP['orderSort'] : 'ASC';
        $pageSize = !empty($paramsTMP['pageSize']) ? $paramsTMP['pageSize'] : 25;

        $between = array();

        if (isset($paramsTMP['numero'])) {
            if (!empty(trim($paramsTMP['numero']))) {
                $param['guia.serienumero'] = trim($paramsTMP['numero']);
            }
        }

        if (isset($paramsTMP['desde']) && isset($paramsTMP['hasta'])) {
            if (!empty($paramsTMP['desde']) && !empty($paramsTMP['hasta'])) {
                $paramsTMP['desde'] = $this->formatFecha($paramsTMP['desde'], 'yyyy-mm-dd');
                $paramsTMP['hasta'] = $this->formatFecha($paramsTMP['hasta'], 'yyyy-mm-dd');
                $between = [$paramsTMP['desde'], $paramsTMP['hasta']];
            }
        }

        $data = $guia->grid($param, $between, $pageSize, $orderName, $orderSort);

        if ($data) {
            return $this->crearRespuesta($data->items(), 200, $data->total());
        }

        return $this->crearRespuestaError('Guia no encontrado', 404);
    }

    public function show($enterprise, $id) {

        $empresa = new empresa();
        $entidad = new entidad();

        $guia = guia::find($id);
        $idempresa = $empresa->idempresa($enterprise);

        $param = array(
            'entidad.idempresa' => $idempresa,
            'tipopersonal' => '1'
        );

        if ($guia) {

            $guia->fechadocumento = $this->formatFecha($guia->fechadocumento);
            $guiadet = $guia->guiadet(['idguia' => $id]);
            $guiadetdet = $guia->guiadetdet(['idguia' => $id]);

            foreach ($guiadet as $i => $row1) {
                $items = [];
                foreach ($guiadetdet as $row2) {
                    if ($row1->idguiadet === $row2->idguiadet) {
                        $vencimiento = ($row2->vencimiento === '0000-00-00') ? NULL : $row2->vencimiento;
                        $items[] = array('vencimiento' => $vencimiento, 'cantidad' => $row2->cantidad);
                    }
                }
                $guiadet[$i]->subitems = $items;
            }

            $listcombox = array(
                'personal' => $entidad->entidades($param),
                'estadodocumentos' => $empresa->estadodocumentos(),
                'guiadet' => $guiadet
            );

            return $this->crearRespuesta($guia, 200, '', '', $listcombox);
        }

        return $this->crearRespuestaError('Guía no encotrado', 404);
    }

    public function newguia(Request $request, $enterprise) {

        $empresa = new empresa();
        $entidad = new entidad();

        $paramsTMP = $request->all();
        $idempresa = $empresa->idempresa($enterprise);

        $param = array(
            'entidad.idempresa' => $idempresa,
            'tipopersonal' => '1'
        );

        $paramSerie = array(
            'idempresa' => $idempresa,
            'serie' => '001',
            'iddocumentofiscal' => ($paramsTMP['movimiento'] === 'I') ? 1 : 2 //1: Guia de Entrada 2:Guia de Salida
        );
        $listcombox = array(
            'serienumero' => $empresa->serienumero($paramSerie),
            'personal' => $entidad->entidades($param),
            'estadodocumentos' => $empresa->estadodocumentos(),
            'fechaactual' => date('d/m/Y')
        );

        return $this->crearRespuesta([], 200, '', '', $listcombox);
    }

    public function store(Request $request, $enterprise) {

        $empresa = new empresa();

        $idempresa = $empresa->idempresa($enterprise);
        $request = $request->all();

        $paramSerie = array(
            'idempresa' => $idempresa,
            'serie' => '001',
            'iddocumentofiscal' => ($request['guia']['movimiento'] === 'I') ? 1 : 2 //1: Guia de Entrada 2:Guia de Salida
        );

        //Suma acumulativa de las cantidades agrupando en FechaVencimiento.
        foreach ($request['guiadet'] as $pk => $row1) {
            $tmp = [];
            foreach ($row1['subitems'] as $fila) {
                $ind = empty($fila['vencimiento']) ? 'Vacio' : $fila['vencimiento'];

                $tmp[$ind]['vencimiento'] = $fila['vencimiento'];
                if (isset($tmp[$ind]['cantidad'])) {
                    $tmp[$ind]['cantidad'] = $tmp[$ind]['cantidad'] + $fila['cantidad'];
                } else {
                    $tmp[$ind]['cantidad'] = $fila['cantidad'];
                }
            }
            $tmpcantidad = [];
            foreach ($tmp as $fila) {
                $tmpcantidad[] = $fila;
            }
            $request['guiadet'][$pk]['subitems'] = $tmpcantidad;
        }

        //1RA VALIDACION            
        //Para estado 2: 'Atendido' y movimiento S: 'Salida' de almacen 
        if ($request['guia']['idestadodocumento'] === 2 and $request['guia']['movimiento'] === 'S') {
            $validation = $this->validarStockenAlmacen($request['guiadet'], $idempresa);

            if ($validation['inValid']) {
                return $this->crearRespuesta($validation['message'], [200, 'info'], '', '', $request['guiadet']);
            }
        }
        //FIN VALIDACIONES

        \DB::beginTransaction();
        try {

            //Graba en 4 tablas(guia, guiadet, guiadetdet y serie)
            $request['guia']['idempresa'] = $idempresa;
            $request['guia']['fecharegistro'] = date('Y-m-d H:i:s')  ;
            $request['guia']['fechadocumento'] = $this->formatFecha($request['guia']['fechadocumento'], 'yyyy-mm-dd');
            $request['guia']['ejercicio'] = date('Y');
            $request['guia']['serie'] = '001';

            $guia = guia::create($request['guia']);
            $empresa->updateSerieNumero($paramSerie);

            foreach ($request['guiadet'] as $row1) {
                $row1['idguia'] = $guia->idguia;

                $subitems = $row1['subitems'];
                unset($row1['subitems']);
                $idguiadet = $guia->insertGuiadet($row1);

                if (!empty($subitems)) {
                    foreach ($subitems as $indice => $fila) {
                        $subitems[$indice]['vencimiento'] = empty($fila['vencimiento'])?NULL:$fila['vencimiento'];
                        $subitems[$indice]['idguiadet'] = $idguiadet;
                    }
                    $guia->insertGuiadetdet($subitems);
                }
            }

            //2: Guia atendido, entonces graba en tablas(almacen, almacendet, almacendetdet).
            //1: Guia por atender, entonces no graba porque aun no a salido de almacen.
            //3: Guia anulado, no es valido cuando el usuario registra una Guia de Entrada.
            if ($request['guia']['idestadodocumento'] === 2) {

                $request['guia']['idguia'] = $guia->idguia;
                $almacen = almacen::create($request['guia']);

                foreach ($request['guiadet'] as $row1) {
                    $row1['idalmacen'] = $almacen->idalmacen;
                    $row1['movimiento'] = $request['guia']['movimiento'];

                    $subitems = $row1['subitems'];
                    unset($row1['subitems']);
                    $idalmacendet = $almacen->insertAlmacendet($row1);

                    if (!empty($subitems)) {
                        foreach ($subitems as $indice => $fila) {
                            $subitems[$indice]['vencimiento'] = empty($fila['vencimiento'])?NULL:$fila['vencimiento'];
                            $subitems[$indice]['idalmacendet'] = $idalmacendet;
                            $subitems[$indice]['idproducto'] = $row1['idproducto'];
                            $subitems[$indice]['movimiento'] = $request['guia']['movimiento'];
                        }
                        $almacen->insertAlmacendetdet($subitems);
                    }
                }
            }
        } catch (QueryException $e) {
            \DB::rollback();
        }
        \DB::commit();

        $guiatext = $guia->movimiento === 'I' ? 'entrada' : 'salida';
        return $this->crearRespuesta('Guía de ' . $guiatext . ' N° "' . $guia->serienumero . '" ha sido creado. ', 200);
    }

    public function update(Request $request, $enterprise, $id) {

        $almacen = new almacen();
        $guia = guia::find($id);

        if ($guia) {
            $request = $request->all();
            $request['guia']['fechadocumento'] = $this->formatFecha($request['guia']['fechadocumento'], 'yyyy-mm-dd');
            $guia->fill($request['guia']);

            $guiadet = $guia->guiadet(['idguia' => $id]);

            $guiadetInsert = []; //guiadet
            $guiadetUpdate = []; //guiadet
            $guiadetDelete = []; //guiadet            
            $guiadetdetInsert = []; //guiadetdet
            $guiadetdetDelete = []; //guiadetdet
            //Extraccion de campo 'idguiadet' de tabla 'guiadet', para eliminar data en tabla 'guiadetdet'
            foreach ($guiadet as $indice => $row1) {
                $guiadetdetDelete[] = $row1->idguiadet;
            }

            //Suma acumulativa de las cantidades agrupando en FechaVencimiento.
            //Ejemplo: Input 2015-06-04 - 13 | 2015-06-04 - 3  Output 2015-06-04 - 16
            //Ejemplo: Input            -  1 |            - 1  Output            -  2
            foreach ($request['guiadet'] as $pk => $row1) {
                $tmp = [];
                foreach ($row1['subitems'] as $fila) {
                    $ind = empty($fila['vencimiento']) ? 'Vacio' : $fila['vencimiento'];

                    $tmp[$ind]['vencimiento'] = $fila['vencimiento'];
                    if (isset($tmp[$ind]['cantidad'])) {
                        $tmp[$ind]['cantidad'] = $tmp[$ind]['cantidad'] + $fila['cantidad'];
                    } else {
                        $tmp[$ind]['cantidad'] = $fila['cantidad'];
                    }
                }
                $tmpcantidad = [];
                foreach ($tmp as $fila) {
                    $tmpcantidad[] = $fila;
                }
                $request['guiadet'][$pk]['subitems'] = $tmpcantidad;
            }// return $this->crearRespuesta($request['guiadet'], [200, 'info']);
            //
            foreach ($request['guiadet'] as $row1) {
                $nuevo = true;
                $update = false;
                $nada = false; // Para insertar en tabla 'guiadedet' 
                foreach ($guiadet as $indice => $row2) {
                    if ($row1['idproducto'] === $row2->idproducto) {
                        $nada = true;
                        $nuevo = false;
                        if ((int) $row1['cantidad'] !== (int) $row2->cantidad) {
                            //$nada = false;
                            $update = true;
                        }
                        unset($guiadet[$indice]);
                        break 1;
                    }
                }

                //Cuando es item nuevo, la insercion en tabla 'guiadetdet' dependerá del campo 'idguiadet' pk generado
                //Inserta en (guiadet, guiadetdet) 
                if ($nuevo) {
                    $row = array(
                        'idguia' => $id,
                        'idproducto' => $row1['idproducto'],
                        'cantidad' => $row1['cantidad'],
                        'subitems' => $row1['subitems']
                    );
                    $guiadetInsert[] = $row;
                }

                //Cuando item ya existe. Y la cantidad ingresada por el usuario es diferente a cantidad en tabla 'guiadet'
                //Actualiza solo a (guiadet)
                if ($update) {
                    $row = array(
                        'data' => array(
                            'cantidad' => $row1['cantidad']
                        ),
                        'where' => array(
                            'idguiadet' => $row2->idguiadet
                        )
                    );
                    $guiadetUpdate[] = $row;
                }

                //Cuando item ya existe. Indiferente a cantidad.
                //Inserta solo a (guiadetdet)
                if ($nada) {
                    foreach ($row1['subitems'] as $fila) {
                        $fila['idguiadet'] = $row2->idguiadet;
                        $fila['vencimiento'] = empty($fila['vencimiento'])?NULL:$fila['vencimiento'];
                        $guiadetdetInsert[] = $fila;
                    }
                }
            }

            //Recorrido y aplicado unset en tabla 'guiadet', si existe items, entonces deben ser eliminados.
            //Elimina solo en (guiadet) 
            if (!empty($guiadet)) {
                foreach ($guiadet as $row1) {
                    $guiadetDelete[] = $row1->idguiadet;
                }
            }

            \DB::beginTransaction();
            try {
                $almacen->deleteAlmacen($id);

                //1RA VALIDACION            
                //Para estado 2: 'Atendido' y movimiento S: 'Salida' de almacen 
                if ($request['guia']['idestadodocumento'] === 2 and $request['guia']['movimiento'] === 'S') {
                    $validation = $this->validarStockenAlmacen($request['guiadet'], $guia->idempresa);
                    if ($validation['inValid']) {
                        return $this->crearRespuesta($validation['message'], [200, 'info']);
                    }
                }
                //FIN VALIDACIONES
                //Graba en 4 tablas(guia, guiadet, guiadetdet y serie)

                if (!empty($guiadetdetDelete)) {
                    $guia->deleteGuiadetdet($guiadetdetDelete);
                }

                if (!empty($guiadetDelete)) {
                    $guia->deleteGuiadet($guiadetDelete);
                }

                if (!empty($guiadetInsert)) {
                    foreach ($guiadetInsert as $fila) {
                        $subitems = $fila['subitems'];

                        unset($fila['subitems']);
                        $idguiadet = $guia->insertGuiadet($fila);

                        if (!empty($subitems)) {
                            foreach ($subitems as $indice => $fila) {
                                $subitems[$indice]['idguiadet'] = $idguiadet;
                            }
                            $guia->insertGuiadetdet($subitems);
                        }
                    }
                }

                if (!empty($guiadetdetInsert)) {
                    $guia->insertGuiadetdet($guiadetdetInsert);
                }

                if (!empty($guiadetUpdate)) {
                    foreach ($guiadetUpdate as $fila) {
                        $guia->updateGuiadet($fila['data'], $fila['where']);
                    }
                }
                $guia->save();

                ///////////////////////////////////////////////////////////  

                if ($request['guia']['idestadodocumento'] === 2) {
                    $request['guia']['idguia'] = $guia->idguia;
                    $request['guia']['idempresa'] = $guia->idempresa;
                    $almacen = almacen::create($request['guia']);

                    foreach ($request['guiadet'] as $row1) {
                        $row1['idalmacen'] = $almacen->idalmacen;
                        $row1['movimiento'] = $request['guia']['movimiento'];

                        $subitems = $row1['subitems'];
                        unset($row1['subitems']);
                        $idalmacendet = $almacen->insertAlmacendet($row1);

                        if (!empty($subitems)) {
                            foreach ($subitems as $indice => $fila) {
                                $subitems[$indice]['idalmacendet'] = $idalmacendet;
                                $subitems[$indice]['idproducto'] = $row1['idproducto'];
                                $subitems[$indice]['movimiento'] = $request['guia']['movimiento'];
                                $subitems[$indice]['vencimiento'] = empty($fila['vencimiento'])?NULL:$fila['vencimiento'];
                            }
                            $almacen->insertAlmacendetdet($subitems);
                        }
                    }
                }
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            $guiatext = $guia->movimiento === 'I' ? 'entrada' : 'salida';
            return $this->crearRespuesta('Guía de ' . $guiatext . ' N° "' . $guia->serienumero . '" ha sido editado. ', 200, '', '', ['I' => $guiadetInsert, 'U' => $guiadetUpdate, 'D' => $guiadetDelete]);
        }

        return $this->crearRespuestaError('El id especificado no corresponde a un producto', 404);
    }

    private function validarStockenAlmacen($data, $idempresa) {

        $validation = array('inValid' => false, 'message' => '');

        $producto = new producto();
        $whereIn = [];

        foreach ($data as $row1) {
            $whereIn[] = $row1['idproducto'];

            //Verificar existencia de Stock en tabla 'Almacendetdet' para SubItems
            if (!empty($row1['subitems'])) {
                $param = array(
                    'producto.idempresa' => $idempresa,
                    'producto.idproducto' => $row1['idproducto']
                );

                $stock = $producto->productostockdet($param);

                foreach ($row1['subitems'] as $fila) {
                    $noExiste = true;
                    $fila['vencimiento'] = empty($fila['vencimiento']) ? '' : (substr($fila['vencimiento'], 0, 10));

                    foreach ($stock as $fila2) {
                        if ($fila['vencimiento'] === $fila2['vencimiento']) {
                            $noExiste = false;
                            if ($fila['cantidad'] > $fila2['stock']) {
                                $producto = producto::find($row1['idproducto']);

                                $validation['inValid'] = true;
                                $validation['message'] = 'Cantidad: ' . $fila['cantidad'] . ' mayor que el stock actual: ' . $fila2['stock'] . ' para producto "' . $producto->nombre . '". Revisar detalle de items.';
                                break 3;
                            }
                        }
                    }
                    if ($noExiste) {
                        //Puede darse cuando el producto fue eliminado de tabla 'almacendetdet' segundos antes. 
                        $producto = producto::find($row1['idproducto']);

                        $validation['inValid'] = true;
                        $validation['message'] = 'Producto "' . $producto->nombre . '" ya no existee en almac&eacute;n!.' . $fila['vencimiento'];
                        break 2;
                    }
                }
            }
        }

        //Verificar existencia de Stock en tabla 'Almacendet'    
        if (!$validation['inValid']) {
            $stock = $producto->productostock($whereIn, $idempresa);
            foreach ($data as $row1) {
                $idproducto = $row1['idproducto'];
                if (isset($stock[$idproducto])) {
                    if ($row1['cantidad'] > $stock[$idproducto]['stock']) {
                        $producto = producto::find($idproducto);

                        $validation['inValid'] = true;
                        $validation['message'] = 'Cantidad: ' . $row1['cantidad'] . ' mayor que el stock actual: ' . $stock[$idproducto]['stock'] . ' para producto "' . $producto->nombre . '"';
                        break 1;
                    }
                } else {
                    //Puede darse cuando el producto fue eliminado de tabla 'almacendet' segundos antes. 
                    $producto = producto::find($idproducto);

                    $validation['inValid'] = true;
                    $validation['message'] = 'Producto "' . $producto->nombre . '" ya no existe en almac&eacute;n!.';
                    break 1;
                }
            }
        }

        return $validation;
    }

    public function destroy($enterprise, $id) {

        $almacen = new almacen();
        $guia = guia::find($id);

        if ($guia) {
            //VALIDACIONES con tablas relacionadas
            /* Ej. almacendetalle o almacen movimiento 
             * */

            $guiadet = $guia->guiadet(['idguia' => $id]);
            $idguiadet = [];
            foreach ($guiadet as $row) {
                $idguiadet[] = $row->idguiadet;
            }

            \DB::beginTransaction();
            try {
                //Elimina en 1 tablas(producto)  
                if (!empty($idguiadet)) {
                    $guia->deleteGuiadetdet($idguiadet);
                }
                $guia->deleteGuiadet($id);
                $guia->delete();

                $almacen->deleteAlmacen($id);
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            $guiatext = $guia->movimiento === 'I' ? 'entrada' : 'salida';
            return $this->crearRespuesta('Guía de ' . $guiatext . ' N° "' . $guia->serienumero . '" a sido eliminado.', 200);
        }
        return $this->crearRespuestaError('producto no encotrado', 404);
    }

}
