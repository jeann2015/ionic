<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\empresa;
use App\Models\producto;
use App\Models\arbol;
use \Firebase\JWT\JWT;

class arbolController extends Controller {

    public function index(Request $request, $enterprise) {

        $paramsTMP = $request->all();

        $empresa = new empresa();
        $arbol = new arbol();

        $paramsTMP['idempresa'] = $empresa->idempresa($enterprise);

        $data = $arbol->grid($paramsTMP);
        if ($data) {
            $data = $this->procesarRaiz($data, ['ID' => 'idarbol', 'PARENT' => 'parent', 'CHILDREN' => 'children']);           
            return $this->crearRespuesta($data, 200);
        }
        return $this->crearRespuestaError('Categor&iacute;a no encontrado', 404);
    }

    public function store(Request $request, $enterprise) {

        $empresa = new empresa();

        $idempresa = $empresa->idempresa($enterprise);

        $request = $request->all();
        $request['idempresa'] = $idempresa;

        $arbol = arbol::create($request);

        return $this->crearRespuesta('"' . $arbol->nombre . '" ha sido creado.', 201);
    }

    public function update(Request $request, $enterprise, $id) {

        $empresa = new empresa();
        $producto = new producto();
        
        $arbol = arbol::find($id);

        if ($arbol) {
            $request = $request->all();

            \DB::beginTransaction();
            try {
                //Graba en 1 tablaa(arbol) y actualiza en 1 tabla producto                  
                if ($arbol->nombre !== $request['nombre']) {                    
                    $where = array(
                        'idempresa' => $empresa->idempresa($enterprise),
                        'idarbol' => $id
                    );
                    
                    $producto->updateProducto(array('categoria' => $request['nombre']), $where);
                }
                $arbol->fill($request);
                $arbol->save();
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta('"' . $arbol->nombre . '" ha sido .actualizado.', 201);
        }

        return $this->crearRespuestaError('El id especificado no corresponde a un categor&iacute;a', 404);
    }

    public function destroy(Request $request, $enterprise, $id) {

        $arbol = arbol::find($id);
        $params = $request->all();

        if ($arbol) {
            // VALIDACIONES             
            // 1ERA VALIDACION
            $params['idempresa'] = $arbol->idempresa;
            $id = (int) $id;
            $data = $this->procesarRaiz($arbol->grid($params), ['ID' => 'idarbol', 'PARENT' => 'parent', 'CHILDREN' => 'children'], $id, true);

            $whereIn = [];
            foreach ($data as $row) {
                $whereIn[] = $row['idarbol'];
            }

            $count = producto::whereIn('idarbol', $whereIn)->count();
            if ($count > 0) {
                return $this->crearRespuesta('"' . $arbol->nombre . '" no puede ser eliminado. Est&aacute; asignado a productos.', [200, 'info']);
            }
            //FIN 1ERA VALIDACION

            \DB::beginTransaction();
            try {
                $arbol->deleteNodo($whereIn);
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta('"' . $arbol->nombre . '" a sido eliminado.', 200);
        }

        return $this->crearRespuestaError('Categor&iacute;a no encotrado.', 404);
    }

}
