<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\empresa;
use App\Models\sede;
use App\Models\entidad;

class sedeController extends Controller {

    public function index(Request $request, $enterprise) {

        $empresa = new empresa();
        $sede = new sede();

        $idempresa = $empresa->idempresa($enterprise);

        $data = $sede->sedes($idempresa);

        if ($data) {
            return $this->crearRespuesta($data, 200);
        }

        return $this->crearRespuestaError('Sede no encontrada', 404);
    }

    public function store(Request $request, $enterprise) {

        $empresa = new empresa();
        $sede = new sede();

        $idempresa = $empresa->idempresa($enterprise);
        $request = $request->all();
        $request['idempresa'] = $idempresa;

        \DB::beginTransaction();
        try {
            if ($request['principal'] === '1') {
                $sede->updateSede(['principal' => '0'], ['idempresa' => $idempresa]);
            }
            $sede = sede::create($request);
        } catch (QueryException $e) {
            \DB::rollback();
        }
        \DB::commit();

        return $this->crearRespuesta('La sede "' . $sede->nombre . '" ha sido creado.', 201);
    }

    public function update(Request $request, $enterprise, $id) {

        $sede = sede::find($id);
        $request = $request->all();

        if ($sede) {
            \DB::beginTransaction();
            try {
                if ($request['principal'] === '1' && $sede->principal === '0') {
                    $sede->updateSede(['principal' => '0'], ['idempresa' => $sede->idempresa]);
                }
                $sede->fill($request);
                $sede->save();
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta('La sede "' . $sede->nombre . '" ha sido editado. ', 200);
        }

        return $this->crearRespuestaError('El id especificado no corresponde a una sede', 404);
    }

    public function destroy($enterprise, $id) {

        $sede = sede::find($id);

        if ($sede) {

            //VALIDACIONES            
            if ($sede->principal === '1') {
                return $this->crearRespuesta('La sede "' . $sede->nombre . '" no puede ser eliminado. Es sede principal.', [200, 'info']);
            }

            $entidad = new entidad();
            $data = $entidad->listaEntidadSede(['sede.idsede' => $id]);
            if (!empty($data)) {
                return $this->crearRespuesta('La sede "' . $sede->nombre . '" no puede ser eliminado. Esta asignado a usuarios.', [200, 'info']);
            }

            $sede->delete();
            return $this->crearRespuesta('La sede "' . $sede->nombre . '" a sido eliminado', 200);
        }

        return $this->crearRespuestaError('Sede no encontrado', 404);
    }

}
