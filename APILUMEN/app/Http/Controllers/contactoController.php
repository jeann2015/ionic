<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\contacto;

class contactoController extends Controller {

    public function index(Request $request, $enterprise) {

        $contacto = new contacto();
        $request = $request->all();

        $data = $contacto->contactos($request['identidad']);

        return $this->crearRespuesta($data, 200);
    }

    public function store(Request $request, $enterprise) {

        $contacto = contacto::create($request->all());

        return $this->crearRespuesta('El contacto "' . $contacto->contacto . '" ha sido creado.', 201);
    }

    public function update(Request $request, $enterprise, $id) {

        $contacto = contacto::find($id);

        if ($contacto) {
            $contacto->fill($request->all());
            $contacto->save();

            return $this->crearRespuesta('El contacto "' . $contacto->contacto . '" ha sido editado. ', 200);
        }
        return $this->crearRespuestaError('El id especificado no corresponde a un contacto', 404);
    }

    public function destroy($enterprise, $id) {

        $contacto = contacto::find($id);

        if ($contacto) {

            $contacto->delete();
            return $this->crearRespuesta('El contacto "' . $contacto->contacto . '" a sido eliminado', 200);
        }
        return $this->crearRespuestaError('Contacto no encontrado', 404);
    }

}
