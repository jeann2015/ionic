<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\empresa;
use App\Models\sede;
use App\Models\entidad;
use App\Models\contacto;
use App\Models\especialidad;
use App\Models\cargoorg;
use \Firebase\JWT\JWT;

class entidadController extends Controller {

    public function __construct(Request $request) {
        $this->getToken($request);
    }

    private function formatFecha($fecha, $format = 'dd/mm/yyyy') {
        $newFecha = NULL;
        if (!empty($fecha) && strlen($fecha) == 10) {
            if ($format === 'dd/mm/yyyy') {
                //de: yyyy-mm-dd a: dd/mm/yyyy 
                $fecha = explode('-', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
            if ($format === 'yyyy-mm-dd') {
                //de: dd/mm/yyyy a: yyyy-mm-dd 
                $fecha = explode('/', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
        }
        return $newFecha;
    }

    public function index(Request $request, $enterprise) {

        $empresa = new empresa();
        $entidad = new entidad();
        $param = array();

        $paramsTMP = $request->all();
        $idempresa = $empresa->idempresa($enterprise);

        $param['entidad.idempresa'] = $idempresa;

        if (isset($paramsTMP['liketelefono']) && !empty($paramsTMP['liketelefono'])) {
            $param['entidad.telefono'] = $paramsTMP['liketelefono'];
        }
//        if (isset($paramsTMP['likehc']) && !empty($paramsTMP['likehc'])) {
//            
//        }

        if (isset($paramsTMP['tipoentidad'])) {
            switch ($paramsTMP['tipoentidad']) {
                case 'cliente':
                    $param['entidad.tipocliente'] = '1';
                    break;
                case 'personal':
                    $param['entidad.tipopersonal'] = '1';
                    break;
                case 'medico':
                    $param['entidad.tipomedico'] = '1';
                    break;
                case 'proveedor':
                    $param['entidad.tipoproveedor'] = '1';
                    break;
                default:
                    break;
            }
        }

        $orderName = !empty($paramsTMP['orderName']) ? $paramsTMP['orderName'] : 'entidad.entidad';
        $orderSort = !empty($paramsTMP['orderSort']) ? $paramsTMP['orderSort'] : 'ASC';
        $pageSize = !empty($paramsTMP['pageSize']) ? $paramsTMP['pageSize'] : 25;

        $like = !empty($paramsTMP['likeentidad']) ? trim($paramsTMP['likeentidad']) : '';
        $data = $entidad->grid($param, $like, $pageSize, $orderName, $orderSort);

        if ($data) {
            //return $this->crearRespuesta($data->items(), 200, $data->total(), $data->count() . '|' . $orderName . '|' . $orderSort, $paramsTMP);
            return $this->crearRespuesta($data->items(), 200, $data->total());
        }

        return $this->crearRespuestaError('Entidad no encontrada', 404);
    }

    public function cumpleanos(Request $request, $enterprise) {
        $empresa = new empresa();
        $entidad = new entidad();

        $param['entidad.idempresa'] = $empresa->idempresa($enterprise);
        $data = $entidad->getCumpleanos($param);

        return $this->crearRespuesta($data, 200);
    }

    public function showprofile($enterprise, $id) {

        $empresa = new empresa();
        $entidad = new entidad();
        $contacto = new contacto();

        $row = $entidad->entidad(['entidad.identidad' => $id]);

        if ($row) {
            $row->fechanacimiento = $this->formatFecha($row->fechanacimiento);

            $listcombox = array(
                'clientes' => $entidad->clientes(['identidad' => $id]),
                'medicos' => $entidad->medicos(['identidad' => $id]),
                //'proveedores' => $entidad->proveedores(['identidad' => $id]),
                'personales' => $entidad->personales(['identidad' => $id]),
                'entidadsede' => $entidad->listaEntidadSede(['entidadsede.identidad' => $id]),
                'entidadespecialidad' => $entidad->listaEntidadEspecialidad(['identidad' => $id]),
                'contactos' => $contacto->contactos($id),
                'timezonedatephp' => env('APP_TIMEZONE') . ' - ' . date("d/m/Y H:i:s")
            );

            $ubigeo = $row->idubigeo;
            $row->sexo = ($row->sexo == 'M') ? 'Masculino' : (($row->sexo == 'F') ? 'Femenino' : '');
            if (!empty($ubigeo)) {
                $pais = substr($ubigeo, 0, 2);
                $dpto = substr($ubigeo, 2, 3);
                $prov = substr($ubigeo, 5, 2);
                $dist = substr($ubigeo, 7, 2);
                $ubigeorow['paises'] = $empresa->paises(['pais' => $pais]);
                $ubigeorow['departamentos'] = $empresa->departamentos($pais, ['dpto' => $dpto]);
                $ubigeorow['provincias'] = $empresa->provincias($pais, $dpto, ['prov' => $prov]);
                $ubigeorow['distritos'] = $empresa->distritos($pais, $dpto, $prov, ['dist' => $dist]);
                $row->pais = empty($ubigeorow['paises']) ? '' : $ubigeorow['paises'][0]->nombre;
                $row->dpto = empty($ubigeorow['departamentos']) ? '' : $ubigeorow['departamentos'][0]->nombre;
                $row->prov = empty($ubigeorow['provincias']) ? '' : $ubigeorow['provincias'][0]->nombre;
                $row->dist = empty($ubigeorow['distritos']) ? '' : $ubigeorow['distritos'][0]->nombre;
            }

            return $this->crearRespuesta($row, 200, '', '', $listcombox);
        }

        return $this->crearRespuestaError('Entidad no encotrado', 404);
    }

    public function show($enterprise, $id) {

        $empresa = new empresa();
        $entidad = new entidad();
        $contacto = new contacto();

        $especialidad = new especialidad();

        $row = $entidad->entidad(['entidad.identidad' => $id]);

        if ($row) {
            $row->fechanacimiento = $this->formatFecha($row->fechanacimiento);
            $idempresa = $empresa->idempresa($enterprise);

            $listcombox = array(
                'documentos' => $empresa->documentos(),
                'perfiles' => $empresa->perfiles($idempresa),
                'clientes' => $entidad->clientes(['identidad' => $id]),
                'medicos' => $entidad->medicos(['identidad' => $id]),
                'proveedores' => $entidad->proveedores(['identidad' => $id]),
                'personales' => $entidad->personales(['identidad' => $id]),
                'sedes' => sede::select('idsede', 'nombre', 'direccion')->where('idempresa', '=', $idempresa)->get(),
                'entidadsede' => $entidad->listaEntidadSede(['entidadsede.identidad' => $id]),
                'cargosorg' => cargoorg::where('idempresa', '=', $idempresa)->get(),
                'especialidades' => $especialidad->especialidades(['especialidad.idempresa' => $idempresa]),
                'entidadespecialidad' => $entidad->listaEntidadEspecialidad(['identidad' => $id]),
                'contactos' => $contacto->contactos($id)
            );

            $ubigeo = $row->idubigeo;
            if (!empty($ubigeo)) {
                $pais = substr($ubigeo, 0, 2);
                $dpto = substr($ubigeo, 2, 3);
                $prov = substr($ubigeo, 5, 2);
                $dist = substr($ubigeo, 7, 2);
                $listcombox['paises'] = $empresa->paises();
                $listcombox['departamentos'] = $empresa->departamentos($pais);
                $listcombox['provincias'] = $empresa->provincias($pais, $dpto);
                $listcombox['distritos'] = $empresa->distritos($pais, $dpto, $prov);
                $row->pais = $pais;
                $row->dpto = $dpto;
                $row->prov = $prov;
                $row->dist = $dist;
            } else {
                $listcombox['paises'] = $empresa->paises();
            }
            return $this->crearRespuesta($row, 200, '', '', $listcombox);
        }

        return $this->crearRespuestaError('Entidad no encotrado', 404);
    }

    public function nrodocumento(Request $request, $enterprise) {

        $paramsTMP = $request->all();

        $row = array('existeEntidad' => false);

        $entidad = entidad::where('numerodoc', '=', $paramsTMP['numerodoc'])->first();

        if ($entidad) {
            switch ($paramsTMP['tipoentidad']) {
                case 'personal':
                    $tipo = 'tipopersonal';
                    break;
                case 'medico':
                    $tipo = 'tipomedico';
                    break;
                case 'cliente':
                    $tipo = 'tipocliente';
                    break;
                case 'proveedor':
                    $tipo = 'tipoproveedor';
                    break;
                default:
                    $tipo = '';
                    break;
            }

            $row['existeEntidad'] = true;
            $row['existeSubEntidad'] = false;
            $row['nombreEntidad'] = $entidad->entidad;
            $row['numeroDocEntidad'] = $entidad->numerodoc;
            $row['idEntidad'] = $entidad->identidad;
            if ($entidad[$tipo] === '1') {
                $row['existeSubEntidad'] = true;
            }
        }
        return $this->crearRespuesta($row, 201);
    }

    public function updatesubentidad(Request $request, $enterprise, $id) {

        $entidad = entidad::find($id);

        if ($entidad) {
            $paramsTMP = $request->all();

            switch ($paramsTMP['tipoentidad']) {
                case 'personal':
                    $tipo = 'tipopersonal';
                    break;
                case 'medico':
                    $tipo = 'tipomedico';
                    break;
                case 'cliente':
                    $tipo = 'tipocliente';
                    break;
                case 'proveedor':
                    $tipo = 'tipoproveedor';
                    break;
                default:
                    $tipo = '';
                    break;
            }
            $entidad->updateEntidad([$tipo => '1'], $id);

            return $this->crearRespuesta($entidad->entidad . ' ha sido creado.', 201);
        }

        return $this->crearRespuestaError('El id especificado no corresponde a una entidad', 404);
    }

    public function updatepassword(Request $request, $enterprise, $id) {
        $entidad = new entidad();

        $row = $entidad->entidad(['entidad.identidad' => $id]);
        $paramsTMP = $request->all();

        if ($row) {
            //if ($paramsTMP['contrasenaactual'] !== $row->password) {
            if (!Hash::check($paramsTMP['contrasenaactual'], $row->password)) {
                return $this->crearRespuesta('Contraseña actual no es correcta.', [200, 'info']);
            }

            $param = array('password' => Hash::make($paramsTMP['contrasenanueva']));
            $entidad->updateEntidad($param, $id);

            return $this->crearRespuesta('Contraseña a sido actualizado.', 201);
        }

        return $this->crearRespuestaError('El id especificado no corresponde a una entidad', 404);
    }

    public function newentidad(Request $request, $enterprise) {

        $empresa = new empresa();
        $especialidad = new especialidad();

        $paramsTMP = $request->all();
        $idempresa = $empresa->idempresa($enterprise);
        $listcombox = array(
            'documentos' => $empresa->documentos(),
            'paises' => $empresa->paises()
        );

        switch ($paramsTMP['tipoentidad']) {
            case 'proveedor':
                break;
            case 'cliente':
                break;
            case 'personal':
                $listcombox['perfiles'] = $empresa->perfiles($idempresa);
                $listcombox['sedes'] = sede::select('idsede', 'nombre', 'direccion')->where('idempresa', '=', $idempresa)->get();
                $listcombox['cargosorg'] = cargoorg::where('idempresa', '=', $idempresa)->get();
                break;
            case 'medico':
                $listcombox['perfiles'] = $empresa->perfiles($idempresa);
                $listcombox['sedes'] = sede::select('idsede', 'nombre', 'direccion')->where('idempresa', '=', $idempresa)->get();
                $listcombox['cargosorg'] = cargoorg::where('idempresa', '=', $idempresa)->get();
                $listcombox['especialidades'] = $especialidad->especialidades(['especialidad.idempresa' => $idempresa]);
                break;
            default:
                break;
        }

        return $this->crearRespuesta([], 200, '', '', $listcombox);
    }

    public function store(Request $request, $enterprise) {

        $empresa = new empresa();
        $idempresa = $empresa->idempresa($enterprise);
        $request = $request->all();

        if (isset($request['entidad']['fechanacimiento'])) {
            $request['entidad']['fechanacimiento'] = $this->formatFecha($request['entidad']['fechanacimiento'], 'yyyy-mm-dd');
        }

        if ($request['entidad']['iddocumento'] == 1 || $request['entidad']['iddocumento'] == 3) { //DNI
            $apemat = isset($request['entidad']['apellidomat']) ? (' ' . $request['entidad']['apellidomat']) : '';
            $request['entidad']['entidad'] = $request['entidad']['apellidopat'] . $apemat . ', ' . $request['entidad']['nombre'];
        }

        if ($request['entidad']['iddocumento'] == 2) { //RUC
            $request['entidad']['entidad'] = $request['entidad']['razonsocial'];
        }

        $request['entidad']['idubigeo'] = NULL;
        if (!empty($request['entidad']['pais'])) {
            $dpto = empty($request['entidad']['dpto']) ? '00' : $request['entidad']['dpto'];
            $prov = empty($request['entidad']['prov']) ? '00' : $request['entidad']['prov'];
            $dist = empty($request['entidad']['dist']) ? '00' : $request['entidad']['dist'];
            $request['entidad']['idubigeo'] = $request['entidad']['pais'] . $dpto . $prov . $dist;
        }

        $request['entidad']['idempresa'] = $idempresa;

        //VALIDACIONES 
        $numerodoc = $request['entidad']['numerodoc'];
        $count = entidad::select('numerodoc')->where('numerodoc', '=', $numerodoc)->count();
        if ($count > 0) {
            return $this->crearRespuesta('No puede registrarse, el n&uacute;mero de documento "' . $numerodoc . '" ya existe.', [200, 'info']);
        }

        \DB::beginTransaction();
        try {
            //Graba en 7 tablas(entidad, entidadperfil, SUBENTIDADES, entidadespecialidad, entidadsede)
            switch ($request['tipoentidad']) {
                case 'proveedor':
                    $request['entidad']['tipoproveedor'] = '1';
                    break;
                case 'cliente':
                    $request['entidad']['tipocliente'] = '1';
                    break;
                case 'medico':
                    $request['entidad']['tipomedico'] = '1';
                    $request['entidad']['tipopersonal'] = '1';
                    break;
                case 'personal':
                    $request['entidad']['tipopersonal'] = '1';
                    break;
            }
            $entidad = entidad::create($request['entidad']);
            $id = $entidad->identidad;

            $dataEntEsp = [];
            foreach ($request['entidadespecialidad'] as $row) {
                $dataEntEsp[] = ['identidad' => $id, 'idespecialidad' => $row['idespecialidad']];
            }

            $dataEntSed = [];
            foreach ($request['entidadsede'] as $row) {
                $dataEntSed[] = ['identidad' => $id, 'idsede' => $row['idsede']];
            }

            $dataEntPer = [];
            if (!empty($request['entidad']['idperfil'])) {
                $dataEntPer = array('identidad' => $id, 'idperfil' => $request['entidad']['idperfil']);
            }

            switch ($request['tipoentidad']) {
                case 'proveedor':
                    $request['proveedor']['identidad'] = $id;
                    break;
                case 'cliente':
                    $request['cliente']['identidad'] = $id;
                    break;
                case 'personal':
                    $request['personal']['identidad'] = $id;
                    $entidad->GrabarEntidadSede($dataEntSed, $id);
                    $entidad->GrabarEntidadPerfil($dataEntPer, $id);
                    $entidad->updateEntidad(['password' => Hash::make($request['entidad']['numerodoc'])], $id);
                    
                    break;
                case 'medico':
                    $request['personal']['identidad'] = $id;
                    $request['medico']['identidad'] = $id;
                    $entidad->GrabarEntidadSede($dataEntSed, $id);
                    $entidad->GrabarEntidadEspecialidad($dataEntEsp, $id);
                    $entidad->GrabarEntidadPerfil($dataEntPer, $id);
                    $entidad->updateEntidad(['password' => Hash::make($request['entidad']['numerodoc'])], $id);
                    break;
                default:
                    break;
            }

            $entidad->GrabarSubEntidades($request);
        } catch (QueryException $e) {
            \DB::rollback();
        }
        \DB::commit();

        return $this->crearRespuesta($entidad->entidad . ' ha sido creado.', 201);
    }

    public function update(Request $request, $enterprise, $id) {

        $entidad = entidad::find($id);

        if ($entidad) {
            $request = $request->all();
            $request['entidad']['fechanacimiento'] = $this->formatFecha($request['entidad']['fechanacimiento'], 'yyyy-mm-dd');
            if ($request['entidad']['iddocumento'] == 1) { //DNI                   
                $apemat = isset($request['entidad']['apellidomat']) ? (' ' . $request['entidad']['apellidomat']) : '';
                $request['entidad']['entidad'] = $request['entidad']['apellidopat'] . $apemat . ', ' . $request['entidad']['nombre'];
            }
            if ($request['entidad']['iddocumento'] == 2) { //RUC
                $request['entidad']['entidad'] = $request['entidad']['razonsocial'];
            }

            $request['entidad']['idubigeo'] = NULL;
            if (!empty($request['entidad']['pais'])) {
                $dpto = empty($request['entidad']['dpto']) ? '00' : $request['entidad']['dpto'];
                $prov = empty($request['entidad']['prov']) ? '00' : $request['entidad']['prov'];
                $dist = empty($request['entidad']['dist']) ? '00' : $request['entidad']['dist'];
                $request['entidad']['idubigeo'] = $request['entidad']['pais'] . $dpto . $prov . $dist;
            }

            $dataEntEsp = [];
            foreach ($request['entidadespecialidad'] as $row) {
                $dataEntEsp[] = ['identidad' => $id, 'idespecialidad' => $row['idespecialidad']];
            }

            $dataEntSed = [];
            foreach ($request['entidadsede'] as $row) {
                $dataEntSed[] = ['identidad' => $id, 'idsede' => $row['idsede']];
            }

            $dataEntPer = [];
            if (!empty($request['entidad']['idperfil'])) {
                $dataEntPer = array('identidad' => $id, 'idperfil' => $request['entidad']['idperfil']);
            }

            $numerodoc = $request['entidad']['numerodoc'];
            $consultado = false;
            if ($numerodoc !== $entidad->numerodoc) {
                $consultado = true;
                $count = entidad::select('numerodoc')->where('numerodoc', '=', $numerodoc)->count();
                if ($count > 0) {
                    return $this->crearRespuesta('No puede registrarse, el n&uacute;mero de documento "' . $numerodoc . '" ya existe.', [200, 'info']);
                }
            }

            $entidad->fill($request['entidad']);

            \DB::beginTransaction();
            try {
                //Graba en 7 tablas(entidad, entidadperfil, SUBENTIDADES, entidadespecialidad, entidadsede)                                                              
                switch ($request['tipoentidad']) {
                    case 'proveedor':
                        if (!isset($request['proveedor']['identidad'])) {
                            $request['proveedor']['identidad'] = $id;
                        }
                        break;
                    case 'cliente':
                        if (!isset($request['cliente']['identidad'])) {
                            $request['cliente']['identidad'] = $id;
                        }
                        break;
                    case 'personal':
                        if (!isset($request['personal']['identidad'])) {
                            $request['personal']['identidad'] = $id;
                        }
                        $entidad->GrabarEntidadSede($dataEntSed, $id);
                        $entidad->GrabarEntidadPerfil($dataEntPer, $id);
                        break;
                    case 'medico':
                        if (!isset($request['personal']['identidad'])) {
                            $request['personal']['identidad'] = $id;
                        }
                        if (!isset($request['medico']['identidad'])) {
                            $request['medico']['identidad'] = $id;
                        }
                        $entidad->GrabarEntidadEspecialidad($dataEntEsp, $id);
                        $entidad->GrabarEntidadSede($dataEntSed, $id);
                        $entidad->GrabarEntidadPerfil($dataEntPer, $id);
                        break;
                    default:
                        break;
                }

                if (empty($entidad->password)) {
                    $entidad->updateEntidad(['password' => Hash::make($request['entidad']['numerodoc'])], $id);
                }

                $entidad->save();
                $entidad->GrabarSubEntidades($request);
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta($entidad->entidad . ' ha sido editado. ', 200);
        }

        return $this->crearRespuestaError('El id especificado no corresponde a una entidad', 404);
    }

    public function destroy(Request $request, $enterprise, $id) {

        $entidad = entidad::find($id);

        if ($entidad) {
            $request = $request->all();

            //VALIDACIONES con tablas relacionadas
            if ($request['tipoentidad'] === 'personal') {
                $responsable = sede::select('idsede')->where('identidad', '=', $id)->count();
                if ($responsable > 0) {
                    return $this->crearRespuesta('"' . $entidad->entidad . '" no puede ser eliminado. Es responsable de una sede.', [200, 'info']);
                }
            }

            $cont = 0;
            if (!empty($entidad->tipopersonal)) {
                $cont++;
            }
            if (!empty($entidad->tipomedico)) {
                $cont++;
            }
            if (!empty($entidad->tipocliente)) {
                $cont++;
            }
            if (!empty($entidad->tipoproveedor)) {
                $cont++;
            }

            \DB::beginTransaction();
            try {
                if ($cont === 1) {
                    //Elimina en 7 tablas(entidad, entidadperfil, SUBENTIDADES, entidadespecialidad, entidadsede)                
                    $entidad->GrabarEntidadPerfil([], $id);
                    $entidad->EliminarSubEntidades($id);
                    $entidad->GrabarEntidadEspecialidad([], $id);
                    $entidad->GrabarEntidadSede([], $id);
                    $entidad->delete();
                } else {
                    //Elimina segun el tipo de entidad
                    switch ($request['tipoentidad']) {
                        case 'personal':
                            if (empty($entidad->tipomedico)) {
                                $entidad->GrabarEntidadPerfil([], $id);
                                $entidad->GrabarEntidadSede([], $id);
                            }
                            $tipo = 'tipopersonal';
                            break;
                        case 'medico':
                            if (empty($entidad->tipopersonal)) {
                                $entidad->GrabarEntidadPerfil([], $id);
                                $entidad->GrabarEntidadSede([], $id);
                            }
                            $entidad->GrabarEntidadEspecialidad([], $id);
                            $tipo = 'tipomedico';
                            break;
                        case 'cliente':
                            $tipo = 'tipocliente';
                            break;
                        case 'proveedor':
                            $tipo = 'tipoproveedor';
                            break;
                        default:
                            $tipo = '';
                            break;
                    }
                    $entidad->EliminarSubEntidades($id, false, $request['tipoentidad']);
                    $entidad->updateEntidad([$tipo => NULL], $id);
                }
            } catch (QueryException $e) {
                \DB::rollback();
            }
            \DB::commit();

            return $this->crearRespuesta('"' . $entidad->entidad . '" a sido eliminado', 200);
        }
        return $this->crearRespuestaError('entidad no encotrado', 404);
    }

    public function modulos(Request $request, $enterprise) {

        $empresa = new empresa();
        $entidad = new entidad();

        $params = array(
            'entidad.numerodoc' => $this->objTtoken->myusername,
            'empresa.url' => $enterprise
        );

        $data['empresa'] = $empresa->empresa(['url' => $enterprise]);
        $data['userProfiles'] = $entidad->ListaPerfiles($params);
        $data['userModules'] = $entidad->ListaModules($params);

        return $this->crearRespuesta($data, 200);
    }

    public function authenticate(Request $request) {
        /* Autor: chaucachavez@gmail.com
         * Descripcion: Validacion de autenticacion
         * Nota: En servidor de despliegue descomentar Hash para la encriptacion de datos.
         * En servidor de desarrollo no es conveniente encriptar.
         */
        $key = "x1TLVtPhZxN64JQB3fN8cHSp69999999";
        $entidad = new entidad();

        $user = $entidad->entidad(['numerodoc' => $request->get('username')]);

        $respuesta = [
            'success' => true,
            'message' => 'Bienvenido',
            'userModules' => [],
            'userProfiles' => [],
            'userEnterprises' => []
        ];

        if ($user) {                        
            if(Hash::check($request->get('password'), $user->password)) {
            //if ($request->get('password') == $user->password) {
                if ($user->acceso === 1) {
                    if (!empty($user->idperfil)) {
                        if (!empty(trim($request->get('enterprise')))) {
                        
                            $params = array(
                                'entidad.numerodoc' => $request->get('username'),
                                'empresa.url' => $request->get('enterprise')
                            );

                            $token = array(
                                "iss" => "http://wwww.lagranescuela.com",
                                "my" => $user->identidad,
                                "myusername" => $request->get('username'),
                                "myenterprise" => $request->get('enterprise')
                            );

                            $jwt = JWT::encode($token, $key);

                            $respuesta['token'] = $jwt;
                            $respuesta['userModules'] = $entidad->ListaModules($params);
                            $respuesta['userProfiles'] = $entidad->ListaPerfiles($params);
                            $respuesta['userEnterprises'] = $entidad->ListaEmpresas(array('entidad.numerodoc' => $request->get('username')));
                        } else {
                            $respuesta = ['success' => false, 'message' => 'No tiene definido URL de empresa por GET'];
                        }
                    } else {
                        $respuesta = ['success' => false, 'message' => 'Su cuenta no tiene asociado un perfil de sistema. Comuníquese con el administrador de sistema.'];
                    }
                } else {
                    $respuesta = ['success' => false, 'message' => 'Su cuenta no tiene permitido el acceso. Comuníquese con el administrador de sistema.'];
                }
            } else {
                $respuesta = ['success' => false, 'message' => 'Contraseña incorrecta'/* , 'params' => $request->all() */];
            }
        } else {
            $respuesta = ['success' => false, 'message' => 'Usuario no existe'/* , 'params' => $request->all() */];
        }

        return response()->json($respuesta);
    }

}
