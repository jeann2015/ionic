<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class empresa extends Model {

    public $table = 'empresa';
    public $primaryKey = 'idempresa';
    public $timestamps = true;
    public $fillable = [
        'url',
        'ruc',
        'razonsocial',
        'direccion',
        'idubigeo',
        'actividadinicio',
        'telefono',
        'celular',
        'email',
        
//        'nroitemsgrid',
//        'nroitemsform',
//        'imglogodoc',
//        'logodocprint',
//        'idmonedanac',
//        'atnombre',
//        'aturlweb',
//        'aturlruc',
//        'aturltc',
//        'atidpais',
//        'atidentidad',
//        'atcontribuyente',
//        'atnombreimpuesto',
//        'atvalorimpuesto',
//        'percepcion',
//        'retencion',
        
        'idtema',
        'imglogologin',
        'imglogosistema',
        'facebook',
        'twitter',
        'paginaweb',
        'whatsapp',
    ];
    protected $hidden = ['idempresa'];

    public function empresa($param) {
        $data = \DB::table('empresa')
                ->join('tema', 'empresa.idtema', '=', 'tema.idtema')
                ->select('empresa.razonsocial', 'empresa.url', 'empresa.direccion', 'empresa.imglogologin', 'empresa.imglogosistema', 'tema.nombre', 'tema.archivo')
                ->where($param)
                ->first();
        
        return $data;
    }

    public function paises($where = NULL) {
        $select = \DB::table('ubigeo')
                ->select('pais', 'nombre', 'nacionalidad')
                ->where('dpto', '=', '000')
                ->where('prov', '=', '00')
                ->where('dist', '=', '00');
        if (!empty($where)) {
            $select->where($where);
        }
        $data = $select
                ->orderBy('nombre', 'asc')
                ->get();
        return $data;
    }

    public function departamentos($pais, $where = NULL) {
        $select = \DB::table('ubigeo')
                ->select('dpto', 'nombre')
                ->where('pais', '=', $pais)
                ->where('dpto', '!=', '000')
                ->where('prov', '=', '00')
                ->where('dist', '=', '00');
        if (!empty($where)) {
            $select->where($where);
        }
        $data = $select
                ->orderBy('nombre', 'asc')
                ->get();

        return $data;
    }

    public function provincias($pais, $departamento, $where = NULL) {
        $select = \DB::table('ubigeo')
                ->select('prov', 'nombre')
                ->where('pais', '=', $pais)
                ->where('dpto', '=', $departamento)
                ->where('prov', '!=', '00')
                ->where('dist', '=', '00');
        if (!empty($where)) {
            $select->where($where);
        }
        $data = $select
                ->orderBy('nombre', 'asc')
                ->get();
        return $data;
    }

    public function distritos($pais, $departamento, $provincia, $where = NULL) {
        $select = \DB::table('ubigeo')
                ->select('dist', 'nombre')
                ->where('pais', '=', $pais)
                ->where('dpto', '=', $departamento)
                ->where('prov', '=', $provincia)
                ->where('dist', '!=', '00');
        if (!empty($where)) {
            $select->where($where);
        }
        $data = $select
                ->orderBy('nombre', 'asc')
                ->get();
        return $data;
    }

    public function idempresa($url) {
        $data = \DB::table('empresa')
                ->select('empresa.idempresa')
                ->where('url', '=', $url)
                ->first();

        return $data->idempresa;
    }

    public function documentos() {
        $data = \DB::table('documento')
                ->select('documento.*')
                ->get();
        return $data;
    }

    public function perfiles($id) {
        $data = \DB::table('perfil')
                ->select('perfil.idperfil', 'perfil.nombre')
                ->where('idempresa', '=', $id)
                ->get();
        return $data;
    }

    public function monedas($id) {
        $data = \DB::table('moneda')
                ->select('moneda.idmoneda', 'moneda.nombre')
                ->where('idempresa', '=', $id)
                ->get();
        return $data;
    }

    public function unidadmedidas($id) {
        $data = \DB::table('unidadmedida')
                ->select('unidadmedida.idunidadmedida', 'unidadmedida.nombre')
                ->where('idempresa', '=', $id)
                ->get();
        return $data;
    }

    public function tipoproductos() {
        $data = \DB::table('tipoproducto')
                ->select('tipoproducto.idtipoproducto', 'tipoproducto.nombre')
                ->get();
        return $data;
    }

    public function tipostocks() {
        $data = \DB::table('tipostock')
                ->select('tipostock.idtipostock', 'tipostock.nombre')
                ->get();
        return $data;
    }

    public function estadodocumentos() {
        $data = \DB::table('estadodocumento')
                ->select('estadodocumento.*')
                ->get();
        return $data;
    }

    public function serienumero($where) {
        $row = \DB::table('serie')
                ->select('serie.serienumero')
                ->where($where)
                ->first();

        return $row->serienumero;
    }

    public function updateSerieNumero($where) {
        $row = \DB::table('serie')
                ->select('serie.idserie', 'serie.serienumero')
                ->where($where)
                ->first();

        $serienumero = (int) $row->serienumero + 1;
        \DB::table('serie')->where('idserie', $row->idserie)->update(['serienumero' => $serienumero]);
    }

}