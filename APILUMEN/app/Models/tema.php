<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tema extends Model {

    protected $table = 'tema';
    protected $primaryKey = 'idtema';
    protected $fillable = [
        'nombre',
        'archivo',
        'imgvista'
    ];

    public function temas() {
        $data = tema::select('tema.*')
                        ->orderBy('nombre', 'DESC')->get()->toArray();

        return $data;
    }

}
