<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class contacto extends Model {

    protected $table = 'contacto';
    protected $primaryKey = 'idcontacto';
    public $timestamps = false;
    protected $fillable = [
        'identidad',
        'contacto',
        'telefono',
        'celular',
        'email',
        'descripcion'
    ];
    protected $hidden = ['idempresa'];

    public function contactos($id) {
        $data = \DB::table('contacto')
                ->select('contacto.*')
                ->where('contacto.identidad', '=', $id)
                ->orderBy('contacto.contacto', 'asc')
                ->get();

        return $data;
    }

}
