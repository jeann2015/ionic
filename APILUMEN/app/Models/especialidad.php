<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class especialidad extends Model {

    protected $table = 'especialidad';
    protected $primaryKey = 'idespecialidad';
    public $timestamps = false;
    protected $fillable = [
        'idempresa',
        'idtipoespecialidad',
        'nombre',
        'descripcion'
    ];

    public function especialidades($param) {
        $data = \DB::table('especialidad')
                ->leftJoin('tipoespecialidad', 'especialidad.idtipoespecialidad', '=', 'tipoespecialidad.idtipoespecialidad')
                ->select('especialidad.idespecialidad', 'especialidad.nombre', 'tipoespecialidad.nombre as tipo')
                ->where($param)
                ->get();

        return $data;
    }

}