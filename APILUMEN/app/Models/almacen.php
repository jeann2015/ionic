<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class almacen extends Model {

    protected $table = 'almacen';
    protected $primaryKey = 'idalmacen';
    public $timestamps = false;
    protected $fillable = [
        'idalmacen',
        'idempresa',
        'identidadregistro',
        'idguia',
        'fecharegistro',
        'ejercicio',
        'movimiento'
    ];
    protected $hidden = ['idempresa'];

    public function insertAlmacendet($data) {
        return \DB::table('almacendet')->insertGetId($data, 'idalmacendet');
    }

    public function insertAlmacendetdet($data) {
        \DB::table('almacendetdet')->insert($data);
    }

    public function deleteAlmacen($idguia) {
        $almacen = almacen::where('idguia', '=', $idguia)->first();

        if ($almacen) {
            $data = \DB::table('almacendet')
                    ->select('almacendet.idalmacendet')
                    ->where('idalmacen', $almacen->idalmacen)
                    ->get();

            $whereIn = [];
            foreach ($data as $row) {
                $whereIn[] = $row->idalmacendet;
            }

            if (!empty($whereIn)) {
                \DB::table('almacendetdet')->whereIn('idalmacendet', $whereIn)->delete();
            }

            \DB::table('almacendet')->where('idalmacen', $almacen->idalmacen)->delete();
            $almacen->delete();
        }
    }

}
