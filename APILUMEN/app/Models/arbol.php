<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class arbol extends Model {

    protected $table = 'arbol';
    protected $primaryKey = 'idarbol';
    public $timestamps = false;
    protected $fillable = [
        'idcategoria',
        'idempresa',
        'parent',
        'nombre'
    ];
    protected $hidden = ['idempresa'];

    public function grid($params) { 
        $data = arbol::select('idarbol', 'parent', 'nombre')
                ->where($params)
                ->orderBy('nombre', 'asc')
                ->get()
                ->toArray();

        return $data;
    }

    public function deleteNodo($where) {
        \DB::table('arbol')->whereIn('idarbol', $where)->delete();
    }

}
