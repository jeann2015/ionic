<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class promocion extends Model {

    protected $table = 'promocion';
    protected $primaryKey = 'idpromocion';
    public $timestamps = false;
    protected $fillable = [
        'idempresa',
        'nombre',
        'idproducto',
        'pvp',
        'idtipopromocion',
        'tipodscto',
        'valordscto',
        'pvpdscto',
        'desde',
        'hasta',
        'activo'
    ];
    protected $hidden = ['idempresa'];

    private function formatFecha($fecha, $format = 'dd/mm/yyyy') {
        $newFecha = NULL;
        if (!empty($fecha) && strlen($fecha) == 10) {
            if ($format === 'dd/mm/yyyy') {
                //de: yyyy-mm-dd a: dd/mm/yyyy 
                $fecha = explode('-', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
            if ($format === 'yyyy-mm-dd') {
                //de: dd/mm/yyyy a: yyyy-mm-dd 
                $fecha = explode('/', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
        }
        return $newFecha;
    }

    public function grid($param, $likename, $items = 25, $orderName, $orderSort) {
        $select = \DB::table('promocion')
                ->join('tipopromocion', 'promocion.idtipopromocion', '=', 'tipopromocion.idtipopromocion')
                ->leftJoin('producto', 'promocion.idproducto', '=', 'producto.idproducto')
                ->select('promocion.idpromocion', 'tipopromocion.idtipopromocion', 'tipopromocion.nombre as promocion', 'promocion.nombre', 'promocion.desde', 'promocion.hasta'
                        , 'promocion.tipodscto', 'promocion.pvp', 'promocion.valordscto', 'promocion.pvpdscto')
                ->where($param);

        if (!empty($likename)) {
            //$select->where('promocion.nombre', 'like', '%' . $likename . '%');
            $select->whereRaw('sp_ascii(promocion.nombre) ilike sp_ascii(?) ', ['%' . $likename . '%']);
        }

        $data = $select
                ->orderBy($orderName, $orderSort)
                ->paginate($items);

        foreach ($data as $row) {
            $row->desde = $this->formatFecha($row->desde);
            $row->hasta = $this->formatFecha($row->hasta);
        }

        return $data;
    }

    public function promociondet($params) {
        $data = \DB::table('promociondet')
                ->join('producto', 'promociondet.idproducto', '=', 'producto.idproducto')
                ->select('promociondet.*', 'producto.nombre')
                ->where($params)
                ->orderBy('producto.nombre', 'ASC')
                ->get();

        return $data;
    }

    public function GrabarPromociondet($data, $idpromocion) {
        \DB::table('promociondet')->where('idpromocion', $idpromocion)->delete();
        \DB::table('promociondet')->insert($data);
    }

}
