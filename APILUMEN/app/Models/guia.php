<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class guia extends Model {

    protected $table = 'guia';
    protected $primaryKey = 'idguia';
    public $timestamps = false;
    protected $fillable = [
        'idguia',
        'idempresa',
        'identidaddocumento',
        'identidadregistro',
        'identidadestado',
        'serie',
        'serienumero',
        'ejercicio',
        'fechadocumento',
        'fecharegistro',
        'movimiento',
        'observacion',
        'idmoneda',
        'mediopago',
        'iddocumentofiscal',
        'idcomprobante',
        'idestadodocumento'
    ];
    protected $hidden = ['idempresa'];

    private function formatFecha($fecha, $format = 'dd/mm/yyyy') {
        $newFecha = NULL;
        if (!empty($fecha) && strlen($fecha) == 10) {
            if ($format === 'dd/mm/yyyy') {
                //de: yyyy-mm-dd a: dd/mm/yyyy 
                $fecha = explode('-', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
            if ($format === 'yyyy-mm-dd') {
                //de: dd/mm/yyyy a: yyyy-mm-dd 
                $fecha = explode('/', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
        }
        return $newFecha;
    }

    public function grid($param, $betweendate, $items = 25, $orderName, $orderSort) {

        $select = \DB::table('guia')
                ->join('guiadet', 'guia.idguia', '=', 'guiadet.idguia')
                ->join('producto', 'guiadet.idproducto', '=', 'producto.idproducto')
                ->join('unidadmedida', 'producto.idunidadmedida', '=', 'unidadmedida.idunidadmedida')
                ->join('estadodocumento', 'guia.idestadodocumento', '=', 'estadodocumento.idestadodocumento')
                ->leftJoin('entidad', 'guia.identidaddocumento', '=', 'entidad.identidad')
                ->select('guia.idguia', 'guia.identidaddocumento', 'guia.serie', 'guia.serienumero', 'guia.fechadocumento', 'entidad.entidad', 'estadodocumento.idestadodocumento', 'estadodocumento.nombre as estadodocumento', 'producto.codigo', 'producto.nombre as producto', 'unidadmedida.abreviatura as unidadabrev', 'guiadet.cantidad')
                ->where($param);

        if (!empty($betweendate)) {
            $select->whereBetween('guia.fechadocumento', $betweendate);
        }
        $data = $select
                ->orderBy('fechadocumento', 'desc')
                ->orderBy('serienumero', 'desc')
                //->orderBy($orderName, $orderSort)
                ->paginate($items);

        foreach ($data as $row) {
            $row->fechadocumento = $this->formatFecha($row->fechadocumento);
        }

        return $data;
    }

    public function guiadet($params) {
        $data = \DB::table('guiadet')
                ->join('producto', 'guiadet.idproducto', '=', 'producto.idproducto')
                ->join('unidadmedida', 'producto.idunidadmedida', '=', 'unidadmedida.idunidadmedida')
                ->select('guiadet.*', 'producto.idproducto', 'producto.categoria', 'producto.codigo', 'producto.nombre as producto', 'producto.idtipostock', 'unidadmedida.abreviatura as unidadabrev')
                ->where($params)
                ->get();

        return $data;
    }

    public function guiadetdet($params) {
        $data = \DB::table('guiadet')
                ->join('guiadetdet', 'guiadet.idguiadet', '=', 'guiadetdet.idguiadet')
                ->select('guiadet.idguiadet', 'guiadetdet.vencimiento', 'guiadetdet.cantidad')
                ->where($params)
                ->get();

        return $data;
    }

    public function insertGuiadet($data) {
        return \DB::table('guiadet')->insertGetId($data, 'idguiadet');
    }

    public function updateGuiadet($data, $where) {
        \DB::table('guiadet')->where($where)->update($data);
    }

    public function deleteGuiadet($where) {
        if (is_array($where)) {
            \DB::table('guiadet')->whereIn('idguiadet', $where)->delete();
        } else {
            \DB::table('guiadet')->where('idguia', $where)->delete();
        }
    }

    public function insertGuiadetdet($data) {
        \DB::table('guiadetdet')->insert($data);
    }

    public function deleteGuiadetdet($where) {
        \DB::table('guiadetdet')->whereIn('idguiadet', $where)->delete();
    }
}
