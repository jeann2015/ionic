<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class producto extends Model {

    protected $table = 'producto';
    protected $primaryKey = 'idproducto';
    public $timestamps = false;
    protected $fillable = [
        'idempresa',
        'idtipoproducto',
        'idarbol',
        'idunidadmedida',
        'idtipoingreso',
        'idmonedacompra',
        'idmonedaventa',
        'categoria',
        'codigo',
        'nombre',
        'marca',
        'valorcompra',
        'valorcompraigv',
        'valorventabase',
        'valorventaigv',
        'valorventa',
        'impuesto',
        'procedencia',
        'stockmin',
        'stockmax',
        'dsctomax',
        'comisionventa',
        'descripcion',
        'activo',
        'idtipostock',
        'interno',
        'localizacion'
    ];
    protected $hidden = ['idempresa'];

    private function formatFecha($fecha, $format = 'dd/mm/yyyy') {
        $newFecha = NULL;
        if (!empty($fecha) && strlen($fecha) == 10) {
            if ($format === 'dd/mm/yyyy') {
                //de: yyyy-mm-dd a: dd/mm/yyyy 
                $fecha = explode('-', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
            if ($format === 'yyyy-mm-dd') {
                //de: dd/mm/yyyy a: yyyy-mm-dd 
                $fecha = explode('/', $fecha);
                $newFecha = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
            }
        }
        
        return $newFecha;
    }

    public function grid($param, $likename, $items = 25, $orderName, $orderSort) {

        $select = \DB::table('producto')
                ->join('tipoproducto', 'producto.idtipoproducto', '=', 'tipoproducto.idtipoproducto')
                ->leftJoin('unidadmedida', 'producto.idunidadmedida', '=', 'unidadmedida.idunidadmedida')
                ->leftJoin('moneda as monedacompra', 'producto.idmonedacompra', '=', 'monedacompra.idmoneda')
                ->leftJoin('moneda as monedaventa', 'producto.idmonedaventa', '=', 'monedaventa.idmoneda')
                ->select('producto.idproducto', 'tipoproducto.idtipoproducto', 'tipoproducto.nombre as tipoproducto', 'producto.categoria', 'producto.nombre', 'producto.codigo', 'unidadmedida.abreviatura as unidadmedidaabrev', 'monedacompra.simbolo as monedacompraabrev', 'monedaventa.simbolo as monedaventaabrev', 'producto.valorcompra', 'producto.valorventabase', 'producto.valorventaigv', 'producto.valorventa', 'producto.activo', 'producto.stockmin')
                ->where($param);

        if (!empty($likename)) {
            //$select->where('producto.nombre', 'like', '%' . $likename . '%');
            $select->whereRaw('sp_ascii(producto.nombre) ilike sp_ascii(?) ', ['%' . $likename . '%']);
        }
        
        $data = $select
                ->orderBy($orderName, $orderSort)
                ->paginate($items);

        return $data;
    }

    public function gridstock($param, $likename, $items = 25, $orderName, $orderSort) {

        $select = \DB::table('producto')
                ->join('tipoproducto', 'producto.idtipoproducto', '=', 'tipoproducto.idtipoproducto')
                ->join('tipostock', 'producto.idtipostock', '=', 'tipostock.idtipostock')
                ->join('unidadmedida', 'producto.idunidadmedida', '=', 'unidadmedida.idunidadmedida')
                ->select('producto.idproducto', 'producto.categoria', 'producto.nombre', 'producto.codigo', 'producto.idtipostock', 'producto.stockmin', 'tipoproducto.idtipoproducto', 'tipoproducto.nombre as tipoproducto', 'unidadmedida.abreviatura as unidadmedidaabrev', 'tipostock.nombre as tipostock')
                ->where($param);

        if (!empty($likename)) {
            //$select->where('producto.nombre', 'like', '%' . $likename . '%');
            $select->whereRaw('sp_ascii(producto.nombre) ilike sp_ascii(?) ', ['%' . $likename . '%']); 
        }
        $data = $select
                ->orderBy($orderName, $orderSort)
                ->paginate($items);

        return $data;
    }

    public function productostock($whereIn, $idempresa) {

        $data = \DB::table('producto')
                ->join('almacendet', 'producto.idproducto', '=', 'almacendet.idproducto')
                ->select('almacendet.idproducto', \DB::raw('SUM(almacendet.cantidad) as total'), 'almacendet.movimiento')
                ->where('producto.idempresa', $idempresa)
                ->whereIn('almacendet.idproducto', $whereIn)
                ->orderBy('almacendet.idproducto', 'ASC')
                ->groupBy('almacendet.idproducto')
                ->groupBy('almacendet.movimiento')
                ->get();

        $stock = [];
        // +/- stock de productos segun movimiento Ingreso/Salida.
        foreach ($data as $fila) {
            if (!isset($stock[$fila->idproducto]['stock'])) {
                $stock[$fila->idproducto]['stock'] = 0;
            }
            if ($fila->movimiento === 'I') {
                $stock[$fila->idproducto]['stock'] += $fila->total;
            }
            if ($fila->movimiento === 'S') {
                $stock[$fila->idproducto]['stock'] -= $fila->total;
            }
        }

        return $stock;
    }

    public function productostockdet($params, $column = 'almacendetdet.vencimiento') {

        $data = \DB::table('producto')
                ->join('almacendetdet', 'producto.idproducto', '=', 'almacendetdet.idproducto')
                ->select($column, \DB::raw('SUM(almacendetdet.cantidad) as total'), 'almacendetdet.movimiento')
                ->where($params)
                ->orderBy($column, 'ASC')
                ->groupBy($column)
                ->groupBy('almacendetdet.movimiento')
                ->get();

        $stock = [];
        // +/- stock de productos segun movimiento Ingreso/Salida.
        foreach ($data as $fila) {
            $campo = $fila->vencimiento === '0000-00-00' ? '' : $fila->vencimiento;
            if (!isset($stock[$campo]['stock'])) {
                $stock[$campo]['stock'] = 0;
            }
            if ($fila->movimiento === 'I') {
                $stock[$campo]['stock'] += $fila->total;
            }
            if ($fila->movimiento === 'S') {
                $stock[$campo]['stock'] -= $fila->total;
            }
        }

        $stockArray = [];
        foreach ($stock as $pk => $fila) {
            $stockArray[] = array('vencimiento' => $pk, 'stock' => $fila['stock']);
        }

        return $stockArray;
    }

    public function productoKardex($params) {

        $data = \DB::table('producto')
                ->join('unidadmedida', 'producto.idunidadmedida', '=', 'unidadmedida.idunidadmedida')
                ->leftJoin('almacendet', 'producto.idproducto', '=', 'almacendet.idproducto')
                ->leftJoin('almacen', 'almacendet.idalmacen', '=', 'almacen.idalmacen')
                ->leftJoin('guia', 'almacen.idguia', '=', 'guia.idguia')
                ->leftJoin('documentofiscal', 'guia.iddocumentofiscal', '=', 'documentofiscal.iddocumentofiscal')
                ->select('producto.idproducto', 'producto.codigo', 'producto.categoria', 'producto.nombre', 'producto.stockmin', 'guia.fechadocumento', 'guia.serienumero', 'almacen.movimiento', 'almacendet.cantidad', 'documentofiscal.nombre as documento', 'unidadmedida.abreviatura as unidadmedidaabrev')
                ->where($params)
                ->orderBy('guia.fechadocumento', 'ASC')
                ->orderBy('guia.iddocumentofiscal', 'ASC')
                ->orderBy('guia.serienumero', 'ASC')
                ->get();

        foreach ($data as $row) {
            $row->fechadocumento = $this->formatFecha($row->fechadocumento);
        }
        return $data;
    }

    public function productoServicios($params) {

        $data = \DB::table('producto')
                ->join('productoservicio', 'producto.idproducto', '=', 'productoservicio.idproducto')
                ->join('producto as productoitem', 'productoservicio.idproductoitem', '=', 'productoitem.idproducto')
                ->join('unidadmedida', 'productoitem.idunidadmedida', '=', 'unidadmedida.idunidadmedida')
                ->select('productoitem.idproducto', 'productoitem.codigo', 'productoitem.nombre as producto', 'unidadmedida.abreviatura as unidadabrev')
                ->where($params)
                ->orderBy('productoitem.nombre', 'ASC')
                ->get();

        return $data;
    }

    public function GrabarProductoServicio($data, $idproducto) {
        \DB::table('productoservicio')->where('idproducto', $idproducto)->delete();
        \DB::table('productoservicio')->insert($data);
    }

    public function listaProducto($params) {
        $data = \DB::table('producto')
                ->select('producto.*')
                ->where($params)
                ->get();
        return $data;
    }

    public function updateProducto($data, $where) {
        \DB::table('producto')->where($where)->update($data);
    }

    public function validadorDataRelacionada($id) {
        $data = \DB::table('almacendet')->select('idproducto')->where('idproducto', $id)->get();
        if (!empty($data)) {
            return ['validator' => true, 'message' => 'Tiene datos en almacén. No puede ser eliminado.'];
        }

        $data = \DB::table('guiadet')->select('idproducto')->where('idproducto', $id)->get();
        if (!empty($data)) {
            return ['validator' => true, 'message' => 'Tiene datos en guías de E/S. No puede ser eliminado.'];
        }

        $data = \DB::table('productoservicio')->select('idproductoitem')->where('idproductoitem', $id)->get();
        if (!empty($data)) {
            return ['validator' => true, 'message' => 'Tiene datos en materiales por tratamiento. No puede ser eliminado.'];
        }

        $data = \DB::table('promocion')->select('idproducto')->where('idproducto', $id)->get();
        if (!empty($data)) {
            return ['validator' => true, 'message' => 'Tiene datos en campañas de descuento por tratamiento. No puede ser eliminado.'];
        }

        $data = \DB::table('promociondet')->select('idproducto')->where('idproducto', $id)->get();
        if (!empty($data)) {
            return ['validator' => true, 'message' => 'Tiene datos en campañas de descuento por paquete. No puede ser eliminado.'];
        }

        return ['validator' => false];
    }
}
